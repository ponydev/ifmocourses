﻿{-# LANGUAGE ScopedTypeVariables #-}
-- REPL for untyped lambda calculus
module UnTyLambda.REPL where

import Monstupar
import UnTyLambda.Interpreter


-- Парсим строку в терм
parseLambda :: Monstupar Char Term
parseLambda = do
  atom <- parseAtom
  spaces
  other <- parseLambda'
  return $ foldl App atom other
  
parseLambda' = (do
  atom <- parseAtom
  spaces
  atoms <- parseLambda'
  return $ atom:atoms) <|> (return [])

parseAtom = parseLam <|> parseBraces <|> parseVar

parseLam = do
  char '\\'
  spaces
  name <- parseVarName
  spaces
  char '.'
  spaces
  expr <- parseLambda
  return $ Lam name expr

parseBraces = do
  char '('
  spaces
  expr <- parseLambda
  spaces
  char ')'
  return expr

parseVar = do
  name <- parseVarName
  return $ Var name

parseVarName = do
  c <- letter
  cs <- many letter
  return $ c:cs

letter = oneOf $ ['a'..'z'] ++ ['A'..'Z']
space = oneOf [' ','\t']
spaces = many space

--------------------------------------------------------------------------------
-- Заметье, что грамматика лямбда-выражений леворекурсивна.
-- Перед тем как бросаться кодить, сначала уберите леворекурсивность на бумаге,
-- а потом напишите получившуюся грамматику в EBNF вот сюда:
--Lamda = Atom Lamda'
--Lamda' = Atom Lambda'
--Atom = \Var.Lambda || (Lambda) || Var
--Var = many letter
--
-- прямо сюда, да
--------------------------------------------------------------------------------

-- Красиво печатаем терм (можно с лишними скобками, можно без)
prettyPrint :: Term -> String
prettyPrint (Var x) = x
prettyPrint (Lam s t) = "\\" ++ s ++ " . " ++ prettyPrint t
prettyPrint (App t1 t2) = "(" ++ prettyPrint t1 ++ " " ++ prettyPrint t2 ++ ")"


-- Собственно сам REPL. Первый аргумент — максимальное число итераций при
-- попытке нормализации стратегией из второго аргумента.
replLoop :: Integer -> (Integer -> Term -> Term) -> IO ()
replLoop patience strategy = do
  putStr "> "
  line <- getLine
  case runParser parseLambda line of
    Left _ -> putStrLn "Parse error" 
    Right (_, term) -> putStrLn $ prettyPrint $ strategy patience term

-- Диалог с (replLoop 100 no) должен выглядеть так:
-- > \x . (\y . y) x x
-- \x . x x