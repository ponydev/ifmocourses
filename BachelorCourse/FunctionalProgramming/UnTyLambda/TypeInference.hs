﻿{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE ScopedTypeVariables #-}
import Text.Printf

data Type = Id String                 --not using Const type
			| Arrow Type Type
  deriving Eq
  
data Term = Var String
          | Lam String Term
          | App Term Term
		  
instance Show Type where
  show (Id s) = s
  show (Arrow ty1 ty2) = showF ty1 ++ "->" ++ show ty2
                        where showF f@(Arrow ty1 ty2) = "(" ++ show f ++ ")"
                              showF z = show z

instance Show Term where
  show (Var x) = x
  show (Lam s t) = "\\" ++ s ++ "." ++ show t
  show (App t1 t2) = "(" ++ show t1 ++ " " ++ show t2 ++ ")"
							  
-- list of unique type variables
type UniqVars = [String]
newTypeVars :: UniqVars
newTypeVars = [ printf "a%i" a | (a :: Integer) <- [1..] ]
  
type Context = String -> Maybe Type
emptyContext = const Nothing

type Substitution = [(String,Type)]

type Constraints = [(Type,Type)]

freeVars :: String -> Type -> Bool
freeVars x1 (Id x2) | x1 == x2 = True
                | otherwise = False
freeVars x1 (Arrow ty1 ty2) = freeVars x1 ty1 || freeVars x1 ty2

-- extend context
extend :: Context -> String -> Context
extend c s s' 
			| s == s' = Just (Id s) 
			| otherwise = c s'

-- Apply substitution to type
apSubT :: Substitution -> Type -> Type
apSubT s t = foldr apSubT' t s
  where
    apSubT' (s1,t) (Id s2) | s1 == s2 = t
                           | otherwise = Id s2
    apSubT' s (Arrow t2 t3) = Arrow (apSubT' s t2) (apSubT' s t3)

-- Apply substitution to constraints
apSub :: Substitution -> Constraints -> Constraints
apSub = fmap . apSub'
  where
    apSub' s (t1,t2) = (apSubT s t1, apSubT s t2)

	
-- tc finds the type constraints of a term given a context.
tc :: UniqVars -> Context -> Term -> (UniqVars, Type, Constraints)
tc uv c (Var s) = maybe (error "Unrecognized variable") (\t -> (uv,t,[])) (c s) 
tc uv c (Lam s t) = let (uv',ty2,cs) = tc uv (extend c s) t
                        in  (uv',Arrow (Id s) ty2,cs)
tc uv c (App t1 t2) = let (uv',ty1,cs1) = tc uv c t1
                          (uv'',ty2,cs2) = tc uv' c t2
                          (xStr:uv''') = uv''
                          x = Id xStr
                          cs = (ty1,Arrow ty2 x) : (cs1 ++ cs2)
						in  (uv''', x, cs)

-- tc1 = tc with empty context and newTypeVars
tc1 ::  Term -> (Type, Constraints)
tc1 t = let (_,ty,c) = tc newTypeVars emptyContext t
        in (ty,c)

-- unification
unify :: Constraints -> Substitution
unify [] = []
unify ((ty1,ty2):cs) | ty1 == ty2 = unify cs
                     | (Id s) <- ty1
                     , not (s `freeVars` ty2)
                     = let sb = [(s,ty2)] in unify (apSub sb cs) ++ sb
                     | (Id s) <- ty2
                     , not (s `freeVars` ty1)
                     = let sb = [(s,ty1)] in unify (apSub sb cs) ++ sb
                     | (Arrow ty11 ty12) <- ty1
                     , (Arrow ty21 ty22) <- ty2
                     = unify ((ty11,ty21):(ty12,ty22):cs)
                     | otherwise = error "could not unify, invalid type"
			
findType :: Term -> Type
findType t = let (ty, cs) = tc1 t  
                 s = unify cs      -- substitution s unifies constraints
                 ty' = apSubT s ty 
             in ty'


t2 :: Term
t2 = Lam "x" (Lam "y" (Lam "z" (App (Var "x") (App (Var "y") (Var "z")))))

t3 :: Term
t3 = Lam "x" (Lam "y" (Lam "z" (App (App (Var "x") (Var "z"))(Var "y") )))

t4 :: Term
t4 = Lam "g" (Lam "h" (Lam "x" (App (App (Var "g") (App (Var "h") (Var "x")))(Var "x") )))

t5 :: Term
t5 = Lam "x" ( App (Lam "y" $ Var "y") (Var "x"))

t6 :: Term
t6 = Lam "x" (Var "x")