module TranReflex where

_⟵_ : Set → Set → Set1
B ⟵ A = B → A → Set

ℙ : Set → Set1
ℙ A = A → Set

_⊆_ : {A : Set} → ℙ A → ℙ A → Set 
r ⊆ s = ∀ a → r a → s a

⊆-refl : {A : Set} → {r : ℙ A} → r ⊆ r
⊆-refl a a∈r = a∈r

⊆-trans : {A : Set} → {r s t : ℙ A} → r ⊆ s → s ⊆ t → r ⊆ t
⊆-trans r⊆s s⊆t a a∈r = s⊆t a (r⊆s a a∈r)

_⊑_ :{A B : Set} → (B ⟵ A) → (B ⟵ A) → Set 
R ⊑ S = ∀ b a → R b a → S b a


⊑-refl : {A B : Set} {R : B ⟵ A} → R ⊑ R
⊑-refl _ = ⊆-refl

⊑-trans : {A B : Set}{R S T : B ⟵ A} → R ⊑ S → S ⊑ T → R ⊑ T
⊑-trans R⊑S S⊑T b = ⊆-trans (R⊑S b) (S⊑T b)



data Star {A B : Set} (T : _⟵_) : B ⟵ A
ε : ∀ {i} → Star T i i
⊑ T (Star T) (Star T) : ∀ {i j k} (x : T i j) (xs : Star T j k) → Star T i k