{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.Tree where

import Prelude (Show,Read,error, show)
import ITMOPrelude.Primitive


data Tree a = Node a (Tree a) (Tree a) 
            | Leaf                     
    deriving Show
	
	
emptyTree = Leaf
	
addToRoot x t = Node x t Leaf

addToLTree x (Node a left right) = Node a (addToLTree x left) right
addToLTree x Leaf = Node x Leaf Leaf

addToRTree x (Node a left right) = Node a left (addToRTree x right)
addToRTree x Leaf = Node x Leaf Leaf

rotateLeft (Node x left (Node y left' right')) = Node y (Node x left left') right'

rotateRight (Node x (Node y left' right') right) = Node y left' (Node x right' right)

tmap :: (a -> b) -> Tree a -> Tree b
tmap f  Leaf = Leaf
tmap f (Node a left right) = Node (f a) (tmap f left) (tmap f right)

tfoldr :: (a -> b -> b) -> b -> Tree a -> b
tfoldr f z Leaf = z
tfoldr f z (Node x left right) = f x (tfoldr f (tfoldr f z right) left)