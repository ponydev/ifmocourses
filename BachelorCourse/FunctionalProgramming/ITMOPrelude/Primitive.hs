﻿{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.Primitive where

import Prelude (Show,Read,error)

import ITMOPrelude.Algebra
---------------------------------------------
-- Синтаксис лямбда-выражений

-- Эквивалентные определения
example1 x  = x
example1'   = \x -> x
example1''  = let y = \x -> x in y
example1''' = y where
    y = \x -> x

-- Снова эквивалентные определения
example2 x y  = x %+ y
example2' x   = \y -> x %+ y
example2''    = \x -> \y -> x %+ y
example2'''   = \x y -> x %+ y
example2''''  = let z = \x y -> x %+ y in z
example2''''' = z where
    z x = \y -> x %+ y

-- Зацикленное выражение
undefined = undefined

-- Ниже следует реализовать все термы, состоящие из undefined заглушки.
-- Любые термы можно переписывать (natEq и natLt --- хорошие кандидаты).

-------------------------------------------
-- Примитивные типы

-- Тип с единственным элементом
data Unit = Unit deriving (Show,Read)

-- Пара, произведение
data Pair a b = Pair { fst :: a, snd :: b } deriving (Show,Read)

-- Вариант, копроизведение
data Either a b = Left a | Right b deriving (Show,Read)

-- Частый частный случай, изоморфно Either Unit a
data Maybe a = Nothing | Just a deriving (Show,Read)

-- Частый частный случай, изоморфно Either Unit Unit
data Bool = False | True deriving (Show,Read)

-- Следует отметить, что встроенный if с этим Bool использовать нельзя,
-- зато case всегда работает.

-- Ну или можно реализовать свой if
if' True a b = a
if' False a b = b

-- Трихотомия. Замечательный тип, показывающий результат сравнения
data Tri = LT | EQ | GT deriving (Show,Read)

-------------------------------------------
-- Булевы значения

-- Логическое "НЕ"
not :: Bool -> Bool
not True = False
not False = True

infixr 3 &&
-- Логическое "И"
(&&) :: Bool -> Bool -> Bool
True  && x = x
False && _ = False

infixr 2 ||
-- Логическое "ИЛИ"
(||) :: Bool -> Bool -> Bool
True  || _ = True
False || x = x

-------------------------------------------
-- Натуральные числа

data Nat = Zero | Succ Nat deriving (Show,Read)

natZero = Zero     -- 0
natOne = Succ Zero -- 1

-- Сравнивает два натуральных числа
natCmp :: Nat -> Nat -> Tri
natCmp Zero Zero			= EQ		
natCmp (Succ n) Zero 		= GT
natCmp Zero (Succ m) 		= LT
natCmp (Succ n) (Succ m)	= natCmp n m


-- n совпадает с m 
natEq :: Nat -> Nat -> Bool
natEq a b = case (natCmp a b) of 
				EQ -> True
				_ -> False

-- n меньше m
natLt :: Nat -> Nat -> Bool
natLt a b = case (natCmp a b) of 
				LT -> True
				_ -> False

infixl 6 +.
-- Сложение для натуральных чисел
(+.) :: Nat -> Nat -> Nat
Zero     +. m = m
(Succ n) +. m = Succ (n +. m)

infixl 6 -.
-- Вычитание для натуральных чисел

(-.) :: Nat -> Nat -> Nat
Zero	-. m = Zero
m		-. Zero = m
(Succ n) -. (Succ m) = n -. m



infixl 7 *.
-- Умножение для натуральных чисел
(*.) :: Nat -> Nat -> Nat
Zero     *. m = Zero
(Succ n) *. m = m +. (n *. m)

-- Целое и остаток от деления n на m
natDivMod :: Nat -> Nat -> Pair Nat Nat
natDivMod n Zero = error "Can't Divide by zero"
natDivMod n (Succ m) = case (natCmp n (Succ m)) of
  LT -> Pair Zero n
  EQ -> Pair natOne Zero
  GT -> let rec = (natDivMod (n -. (Succ m)) (Succ m)) in
    Pair (Succ $ fst rec) (snd rec)

natDiv n = fst . natDivMod n -- Целое
natMod n = snd . natDivMod n-- Остаток

-- Поиск GCD алгоритмом Евклида (должен занимать 2 (вычислителельная часть) + 1 (тип) строчки)
gcd :: Nat -> Nat -> Nat
gcd n m = case (natEq m Zero) of 
				True -> n
				False -> gcd m (n `natMod` m)
				
				
instance Monoid Nat where  
    mempty = Zero
    mappend = (+.)  
-------------------------------------------
-- Целые числа

-- Требуется, чтобы представление каждого числа было единственным
data Int = Neg Nat | NonNeg Nat deriving (Show,Read)

intZero   = NonNeg Zero   -- 0
intOne    = NonNeg $ Succ Zero    -- 1
intNegOne = Neg Zero -- -1

-- n -> - n
intNeg :: Int -> Int
intNeg (NonNeg (Succ a)) = Neg a
intNeg (Neg a) = NonNeg $ Succ a
intNeg (NonNeg Zero) = NonNeg Zero

-- Дальше также как для натуральных
intCmp :: Int -> Int -> Tri
intCmp (Neg a) (NonNeg b) = LT
intCmp (NonNeg a) (Neg b) = GT
intCmp (NonNeg a) (NonNeg b) = natCmp a b
intCmp (Neg a) (Neg b) = natCmp b a

-- n совпадает с m 
intEq :: Int -> Int -> Bool
intEq a b = case (intCmp a b) of 
				EQ -> True
				_ -> False

-- n меньше m
intLt :: Int -> Int -> Bool
intLt a b = case (intCmp a b) of 
				LT -> True
				_ -> False

infixl 6 .+., .-.
-- У меня это единственный страшный терм во всём файле
(.+.) :: Int -> Int -> Int
(NonNeg (Succ a)) .+. (Neg Zero) = NonNeg a -- -1
(NonNeg Zero) .+. a = a --0+something
(NonNeg (Succ a)) .+. (NonNeg (Succ b)) = NonNeg $ a +. b --2pos
(Neg (Succ a)) .+. (Neg (Succ b)) = Neg $ Succ $ a +. b --2neg
(NonNeg (Succ a)) .+. (Neg (Succ b)) = NonNeg a .+. Neg b --pos+neg
(Neg a) .+. (NonNeg b) = (NonNeg b) .+. (Neg a) --neg+pos

(.-.) :: Int -> Int -> Int
n .-. m = n .+. (intNeg m)

infixl 7 .*.
(.*.) :: Int -> Int -> Int
(NonNeg Zero) .*. _ = NonNeg Zero  --Zero
(NonNeg a) .*. (NonNeg b) = NonNeg $ a *. b --2 pos
(Neg a) .*. (Neg b) = NonNeg $ (Succ a) *. (Succ b) --2 neg
(NonNeg a) .*. (Neg b) = Neg z where Succ z = a *. b  --pos on neg
(Neg a) .*. (NonNeg b) = (NonNeg b) .*. (Neg a)  --neg on pos

instance Monoid Int where  
    mempty = intZero
    mappend = (.+.)

instance Group Int where
	gempty = intZero
	ginv = intNeg
	gappend = (.+.)
	
	
data MulInt = Mult Int

instance Monoid MulInt where
    mempty = Mult intOne
    (Mult a) `mappend` (Mult b) = Mult $ a .*. b

-------------------------------------------
-- Рациональные числа

data Rat = Rat Int Nat

ratNeg :: Rat -> Rat
ratNeg (Rat x y) = Rat (intNeg x) y

-- У рациональных ещё есть обратные элементы
ratInv :: Rat -> Rat
ratInv (Rat (NonNeg Zero) _) = error "No inverse bse of 0!"
ratInv (Rat _ Zero) = error "Strange rational - can't be 0 in denominator"
ratInv (Rat (NonNeg (Succ a)) (Succ b)) = (Rat (NonNeg (Succ b)) (Succ a))
ratInv (Rat (Neg a) b) = Rat (Neg b) a

-- Дальше как обычно
ratCmp :: Rat -> Rat -> Tri
ratCmp (Rat a Zero) (Rat c d) = error "Strange first rational - can't be 0 in denominator"
ratCmp (Rat a b) (Rat c Zero) = error "Strange second rational - can't be 0 in denominator"
ratCmp (Rat a (Succ b)) (Rat c (Succ d)) = intCmp (a .*. NonNeg(Succ d)) (c .*. NonNeg(Succ b))

-- n совпадает с m 
ratEq :: Rat -> Rat -> Bool
ratEq a b = case (ratCmp a b) of 
				EQ -> True
				_ -> False

-- n меньше m
ratLt :: Rat -> Rat -> Bool
ratLt a b = case (ratCmp a b) of 
				LT -> True
				_ -> False

infixl 7 %+, %-
(%+) :: Rat -> Rat -> Rat
(Rat a Zero) %+ (Rat c d) = error "Strange first rational - can't be 0 in denominator"
(Rat a b) %+ (Rat c Zero) = error "Strange second rational - can't be 0 in denominator"
(Rat a (Succ b)) %+ (Rat c (Succ d)) =  Rat (a .*. NonNeg (Succ d) .+. c .*. NonNeg (Succ b)) (Succ b *. Succ d)

(%-) :: Rat -> Rat -> Rat
n %- m = n %+ ratNeg m

infixl 7 %*, %/
(%*) :: Rat -> Rat -> Rat
(Rat a Zero) %* (Rat c d) = error "Strange first rational - can't be 0 in denominator"
(Rat a b) %* (Rat c Zero) = error "Strange second rational - can't be 0 in denominator"
(Rat a (Succ b)) %* (Rat c (Succ d)) =  Rat (a .*. c) (Succ b *. Succ d)

(%/) :: Rat -> Rat -> Rat
n %/ m = n %* (ratInv m)

instance Monoid Rat where
  mempty = Rat intZero natOne
  mappend = (%+)

instance Group Rat where
  gempty = Rat intZero natOne
  gappend = (%+)
  ginv = ratNeg


-------------------------------------------
-- Операции над функциями.
-- Определены здесь, но использовать можно и выше

infixr 9 .
f . g = \ x -> f (g x)

infixr 0 $
f $ x = f x

-- Эквивалентные определения
example3   a b c = gcd a (gcd b c)
example3'  a b c = gcd a $ gcd b c
example3'' a b c = ($) (gcd a) (gcd b c)

-- И ещё эквивалентные определения
example4  a b x = (gcd a (gcd b x))
example4' a b = gcd a . gcd b