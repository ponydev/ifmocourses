package com.ifmo;

import java.io.*;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by killerforfun on 21.03.15.
 */

//Simpler wrapper around BufferedReader
public class ChunkWriter {
    private final BufferedWriter bufferedWriter;

    public ChunkWriter(String outputFileName) throws IOException {
        this.bufferedWriter = new BufferedWriter(new FileWriter(outputFileName));
    }

    public void writeChunk(Queue<Long> outputChunk) throws IOException {
        while (outputChunk.size() > 0)
            bufferedWriter.write(outputChunk.poll().toString() + "\n");
        bufferedWriter.flush();
    }

    public void writeChunkAsList(List<Long> outputChunk) throws IOException {
        for (Long s : outputChunk)
            bufferedWriter.write(s + "\n");
        bufferedWriter.flush();
    }

    public void close() {
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
