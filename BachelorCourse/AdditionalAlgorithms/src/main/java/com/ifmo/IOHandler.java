package com.ifmo;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by killerforfun on 15.03.15.
 */
public class IOHandler {
    /**
     * Arrays with path to tempfiles. tempfile1 - bunch of input/output files, tempfile2 - output/input.
     */
    private final ArrayList<Path> tempFiles1;
    private final ArrayList<Path> tempFiles2;
    /**
     * Readers and writers. Initializing on tempfiles1 and tempfiles2.
     */
    private ArrayList<ChunkReader> tempReaders;
    private ArrayList<ChunkWriter> tempWriters;


    /**
     * Getters for sample data.
     */
    public int getCurrentWriter() {
        return currentWriter;
    }

    public ArrayList<Path> getTempFiles1() {
        return tempFiles1;
    }

    public ArrayList<Path> getTempFiles2() {
        return tempFiles2;
    }


    private ArrayList<Queue<Long>> chunksToMerge;
    private int runLength = 1;


    private int previousWriter = 0;
    private int currentWriter = 0;
    private int currentRWMode = 0;
    private int activeCount;

    public int getCurrentRWMode() {
        return currentRWMode;
    }

    private void nextWriter() {
        if (activeCount < wayNumber)
            activeCount++;
        previousWriter = currentWriter;
        currentWriter++;
        currentWriter %= wayNumber;
    }

    public int prevWriter() {
        return previousWriter;
    }

    /**
     * k-value of k-merge.
     */
    private final int wayNumber;
    /**
     * Directory to store temporary files.
     */
    private final Path tempDir;
    /**
     * Maximum chunk limit to store in buffer simultaneously. For moment of time only chunkLimit * (wayNumber + 1) will be in memory.
     */
    private final int chunkLimit;

    public IOHandler(Path tempDir, int wayNumber, int chunkLimit) {
        this.activeCount = chunkLimit;
        this.tempFiles1 = new ArrayList<>();
        this.tempFiles2 = new ArrayList<>();
        this.wayNumber = wayNumber;
        this.tempDir = tempDir;
        this.chunkLimit = chunkLimit;
        tempReaders = new ArrayList<>();
        tempWriters = new ArrayList<>();
        chunksToMerge = new ArrayList<>();
    }

    /**
     * Initializing files in temp directory.
     */
    public void initializeTempFiles() {
        for (int i = 0; i < wayNumber * 2; i++) {
            Path f = Paths.get(tempDir + "/file" + i + ".txt");
            f.toFile().getParentFile().mkdirs();
            try {
                f.toFile().createNewFile();
            } catch (IOException e) {
                System.err.println("There is no such path for output files! Check output dir!");
            }

            if (i < wayNumber)
                tempFiles1.add(f);
            else
                tempFiles2.add(f);
        }
        try {

            for (Path p : tempFiles1) {
                tempWriters.add(new ChunkWriter(p.toString()));
                //clear files.
                new PrintWriter(p.toString()).close();
            }
            for (Path p : tempFiles2) {
                tempReaders.add(new ChunkReader(p.toString(), chunkLimit, chunkLimit));
                //clear files.
                new PrintWriter(p.toString()).close();
            }
        } catch (IOException e) {
            System.err.println("Error during initialization!");
        }
    }

    /**
     * Procedure for writing chunk to 'split File' (1-st stage of algo).
     *
     * @param currentOutChunk - chunk to write.
     */
    public void writeInputToTempFiles(List<Long> currentOutChunk) {
        try {
            java.util.Collections.sort(currentOutChunk);
            tempWriters.get(currentWriter).writeChunkAsList(currentOutChunk);
            currentOutChunk.clear();
        } catch (IOException e) {
            System.err.println("Can't write to  writer while splitting input to tempfiles!");
        }
        nextWriter();
    }

    public void initialize() {
        initializeTempFiles();
    }

    /**
     * Function that switched bunch of input files with output files.
     *
     * @return new length value of run.
     */
    public long switchRWFiles() {
        currentWriter = 0;
        currentRWMode++;
        currentRWMode %= 2;
        try {
            for (int i = 0; i < wayNumber; i++) {
                tempWriters.get(i).close();
                tempReaders.get(i).close();
            }

            tempReaders.clear();
            tempWriters.clear();
            for (Path p : (currentRWMode == 0 ? tempFiles1 : tempFiles2)) {
                new PrintWriter(p.toString()).close();
                tempWriters.add(new ChunkWriter(p.toString()));
            }

            runLength *= activeCount;
            for (Path p : (currentRWMode == 1 ? tempFiles1 : tempFiles2))
                tempReaders.add(new ChunkReader(p.toString(), chunkLimit, runLength));
            activeCount = 0;
        } catch (IOException e) {
            System.err.println("Can't write to  writer while switching!");
        }
        return runLength;
    }

    public void readChunksToMerge() throws IOException {
        for (ChunkReader cr : tempReaders) {
            cr.clearCurrentChunk();
            Queue<Long> qq = cr.readChunk();
            if (qq.size() > 0) {
                chunksToMerge.add(qq);
            }
        }
    }

    public void mergeChunks() throws IOException {
        currentWriter = 0;
        readChunksToMerge();
        while (chunksToMerge.size() > 0) {
            merge(chunksToMerge);
            chunksToMerge.clear();
            readChunksToMerge();
            nextWriter();
        }
    }

    public void fillBuffer(ArrayList<Queue<Long>> elements, int minPos) throws IOException {
        if (tempReaders.get(minPos).moreChunks()) {
            Queue<Long> temp = tempReaders.get(minPos).readChunk();
            if (temp.size() > 0)
                elements.set(minPos, temp);
        }
    }

    /**
     * @param elements - arraylist of chunks
     * @throws IOException
     */


    public void merge(ArrayList<Queue<Long>> elements) throws IOException {
        Queue<Long> out = new ArrayBlockingQueue<>(chunkLimit);
        while (elements.size() > 0) {

            Long min = Long.MAX_VALUE;
            int minPos = -1;
            for (int i = 0; i < elements.size(); i++) {
                if (!elements.get(i).isEmpty() && elements.get(i).peek().compareTo(min) < 0) {
                    min = elements.get(i).peek();
                    minPos = i;
                }
            }
            if (minPos == -1) {
                break;
            }
            out.add(min);
            if (out.size() >= chunkLimit) {
                tempWriters.get(currentWriter).writeChunk(out);
                out.clear();
            }
            elements.get(minPos).poll();
            if (elements.get(minPos).size() == 0)
                fillBuffer(elements, minPos);
        }
        tempWriters.get(currentWriter).writeChunk(out);

    }

}

