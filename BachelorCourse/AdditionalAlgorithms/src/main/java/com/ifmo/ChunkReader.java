package com.ifmo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by killerforfun on 21.03.15.
 */

//Simpler wrapper around BufferedReader
public class ChunkReader {
    private final BufferedReader bufferedReader;
    private final int chunkLength;
    private Long element;
    private boolean fileEnd = false;
    private int runLength;
    private int currentChunk;

    public ChunkReader(String inputFileName, int chunkLength, int runLength) throws FileNotFoundException {
        this.chunkLength = chunkLength;
        this.bufferedReader = new BufferedReader(new FileReader(inputFileName));
        this.runLength = runLength;
        currentChunk = 1;
    }

    public boolean canRead() {
        return !fileEnd;
    }

    public Queue<Long> readChunk() throws IOException {
        Queue<Long> outputChunk = new ArrayBlockingQueue<>(chunkLength);
        int elementsRead = 0;
        do {
            String line = bufferedReader.readLine();
            if (line == null) {
                fileEnd = true;
                break;
            }
            element = Long.parseLong(line);
            elementsRead++;
            outputChunk.add(element);
        } while (elementsRead % chunkLength != 0);
        currentChunk++;
        return outputChunk;
    }

    public ArrayList<Long> readChunkAsList() throws IOException {
        return new ArrayList<>(readChunk());
    }

    public void close() {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearCurrentChunk() {
        currentChunk = 1;

    }

    public boolean moreChunks() {
        return currentChunk <= runLength / chunkLength;
    }

}
