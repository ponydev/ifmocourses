package com.ifmo; /**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 04.03.2015
 * To change this template use File | Settings | File Templates.
 */

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class KWayMergeSort {
    public final int numberOfWays;
    public final int chunkLimit;
    private IOHandler ioHandler;
    private long totalNumberOfElements = 0;


    public KWayMergeSort(String tempDir, int numberOfWays, int chunkLimit) {
        this.numberOfWays = numberOfWays;
        this.chunkLimit = chunkLimit;
        this.ioHandler = new IOHandler(Paths.get(tempDir), numberOfWays, chunkLimit);
        this.ioHandler.initialize();
    }


    public static void generateRandomFile(String fullName, long count) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(fullName));
        // create random object
        Random r = new Random();
        for (long i = 0; i < count; i++) {
            if (i % 100000000 == 0) System.out.println("Generated - " + i);
            bw.write(r.nextLong() + "\n");
        }
        bw.flush();
        bw.close();
    }

    long nextLong(Random rng, long n) {
        long bits, val;
        do {
            bits = (rng.nextLong() << 1) >>> 1;
            val = bits % n;
        } while (bits - val + (n - 1) < 0L);
        return val;
    }

    public long sortStep() {
        long k = ioHandler.switchRWFiles();
        try {
            ioHandler.mergeChunks();
        } catch (IOException e) {
            System.err.println("Error during sort step!");
        }
        return k;
    }

    public void sort(String outFile) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        System.out.println(dateFormat.format(cal.getTime())); //2014/08/06 16:00:22
        long k = 1;
        while (totalNumberOfElements > k) {
            k = sortStep();
            System.out.println("Current step - " + k + "/" + totalNumberOfElements);
        }
        sortStep();

        try {
            Files.copy((ioHandler.getCurrentRWMode() == 1 ? ioHandler.getTempFiles1().get(0) : ioHandler.getTempFiles2().get(0)), Paths.get(outFile), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            System.err.println("Can't write result to output file!");
        }
        System.out.println(dateFormat.format(cal.getTime()));
    }

    private String getResourceInput(String inputName) {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResource(inputName).getFile();
    }

    public void splitInput(String inputFileName) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        System.out.println(dateFormat.format(cal.getTime())); //2014/08/06 16:00:22
        ArrayList<Long> element;
        ClassLoader classLoader = getClass().getClassLoader();
        totalNumberOfElements = 0;
        try {
            ChunkReader chunkReader = new ChunkReader(inputFileName, chunkLimit, chunkLimit);
            while (chunkReader.canRead()) {
                element = chunkReader.readChunkAsList();
                totalNumberOfElements++;
                ioHandler.writeInputToTempFiles(element);
            }
            chunkReader.close();
        } catch (IOException e) {
            System.err.println("Error during split input!");
        }
        System.out.println(dateFormat.format(cal.getTime()));

    }

    //Something like dumb checker. Best not used.
    public static int checkOutput(String fileCheck) {
        long next, now = 0l;
        Path file = Paths.get(fileCheck);
        try {
            BufferedReader br = new BufferedReader(new FileReader(file.toFile()));
            String line = br.readLine();
            int lineNumber = 1;
            while (line != null) {
                now = Long.parseLong(line);
                line = br.readLine();
                lineNumber++;
                if (line != null)
                    next = Long.parseLong(line);
                else break;
                if (next < now) {
                    System.out.println("Проблема! Next = " + next + " Now = " + now + " in lines " + (lineNumber - 1) + " and " + lineNumber);
                    br.close();
                    return 1;
                }
            }
            br.close();
            System.out.println("Файл упорядочен.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }


    //Just for testing purposes.
    public void standartSort(String fullName) {
        try {
            long startTime = System.nanoTime();
            BufferedReader br = new BufferedReader(new FileReader(fullName));
            List<Long> ll = new LinkedList<>();
            String line = br.readLine();
            while (line != null) {
                ll.add(Long.parseLong(line));
                line = br.readLine();
            }

            br.close();
            long endTime = System.nanoTime();
            System.out.println("Reading " + (endTime - startTime));
            Collections.sort(ll);


            FileWriter writer = new FileWriter("output.txt");
            for (Long str : ll) {
                writer.write(Long.toString(str));
            }
            writer.close();


        } catch (IOException e) {
            System.err.println("Something went wrong with standard sort input");
        }
    }


    public static void main(String[] args) {
        int wayNumber = 0;
        int chunkLimit = 0;
        String inputFile = "in";
        String outputFile = "out";
        String tempDir = System.getProperty("user.dir") + "/temp";


        if (args.length > 0) {
            if ("/h".equals(args[0])) {
                System.out.println("Usage:");
                System.out.println("kwaymergesort 3 15 input.txt output.txt temp");
                System.out.println("3 - number of ways (k)");
                System.out.println("15 - start batch size and buffer limit for rw");
                System.out.println("input.txt - path to input file");
                System.out.println("output.txt - path to output file");
                System.out.println("temp - temp dir for temp files store in there");
                System.out.println("");
                System.out.println("Also it can generate sample input");
                System.out.println("kwaymergesort g 10000 out.txt");
                System.out.println("10000 - number of longs in file");
                System.out.println("out.txt - output file name");
                System.out.println("");
                System.out.println("Also it can check file with longs on correct sorting");
                System.out.println("kwaymergesort c in.txt");
                System.out.println("in.txt - name of file for check");

                System.exit(0);
            }
            if ("g".equals(args[0])) {
                System.out.println("Generating sample input");
                long numberOfLongs = 0;
                try {
                    numberOfLongs = Long.parseLong(args[1]);
                } catch (NumberFormatException e) {
                    System.err.println("Second argument - number of longs in file must be long.");
                    System.exit(1);
                } catch (ArrayIndexOutOfBoundsException e1) {
                    System.err.println("Second argument must present. See /h for more info.");
                    System.exit(1);
                }
                String outFileName = args[2];

                try {
                    generateRandomFile(outFileName, numberOfLongs);
                } catch (IOException e) {
                    System.err.println("Can't generate random file!");
                    System.exit(1);
                }

                System.exit(0);
            }
            if ("c".equals(args[0])) {
                try {
                    String checkFile = args[1];
                    System.out.println("Checking file: " + checkFile);
                    checkOutput(checkFile);
                    System.exit(0);
                } catch (ArrayIndexOutOfBoundsException e1) {
                    System.err.println("Second argument must present. See /h for more info.");
                    System.exit(1);
                }
            }


            try {
                wayNumber = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.err.println("First argument - way number must be integer.");
                System.exit(1);
            }

            try {
                chunkLimit = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                System.err.println("Second argument - chunk limit must be integer.");
                System.exit(1);
            }

            inputFile = args[2];
            outputFile = args[3];
            tempDir = args[4];

        } else {
            System.err.println("Need more args!");
            System.exit(1);
        }


        KWayMergeSort kwms = new KWayMergeSort(tempDir, wayNumber, chunkLimit);
        long startTime = System.nanoTime();
        System.out.println("Started.");
        kwms.splitInput(inputFile);
        System.out.println("Time spend on splitting - " + (System.nanoTime() - startTime));
        kwms.sort(outputFile);
        System.out.println("Time spend on sorting - " + (System.nanoTime() - startTime));

    }


    private void test() {

        KWayMergeSort kwms;
        long startTime, endTime;
        String tempDir = System.getProperty("user.dir") + "/temp";


        kwms = new KWayMergeSort(tempDir, 20, 1000000);
        startTime = System.nanoTime();
        kwms.splitInput(tempDir + "inputTest.txt");
        kwms.sort("out.txt");
        endTime = System.nanoTime();

        System.out.println(endTime - startTime);
        System.out.println((endTime - startTime) / 1000000);


        kwms = new KWayMergeSort(tempDir, 30, 1000000);
        startTime = System.nanoTime();
        kwms.splitInput(tempDir + "inputTest.txt");
        kwms.sort("out.txt");
        endTime = System.nanoTime();

        System.out.println(endTime - startTime);
        System.out.println((endTime - startTime) / 1000000);


        kwms = new KWayMergeSort(tempDir, 50, 1000000);
        startTime = System.nanoTime();
        kwms.splitInput(tempDir + "inputTest.txt");
        kwms.sort("out.txt");
        endTime = System.nanoTime();

        System.out.println(endTime - startTime);
        System.out.println((endTime - startTime) / 1000000);


        kwms = new KWayMergeSort(tempDir, 70, 1000000);
        startTime = System.nanoTime();
        kwms.splitInput(tempDir + "inputTest.txt");
        kwms.sort("out.txt");
        endTime = System.nanoTime();

        System.out.println(endTime - startTime);
        System.out.println((endTime - startTime) / 1000000);


        kwms = new KWayMergeSort(tempDir, 100, 1000000);
        startTime = System.nanoTime();
        kwms.splitInput(tempDir + "inputTest.txt");
        kwms.sort("out.txt");
        endTime = System.nanoTime();

        System.out.println(endTime - startTime);
        System.out.println((endTime - startTime) / 1000000);

        startTime = System.nanoTime();
        kwms.standartSort(tempDir + "inputTest.txt");
        endTime = System.nanoTime();

        System.out.println(endTime - startTime);
        System.out.println((endTime - startTime) / 1000000);

    }
}
