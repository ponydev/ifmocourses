import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.*;


@SuppressWarnings("serial")
public class SetUpDisplay extends JFrame	{
	public  int sizex=400; // max x = sizex min y = -sizey400
	public  int sizey=300; // max y = sizey min y = -sizey300
	public int offsetx,offsety;
	final static Point BotRight=new Point(10000,10000);
	final static Point BotLeft=new Point(-10000,10000);
	final static Point TopLeft=new Point(-10000,-10000);
	final static Point TopRight=new Point(10000,-10000);
	
	
	final Triangle Left=new Triangle(BotLeft,BotRight,TopLeft);
	final Triangle Right=new Triangle(TopLeft,BotRight,TopRight);
	
	public Set<Point> q;
	
	public JLabel label;
	public JLabel label1;
	public JLabel label2;
	public JLabel label3;
	public JLabel label10;
	public JLabel label11;
	public JLabel label25;
	public JLabel label26;
	public JLabel label27;
	public JLabel label28;
	public JTextField RandomDots;
	public JButton Generation,Restoration;
	
	//DirectedAcyclicGraph<Triangle, DefaultEdge> TriangleGraph = new DirectedAcyclicGraph<Triangle, DefaultEdge>(DefaultEdge.class);
	HashMap <Triangle,Set<Triangle>> History=new HashMap<Triangle,Set<Triangle>>();
	Set<Triangle> Delane=new HashSet<Triangle>();
	Set<Triangle> Copy=new HashSet<Triangle>();
	Set<Triangle> Otv=new HashSet<Triangle>();
	HashMap <Edge,Triangle> AdjacentEdges=new HashMap<Edge, Triangle>();
	HashMap <Edge,Triangle> AdjacentEdgesF=new HashMap<Edge, Triangle>();
	
	SetUpDisplay(Set<Point> z,int testmode)	{
		super("Triangulation");
		final JCheckBox checkBox = new JCheckBox("Delaunay triangulation Off",false);
		final JCheckBox checkBox1 = new JCheckBox("Circumcircles Off",false);
		final JCheckBox checkBox2 = new JCheckBox("Voronoi diagram Off",false);
		
		
		
		//////////////////////////TESTMODE///////////////////////////////////////////
		
		if (testmode!=0) {
			//Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
			//System.out.println(currentTimestamp);
			//long timestamp = System.currentTimeMillis()/1000;

			q=z;
			AdjacentEdgesF.put(Right.Sfirst,Right);
			AdjacentEdgesF.put(Right.Ssecond,Right);
			AdjacentEdgesF.put(Right.Sthird,Right);
			AdjacentEdgesF.put(Left.Sfirst,Left);
			AdjacentEdgesF.put(Left.Ssecond,Left);
			AdjacentEdgesF.put(Left.Sthird,Left);
			
			
			//random generator dots
			Set<Point> xx=new HashSet<Point> ();
			for (int j=1;j<testmode+1;j++) {
				xx.add(new Point ((int)(Math.random()*780)-400,(int)(Math.random()*545)-255));
			}
			
			long timestamp2 = System.nanoTime();
			
			
			//algorithm
			final Triangulation Algo = new Triangulation (xx,AdjacentEdgesF);
			Algo.SolveDelone(History,Delane,Left,Right);
			
			
			
			
			long timestamp3 = System.nanoTime();
			
			
			
			//System.out.println(timestamp3-timestamp2);  
		}else {
		
			
			/////////////////////////////////////////////////////////////////////////////////
		
			
			
			
			
			
			
			
			
		q=z;
		AdjacentEdgesF.put(Right.Sfirst,Right);
		AdjacentEdgesF.put(Right.Ssecond,Right);
		AdjacentEdgesF.put(Right.Sthird,Right);
		AdjacentEdgesF.put(Left.Sfirst,Left);
		AdjacentEdgesF.put(Left.Ssecond,Left);
		AdjacentEdgesF.put(Left.Sthird,Left);
		
		final Triangulation Algo = new Triangulation (q,AdjacentEdgesF);
		Algo.SolveDelone(History,Delane,Left,Right);
		
		
		
		Iterator<Triangle> it1= Delane.iterator();
		while (it1.hasNext()) {
			Triangle NextT=it1.next();
			if ((NextT.a!=BotLeft) && (NextT.b!=BotLeft) && (NextT.c!=BotLeft) && (NextT.a!=BotRight) && (NextT.b!=BotRight) && (NextT.c!=BotRight) && (NextT.a!=TopLeft) && (NextT.b!=TopLeft) && (NextT.c!=TopLeft) && (NextT.a!=TopRight) && (NextT.b!=TopRight) && (NextT.c!=TopRight)	) {
				Copy.add(NextT);
			}
		}
		Iterator<Edge> it2= AdjacentEdgesF.keySet().iterator();
		AdjacentEdges.clear();
		while (it2.hasNext()) {
			Edge NextT=it2.next();
			if ((NextT.a!=BotLeft) && (NextT.b!=BotLeft)  && (NextT.a!=BotRight) && (NextT.b!=BotRight)  && (NextT.a!=TopLeft) && (NextT.b!=TopLeft)  && (NextT.a!=TopRight) && (NextT.b!=TopRight) ) {
				Triangle NextTT=AdjacentEdgesF.get(NextT);
				if ((NextTT.a!=BotLeft) && (NextTT.b!=BotLeft) && (NextTT.c!=BotLeft) && (NextTT.a!=BotRight) && (NextTT.b!=BotRight) && (NextTT.c!=BotRight) && (NextTT.a!=TopLeft) && (NextTT.b!=TopLeft) && (NextTT.c!=TopLeft) && (NextTT.a!=TopRight) && (NextTT.b!=TopRight) && (NextTT.c!=TopRight)	) {
					Edge NextTemp=new Edge (NextT.b,NextT.a);
					AdjacentEdges.put(NextTemp, NextTT);
				}
			}
		}
		
		
		ChangeCoord(Copy);
		BasicJPanel panel= new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,0);
		getContentPane().add(panel);
		setPreferredSize(new Dimension (2*sizex,2*sizey));
		setBackground(Color.black);
		panel.setOpaque(true);
		panel.setBackground(Color.black);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		
		final JFrame j = new JFrame ("Menu");
		j.setLayout(new FlowLayout());
		j.setPreferredSize(new Dimension (400,600));
		j.setVisible(true);
		label = new JLabel("Current Mouse Coords =>");
		label1 = new JLabel("Last Clicked Point =>");
		label25 = new JLabel("Last Asked Triangle :");
		label26 = new JLabel("Point A:");
		label27 = new JLabel("Point B:");
		label28 = new JLabel("Point C:");
		label2 = new JLabel ("Number of Dots in Triangulation = "+q.size());
		label3 = new JLabel ("Number of Triangles in Triangulation = "+Copy.size());
		label10 = new JLabel ("Generator of Random Dots:");
		label11 = new JLabel("This button deletes all! Be carefully with it!");
		RandomDots = new JTextField ("Enter Amount");
		RandomDots.setPreferredSize(new Dimension (200,30));
		Generation = new JButton ("Generate!");
		Restoration = new JButton ("Restore!");
		j.setLocation(2*sizex,0);
		j.pack();
		Container contentPane = j.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		JPanel Labels = new JPanel ();
		JPanel Checks = new JPanel ();
		JPanel TextField = new JPanel ();
		JPanel Hints = new JPanel ();
		Checks.setLayout(new BoxLayout(Checks, BoxLayout.Y_AXIS));
		Labels.setLayout(new BoxLayout(Labels, BoxLayout.Y_AXIS));
		Hints.setLayout(new BoxLayout(Hints, BoxLayout.Y_AXIS));
		TextField.setLayout(new FlowLayout());
		Checks.add(checkBox);
		Checks.add(checkBox1);
		Checks.add(checkBox2);
		Labels.add(label);
		Labels.add(label1);
		Labels.add(Box.createRigidArea(new Dimension(0,30)));
		Labels.add(label25);
		Labels.add(Box.createRigidArea(new Dimension(0,10)));
		Labels.add(label26);
		Labels.add(label27);
		Labels.add(label28);
		Labels.add(Box.createRigidArea(new Dimension(0,20)));
		Labels.add(label2);
		Labels.add(label3);
		JLabel hotkey0 = new JLabel("HOTKEYS: ");
		JLabel hotkey1 = new JLabel("t  --  Printing triangle,in which your mouse is.");
		JLabel hotkey2 = new JLabel("s  --  Save points");
		JLabel hotkey3 = new JLabel("o  --  Open file with points");
		Hints.add(hotkey0);
		Hints.add(hotkey1);
		Hints.add(hotkey2);
		Hints.add(hotkey3);
		TextField.add(RandomDots);
		TextField.add(Generation);
		
		
		contentPane.add(Checks);
		contentPane.add(Box.createRigidArea(new Dimension(0,30)));
		contentPane.add(Labels);
		contentPane.add(Box.createRigidArea(new Dimension(0,30)));
		contentPane.add(label11);
		contentPane.add(Box.createRigidArea(new Dimension(0,10)));
		contentPane.add(Restoration);
		contentPane.add(Box.createRigidArea(new Dimension(0,30)));
		contentPane.add(label10);
		contentPane.add(TextField);
		contentPane.add(Box.createRigidArea(new Dimension(0,20)));
		contentPane.add(Hints);
		
		
		
		
		Generation.setActionCommand("disable");
		Generation.setEnabled(true);
		Generation.addActionListener(new ActionListener() {    
			public void actionPerformed(ActionEvent e) {
				if ("disable".equals(e.getActionCommand())) {
					Generation.setEnabled(true);
					String s = RandomDots.getText();
					try {
						int i = Integer.parseInt(s); //this line will throw a NumberFormatException. It will also throw it if you use Integer.parseInt to turn into an int the String: "2.35" which is a float number
						Set<Point> z=new HashSet<Point> ();
						for (int j=1;j<i+1;j++) {
							z.add(new Point ((int)(Math.random()*780)-400,(int)(Math.random()*545)-255));
						}
						dispose();
						j.dispose();
						new SetUpDisplay((Set <Point>)z,0);
					} catch (NumberFormatException nfe) {

						String message="Please input positive integer!";
					    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
					            JOptionPane.ERROR_MESSAGE);
					}
				} 
			}
		});
		
		
		Restoration.setActionCommand("disable");
		Restoration.setEnabled(true);
		Restoration.addActionListener(new ActionListener() {    
			public void actionPerformed(ActionEvent e) {
				if ("disable".equals(e.getActionCommand())) {
					Restoration.setEnabled(true);
						dispose();
						j.dispose();
						HashSet<Point> z=new HashSet<Point>();
						new SetUpDisplay((Set <Point>)z,0);
				} 
			}
		});
		
		
		RandomDots.addMouseListener(new MouseAdapter() {    
			public void mouseClicked(MouseEvent e) {
				RandomDots.selectAll();
			} 
		});
	    	
		addMouseMotionListener( new MouseAdapter() {  
			public void mouseMoved(MouseEvent e) {
				offsetx =e.getX()-sizex-8; 
				offsety=Math.abs(e.getY()-sizey-30);
				if (Math.signum(e.getY()-sizey-30)==1) {
					offsety=-Math.abs(e.getY()-sizey-30);
				}
				label.setText("Current Mouse Coords => X:"+offsetx+" |Y:"+offsety);
			}
		});
			addKeyListener (new KeyListener() {
				@Override
				public void keyPressed(KeyEvent ke) {
					 char i = ke.getKeyChar();
					  String str = Character.toString(i);
					  if (str.equals("t")) {
							Triangle temp=Left;
							Point Next=new Point (offsetx,offsety);
							if (Left.IsInsideTriangle(Next)==-1){
								temp=Algo.FindTriangle(History,Left,Next);
							}
							if (Right.IsInsideTriangle(Next)==-1){
								temp=Algo.FindTriangle(History,Right,Next);
							}
							if ((Right.IsInsideTriangle(Next)!=-1)&&(Left.IsInsideTriangle(Next)!=-1)){
								temp=Algo.FindTriangle(History,Right,Next);
							}
					  temp.ChC();
					  if ((Math.abs(temp.a.x)==10000) || (Math.abs(temp.b.x)==10000) || (Math.abs(temp.c.x)==10000)) {
					  }else {
							label26.setText("Point A: ("+temp.a.x+", "+temp.a.y+")");
							label27.setText("Point B: ("+temp.b.x+", "+temp.b.y+")");
							label28.setText("Point C: ("+temp.c.x+", "+temp.c.y+")");
					  }
					  temp.ChC();
					  }
					  if (str.equals("o")) {
						  final JFileChooser fc = new JFileChooser();
				         //   fc.showOpenDialog(fc); 
				            int option = fc.showOpenDialog(j);  
				            if(option == JFileChooser.APPROVE_OPTION){  
				            if(fc.getSelectedFile()!=null){  
				            File theFileToOpen = fc.getSelectedFile();
				            try {
								dispose();
								j.dispose();
								new FileOpener(theFileToOpen);
							} catch (FileNotFoundException e) {
								String message = "There is no such file to open!";
							    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
							            JOptionPane.ERROR_MESSAGE);
							}
				            }
				            }
					  }
					  if (str.equals("s")) {
						  final JFileChooser fc = new JFileChooser();
					            int option = fc.showSaveDialog(j);  
					            if(option == JFileChooser.APPROVE_OPTION){  
					            if(fc.getSelectedFile()!=null){  
					            File theFileToSave = fc.getSelectedFile();
									try {
										new FileSaver(theFileToSave,q);
									} catch (IOException e) {
										String message = "There is no such file to open!";
									    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
									            JOptionPane.ERROR_MESSAGE);
									}

					            }
					            }
					  }
					  
				}

				@Override
				public void keyReleased(KeyEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					
				}	
				
			}
			);

		addMouseListener( new MouseAdapter() {  
			public void mousePressed(MouseEvent e) {
				int offsetx =e.getX()-sizex-8; 
				int offsety=Math.abs(e.getY()-sizey-30);
				if (Math.signum(e.getY()-sizey-30)==1) {
					offsety=-Math.abs(e.getY()-sizey-30);
				}
				Point P=new Point (offsetx,offsety);	
				boolean NeedToAdd=true;
				Iterator<Point> it2= q.iterator();
				while (it2.hasNext()) {
					Point temp;
					temp=it2.next();
					if ((P.x==temp.x) && (P.y==temp.y)) {NeedToAdd=false;;break;}	
				}
				
				ChangeCoord(Copy);
				if (NeedToAdd) {
					q.add(P);
					label1.setText("Last Clicked Point => X:"+P.x+" |Y:"+P.y);
					Triangulation Algo = new Triangulation (q,AdjacentEdgesF);
					Algo.SolveDelone(History, Delane,P,Left,Right);
					Copy.clear();
					
					Iterator<Triangle> it1= Delane.iterator();
					while (it1.hasNext()) {
						Triangle NextT=it1.next();
						if ((NextT.a!=BotLeft) && (NextT.b!=BotLeft) && (NextT.c!=BotLeft) && (NextT.a!=BotRight) && (NextT.b!=BotRight) && (NextT.c!=BotRight) && (NextT.a!=TopLeft) && (NextT.b!=TopLeft) && (NextT.c!=TopLeft) && (NextT.a!=TopRight) && (NextT.b!=TopRight) && (NextT.c!=TopRight)	) {
									Copy.add(NextT); 
						}
					}
					Iterator<Edge> it3= AdjacentEdgesF.keySet().iterator();
					AdjacentEdges.clear();
					while (it3.hasNext()) {
						Edge NextT=it3.next();
						if ((NextT.a!=BotLeft) && (NextT.b!=BotLeft)  && (NextT.a!=BotRight) && (NextT.b!=BotRight)  && (NextT.a!=TopLeft) && (NextT.b!=TopLeft)  && (NextT.a!=TopRight) && (NextT.b!=TopRight) ) {
							Triangle NextTT=AdjacentEdgesF.get(NextT);
							if ((NextTT.a!=BotLeft) && (NextTT.b!=BotLeft) && (NextTT.c!=BotLeft) && (NextTT.a!=BotRight) && (NextTT.b!=BotRight) && (NextTT.c!=BotRight) && (NextTT.a!=TopLeft) && (NextTT.b!=TopLeft) && (NextTT.c!=TopLeft) && (NextTT.a!=TopRight) && (NextTT.b!=TopRight) && (NextTT.c!=TopRight)	) {
								Edge NextTemp=new Edge (NextT.b,NextT.a);
								Point ap1=new Point (NextTT.a.x,NextTT.a.y);
								Point bp1=new Point (NextTT.b.x,NextTT.b.y);
								Point cp1=new Point (NextTT.c.x,NextTT.c.y);
								Triangle NextTTemp=new Triangle (ap1,bp1,cp1);
								AdjacentEdges.put(NextTemp, NextTTemp);
							}		
						}
					}
				}
				
				
				setPreferredSize(new Dimension (2*sizex,2*sizey));
				setBackground(Color.black);
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				ChangeCoord(Copy);
				label3.setText("Number of Triangles in Triangulation = "+Copy.size());
				label2 .setText("Number of Dots in Triangulation = "+q.size());
				validate();
				repaint();
				pack();
				setVisible(true);
			}
		}		
);
			
			
	
		   	  checkBox.addActionListener(new ActionListener() {
		          public void actionPerformed(ActionEvent e) {
		              AbstractButton abstractButton = (AbstractButton)e.getSource();
		              boolean selected = abstractButton.getModel().isSelected();
		              String newLabel="";
		              BasicJPanel panel1;
		              BasicJPanel panel2;
		              if (selected) {
		            	  newLabel= "Delaunay triangulation On";
		            	  getContentPane().removeAll();
		            	 
		            	  if (checkBox1.isSelected()) {
		            		  if (checkBox2.isSelected()) {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,7);   
		            			  // label1.setText("Current States:Triangulation - ON,Circus-ON,Voronogo-ON");
		            		  }else {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,4);
		            			  // label1.setText("Current States:Triangulation - ON,Circus-ON,Voronogo-OFF");
		            		  }
		            	  }else {
		            		  if (checkBox2.isSelected()) {		     
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,6);
		            			  // label1.setText("Current States:Triangulation - ON,Circus-OFF,Voronogo-ON");
		            		  }else {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,1);
		            			  // label1.setText("Current States:Triangulation - ON,Circus-OFF,Voronogo-OFF");
		            		  }
		            	  }
		            	   
		            	  getContentPane().add(panel1);
		            	  panel1.setForeground(Color.black);
		            	  panel1.setOpaque(true);
		            	  validate();
		            	  repaint();
		            	  pack();
		            	  setVisible(true);
		              	}else {
		              		newLabel= "Delaunay triangulation Off";
		              		getContentPane().removeAll();
		              		if (checkBox1.isSelected()) {
		              			if (checkBox2.isSelected()) {
		              				panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,5);   
		              				//label1.setText("Current States:Triangulation - OFF,Circus-ON,Voronogo-ON");
		              			}else {
		              				panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,2);
		              				//label1.setText("Current States:Triangulation - OFF,Circus-ON,Voronogo-OFF");
		              			}
		              			}else {
		              				if (checkBox2.isSelected()) {
		              					panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,3);
		              					// label1.setText("Current States:Triangulation - OFF,Circus-OFF,Voronogo-ON");
		              				}else {
		              					panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,0);
		              					// label1.setText("Current States:Triangulation - OFF,Circus-OFF,Voronogo-OFF");
		              				}
		              			} 
		              			getContentPane().add(panel2);
		              			panel2.setForeground(Color.black);
		              			panel2.setOpaque(true);
		              			validate();
		              			repaint();
		              			pack();
		              			setVisible(true);
		              	}
		              	abstractButton.setText(newLabel);
		          	}
		   	  } );

		   	  
		   	  
		   	  checkBox1.addActionListener(new ActionListener() {
		   		  public void actionPerformed(ActionEvent e) {
		   			  AbstractButton abstractButton = (AbstractButton)e.getSource();
		   			  boolean selected = abstractButton.getModel().isSelected();
		   			  String newLabel="";
		              BasicJPanel panel1;
		              BasicJPanel panel2;
		              if (selected) {
		            	  newLabel= "Circumcircles On";
		            	  getContentPane().removeAll();
		            	  if (checkBox.isSelected()) {
		            		  if (checkBox2.isSelected()) {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,7); 
		            			  // label1.setText("Current States:Triangulation - ON,Circus-ON,Voronogo-ON");
		            		  }else {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,4);
		            			  // label1.setText("Current States:Triangulation - ON,Circus-ON,Voronogo-OFF");
		            		  }
		            	  }else {
		            		  if (checkBox2.isSelected()) {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,5);
		            			  // label1.setText("Current States:Triangulation - OFF,Circus-ON,Voronogo-ON");
		            		  }else {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,2);
		            			  // label1.setText("Current States:Triangulation - OFF,Circus-ON,Voronogo-OFF");
		            		  }
		            	  } 			              
		            	  getContentPane().add(panel1);
		            	  //setContentPane(panel);
		            	  panel1.setForeground(Color.black);
		            	  panel1.setOpaque(true);
		            	  validate();
		            	  repaint();
		            	  pack();
		            	  setVisible(true);
		              	}else {
		              		newLabel= "Circumcircles Off";
		              		getContentPane().removeAll();
		              		if (checkBox.isSelected()) {
		              			if (checkBox2.isSelected()) {
		              				panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,6); 
		              				// label1.setText("Current States:Triangulation - ON,Circus-OFF,Voronogo-ON");
		              			}else {
		              				panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,1);
		              				//  label1.setText("Current States:Triangulation - ON,Circus-OFF,Voronogo-OFF");
		              			}
		              		}else {
		              			if (checkBox2.isSelected()) {
		              				panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,3);
		              				// label1.setText("Current States:Triangulation - OFF,Circus-OFF,Voronogo-ON");
		              			}else {
		              				panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,0);
		              				// label1.setText("Current States:Triangulation - OFF,Circus-OFF,Voronogo-OFF");
		              			}
		              		}
		              		getContentPane().add(panel2);
		              		panel2.setForeground(Color.black);
		              		panel2.setOpaque(true);
		              		validate();
		              		repaint();
		            		pack();
							setVisible(true);
		              	}
		              	abstractButton.setText(newLabel);
		   		  }
		   	  } );
		   	 
		   	 
		   	  
		   	  checkBox2.addActionListener(new ActionListener() {
		   		  public void actionPerformed(ActionEvent e) {
		   			  AbstractButton abstractButton = (AbstractButton)e.getSource();
		              boolean selected = abstractButton.getModel().isSelected();
		              String newLabel="";
		              BasicJPanel panel1;
		              BasicJPanel panel2;
		              if (selected) {
		            	  newLabel= "Voronoi diagram On";
		            	  getContentPane().removeAll();
		            	  if (checkBox.isSelected()) {
		            		  if (checkBox1.isSelected()) {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,7); 
		            			  //label1.setText("Current States:Triangulation - ON,Circus-ON,Voronogo-ON");
		            		  }else {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,6);
		            			  // label1.setText("Current States:Triangulation - ON,Circus-OFF,Voronogo-ON");
		            		  }
		            	  }else {
		            		  if (checkBox1.isSelected()) {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,5);
		            			  // label1.setText("Current States:Triangulation - OFF,Circus-ON,Voronogo-ON");
		            		  }else {
		            			  panel1 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,3); //3
		            			  //label1.setText("Current States:Triangulation - OFF,Circus-OFF,Voronogo-ON");
		            		  }
		            	  }
		            	  getContentPane().add(panel1);
		            	  //setContentPane(panel);
		            	  panel1.setForeground(Color.black);
		            	  panel1.setOpaque(true);
		            	  validate();
		            	  repaint();
		            	  pack();
		            	  setVisible(true);
		              }else {
		            	  newLabel= "Voronoi diagram Off";
		            	  getContentPane().removeAll();
		            	  if (checkBox.isSelected()) {
		            		  if (checkBox1.isSelected()) {
		            			  panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,4); 
		            			  //label1.setText("Current States:Triangulation - ON,Circus-ON,Voronogo-OFF");
		            		  }else {
		            			  panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,1);
		            			  //label1.setText("Current States:Triangulation - ON,Circus-OFF,Voronogo-OFF");
		            		  }
		            	  }else {
		            		  if (checkBox1.isSelected()) {
		            			  panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,2);
		            			  // label1.setText("Current States:Triangulation - OFF,Circus-ON,Voronogo-OFF");
		            		  }else {
		            			  panel2 = new BasicJPanel (q,Copy,AdjacentEdges,sizex,sizey,0);
		            			  // label1.setText("Current States:Triangulation - OFF,Circus-OFF,Voronogo-OFF");
		            		  }
		            	  	}
		            	  getContentPane().add(panel2);
		            	  panel2.setForeground(Color.black);
		            	  panel2.setOpaque(true);
		            	  validate();
		            	  repaint();
		            	  pack();
		            	  setVisible(true);
		              }
		              abstractButton.setText(newLabel);
		   		  }
		   	  } );
	}
	}


	 
	static void ChangeCoord (Set <Triangle> ST) {
		Iterator<Triangle> it1 = ST.iterator();
		while (it1.hasNext()) {
		    Triangle poly = it1.next();
		    poly.ChC();
		}

	}
}
	

