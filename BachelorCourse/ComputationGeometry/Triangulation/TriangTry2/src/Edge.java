import java.awt.Point;


public class Edge {
	Point a;
	Point b;
	
	//Constructor
	Edge (Point x, Point y) {
		a=x;
		b=y;
	}
	
	@Override								//Overriding equals() for known purpose ;)
	public boolean equals (Object e) {
		Edge edge=(Edge)e;
		if ((this.a.equals(edge.a)) &&((this.b.equals(edge.b)))) {
			return true;
		}
		return false;	
	}
	
	@Override
	public int hashCode () {				//Overriding hashcode() for using in HashMaps
		int hash=1; 
		Edge ed=(Edge)this;
		hash = hash * 31 + ed.a.hashCode()*2 +  ed.b.hashCode()*3;
		return hash;
	}
	
	public void PrintEdge () {				//Just Printing for debug
		System.out.println("-------");
		System.out.println("" + a + "  "+ b);
		System.out.println("-------");
	}
	
	public boolean isLeft(Point c){
	     return ((a.x - c.x)*(b.y - c.y) - (a.y - c.y)*(b.x - c.x)) > 0;
	}
	
}
