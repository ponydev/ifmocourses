import java.awt.*;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JComponent;


@SuppressWarnings("serial")
public class BasicJPanel extends JComponent{
		Point P;
		int sizex;
		int sizey;
		Set<Point> SP;
		Set<Triangle> ST;
		Triangle T;
		int mode;
		HashMap <Edge,Triangle> AdjacentEdges;
		
		// Panel Constructor
	public BasicJPanel(Set<Point> PointSet,Set<Triangle> Triangles,HashMap <Edge,Triangle>  Adj,int x,int y,int z) {
		SP=PointSet;
		sizex=x;         
		sizey=y;
		ST=Triangles;
		mode=z;
		AdjacentEdges=Adj;
	}

		
	public void paint(Graphics g) {
		
		g.translate(sizex, sizey);                  //translating center of coords to sizex,sizey
		Picture Pic=new Picture (sizex,sizey);		//new Picture
	
		if ((mode==1)||(mode==4)||(mode==6)||(mode==7)){
			Pic.drawTriangulation(g,SP,ST);			//mods with Triangulation On
		}
		if ((mode==2)||(mode==4)||(mode==5)||(mode==7)){
			Pic.drawPoints(g, SP);  				//mods with Cirumcircles On
			Pic.drawCircumCircles(g,ST);
		}
		if ((mode==3)||(mode==5)||(mode==6)||(mode==7)){
			Pic.drawPoints(g, SP);					//mods with Voronoy On
			Pic.drawVoronoy(g, AdjacentEdges);
		}
		
	}

	
}