import java.awt.Point;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Triangulation {
	final static Point BotRight=new Point(10000,10000);
	final static Point BotLeft=new Point(-10000,10000);
	final static Point TopLeft=new Point(-10000,-10000);
	final static Point TopRight=new Point(10000,-10000);
	Set <Point> InputDots;
	Point InputDot;
	int sizex;
	int sizey;
	HashMap<Edge, Triangle> AdjacentEdges;
	
	Triangulation(Set <Point> a,HashMap<Edge, Triangle> m) {
		InputDots = a;
		AdjacentEdges=m;
	}
	
	//Find Triangle in Dag of triangle history
	Triangle FindTriangle (HashMap <Triangle,Set<Triangle>> History,Triangle check,Point a) {
		Set<Triangle> OutEdges=History.get(check);
		Triangle e;
		Iterator<Triangle> it=OutEdges.iterator();	 
		while (it.hasNext()) {
			e=it.next();
			if (e.IsInsideTriangle(a)!=1){
				check=FindTriangle(History,e,a);
			}
		}	
		return check;
	}
	
	//Check if point lie on a line
	boolean checkPointLieOnLine (Point a1,Point a2,Point p){
		if (a1.x==a2.x) {
			if (a1.x==p.x) {
				return true;
			}else {
				return false;
			}
		}
		if (a1.y==a2.y) {

			if (a1.y==p.y) {
				return true;
			}else {
				return false;
			}
		}
		long[][] mat = new long[3][3];
		mat[0][0]=a1.x;
		mat[0][1]=a1.y;
		mat[0][2]=1;
		mat[1][0]=a2.x;
		mat[1][1]=a2.y;
		mat[1][2]=1;
		mat[2][0]=p.x;
		mat[2][1]=p.y;
		mat[2][2]=1;
		if (new Triangle(a1,a2,p).determinant(mat)==0){
			return true;
		}else {
			return false;
		}
	}
	
	//Find Adjacent Triangle to triangle 
	Triangle AdjacentTriangle (Point a,Point e1,Point e2,Triangle InTriangle,Set<Triangle>Delane) {
		if (AdjacentEdges.containsKey(new Edge(e1,e2))) {
			if (InTriangle.equal(AdjacentEdges.get(new Edge(e1,e2)))) {
				if (AdjacentEdges.containsKey(new Edge(e2,e1))) {
					return AdjacentEdges.get(new Edge(e2,e1));
				}
			}else {
				return AdjacentEdges.get(new Edge(e1,e2));
			}
		}
		if (AdjacentEdges.containsKey(new Edge(e2,e1))) {
			if (InTriangle.equal(AdjacentEdges.get(new Edge(e2,e1)))) {
				if (AdjacentEdges.containsKey(new Edge(e1,e2))) {
					return AdjacentEdges.get(new Edge(e1,e2));
				}
			}else {
				return AdjacentEdges.get(new Edge(e2,e1));
			}
		}
		 String message = "Very strange just happens..please save dots on what you were working and send to me on viktor.killer@gmail.com.";  
		  JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
		            JOptionPane.ERROR_MESSAGE);
		return new Triangle(a,a,a);		
	}
	
	//Check having adjacent triangle
	boolean HaveAdjacentTriangle (Point a,Point e1,Point e2,Triangle InTriangle,Set<Triangle>Delane) {
		if (AdjacentEdges.containsKey(new Edge(e1,e2))) {
			if (InTriangle.equal(AdjacentEdges.get(new Edge(e1,e2)))) {
				if (AdjacentEdges.containsKey(new Edge(e2,e1))) {
					return true;
				}else {
					return false;
				}
			}else {
				return true;
			}
		}
		if (AdjacentEdges.containsKey(new Edge(e2,e1))) {
			if (InTriangle.equal(AdjacentEdges.get(new Edge(e2,e1)))) {
				if (AdjacentEdges.containsKey(new Edge(e1,e2))) {
					return true;
				}else {
					return false;
				}
			}else {
				return true;
			}
		}
		return false;
	}

	//Legalization process
	void Legalize(Point a,Point e1,Point e2,Triangle InTriangle,Set<Triangle>Delane,HashMap <Triangle,Set<Triangle>> History){
		if (HaveAdjacentTriangle(a, e1, e2, InTriangle, Delane)) {   //VKLUCHIT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			Triangle t1 = AdjacentTriangle(a,e1,e2,InTriangle,Delane);
			InTriangle.TurnCCW();
			t1.TurnCCW();
			Point p1=t1.ThirdPoint(e1,e2);
			if (InTriangle.IsInsideCircumcircle(p1)==-1) {
				Triangle Part1=new Triangle (a,e1,p1);
				Triangle Part2=new Triangle (a,p1,e2);
				Part1.TurnCCW();
				Part2.TurnCCW();
				flip (a,p1,InTriangle,t1,e1,e2,Delane,History,Part1,Part2);
				Legalize(a,e1,p1,Part1,Delane,History);
				Legalize(a,p1,e2,Part2,Delane,History);
			}
		}	
	}
	
	//flipping process
	void flip(Point a,Point p1,Triangle t,Triangle t1,Point e1,Point e2,Set<Triangle>Delane,HashMap <Triangle,Set<Triangle>> History,Triangle Part1,Triangle Part2){
		Delane.remove(t);
		Delane.remove(t1);
		History.put(Part1, new HashSet<Triangle>());
		History.put(Part2, new HashSet<Triangle>());
		History.get(t).add(Part1);
		History.get(t).add(Part2);
		History.get(t1).add(Part1);
		History.get(t1).add(Part2);
		Delane.add(Part1);
		Delane.add(Part2);
		AdjacentEdges.keySet().remove(new Edge(e2,e1));
		AdjacentEdges.keySet().remove(new Edge(e1,e2));
		AdjacentEdges.put(new Edge(a,e1),Part1);
		AdjacentEdges.put(new Edge(e2,a),Part2);
		AdjacentEdges.put(new Edge(e1,p1),Part1);
		AdjacentEdges.put(new Edge(p1,e2),Part2);
		AdjacentEdges.put(new Edge(a,p1),Part2);
		AdjacentEdges.put(new Edge(p1,a),Part1);
	}

	//add Point to Delone
	void addToDelone (HashMap <Triangle,Set<Triangle>> History,Set<Triangle> Delane,Triangle InTriangle,Point a) {
		if (InTriangle.CheckValidaty()) {	
			if (InTriangle.IsInsideTriangle(a)==-1)	{
				Delane.remove(InTriangle);
				InTriangle.TurnCCW();
				Triangle Part1=new Triangle (InTriangle.a,InTriangle.b,a);
				Triangle Part2=new Triangle (a,InTriangle.b,InTriangle.c);
				Triangle Part3=new Triangle (InTriangle.a,a,InTriangle.c);
				Part1.TurnCCW();
				Part2.TurnCCW();
				Part3.TurnCCW();
				History.put(Part1, new HashSet<Triangle>());
				History.put(Part2, new HashSet<Triangle>());
				History.put(Part3, new HashSet<Triangle>());
				History.get(InTriangle).add(Part1);
				History.get(InTriangle).add(Part2);
				History.get(InTriangle).add(Part3);
				Delane.add(Part1);
				Delane.add(Part2);
				Delane.add(Part3);				
				AdjacentEdges.put(new Edge (InTriangle.a,InTriangle.b),Part1);
				AdjacentEdges.put(new Edge (InTriangle.b,InTriangle.c),Part2);
				AdjacentEdges.put(new Edge (InTriangle.c,InTriangle.a),Part3);
				AdjacentEdges.put(new Edge (InTriangle.b,a),Part1);
				AdjacentEdges.put(new Edge (a,InTriangle.a),Part1);
				AdjacentEdges.put(new Edge (a,InTriangle.b),Part2);
				AdjacentEdges.put(new Edge (InTriangle.c,a),Part2);
				AdjacentEdges.put(new Edge (InTriangle.a,a),Part3);
				AdjacentEdges.put(new Edge (a,InTriangle.c),Part3);	
				Legalize(a,InTriangle.a,InTriangle.b,Part1,Delane,History);
				Legalize(a,InTriangle.b,InTriangle.c,Part2,Delane,History);
				Legalize(a,InTriangle.c,InTriangle.a,Part3,Delane,History);
			}else {
				if (InTriangle.IsInsideTriangle(a)==0) {
					Point e1=new Point ();
					Point e2=new Point ();
					Point c=new Point ();
					InTriangle.TurnCCW();
					if(checkPointLieOnLine(InTriangle.a,InTriangle.b,a)) {
						e1=InTriangle.a;
						e2=InTriangle.b;
						c=InTriangle.c;
					}else {
						if(checkPointLieOnLine(InTriangle.b,InTriangle.c,a)) {
							e1=InTriangle.b;
							e2=InTriangle.c;
							c=InTriangle.a;
						}else {
							if(checkPointLieOnLine(InTriangle.c,InTriangle.a,a)) {
								e1=InTriangle.c;
								e2=InTriangle.a;
								c=InTriangle.b;
							}
						}
					}
					Triangle temps=AdjacentTriangle (c,e1,e2,InTriangle,Delane);
					temps.TurnCCW();
					InTriangle.TurnCCW();
					Point F=temps.ThirdPoint(e1,e2);
					if ((F.x!=-100000)&&(F.y!=-100000)) {
						Triangle Part1=new Triangle (c,e1,a);
						Triangle Part2=new Triangle (e2,c,a);
						Triangle Part3=new Triangle (F,e2,a);
						Triangle Part4=new Triangle (e1,F,a);
						Delane.remove(InTriangle);
						Delane.remove(temps);
						Part1.TurnCCW();
						Part2.TurnCCW();
						Part3.TurnCCW();
						Part4.TurnCCW();
						History.put(Part1, new HashSet<Triangle>());
						History.put(Part2, new HashSet<Triangle>());
						History.put(Part3, new HashSet<Triangle>());
						History.put(Part4, new HashSet<Triangle>());
						History.get(InTriangle).add(Part1);
						History.get(InTriangle).add(Part2);
						History.get(temps).add(Part3);
						History.get(temps).add(Part4);
						Delane.add(Part1);
						Delane.add(Part2);
						Delane.add(Part3);
						Delane.add(Part4);
						AdjacentEdges.remove(new Edge(e1,e2));
						AdjacentEdges.remove(new Edge(e2,e1));
						AdjacentEdges.put(new Edge(c,e1),Part1);
						AdjacentEdges.put(new Edge(e2,c),Part2);
						AdjacentEdges.put(new Edge(e1,F),Part4);
						AdjacentEdges.put(new Edge(F,e2),Part3);
					
						AdjacentEdges.put(new Edge(a,c),Part1);
						AdjacentEdges.put(new Edge(c,a),Part2);
					
						AdjacentEdges.put(new Edge(a,e2),Part2);
						AdjacentEdges.put(new Edge(e2,a),Part3);
					
						AdjacentEdges.put(new Edge(a,F),Part3);
						AdjacentEdges.put(new Edge(F,a),Part4);
			
						AdjacentEdges.put(new Edge(a,e1),Part4);
						AdjacentEdges.put(new Edge(e1,a),Part1);

						Legalize(a, c,e1,Part1,Delane,History);
						Legalize(a,e2,c,Part2,Delane,History);
						Legalize(a,F,e2,Part3,Delane,History);
						Legalize(a,e1,F,Part4,Delane,History);	
					}
				}
				if (InTriangle.IsInsideTriangle(a)==1) {
				//errror
				}
			}
		}
	}
	
	//Solve Delone for set of dots
	void SolveDelone (HashMap <Triangle,Set<Triangle>> History, Set<Triangle> Delane,Triangle Left,Triangle Right) {
			Iterator<Point> it= InputDots.iterator();
			Delane.add(Left);
			Delane.add(Right);
			
			History.put(Left, new HashSet<Triangle>());
			History.put(Right, new HashSet<Triangle>());
	
			while (it.hasNext()) {
				Point Next=it.next();
				if (Left.IsInsideTriangle(Next)==-1){
					addToDelone (History,Delane,FindTriangle(History,Left,Next),Next);
				}
				if (Right.IsInsideTriangle(Next)==-1){
					addToDelone (History,Delane,FindTriangle(History,Right,Next),Next);
				}
				if ((Right.IsInsideTriangle(Next)!=-1)&&(Left.IsInsideTriangle(Next)!=-1)){
					addToDelone (History,Delane,FindTriangle(History,Right,Next),Next);
				}
			}
	}
	
	//Solve Delone for +1 dot
		void SolveDelone (HashMap <Triangle,Set<Triangle>> History, Set<Triangle> Delane,Point P,Triangle Left,Triangle Right) {

				Point Next = new Point();
				Next=P;
				if (Left.IsInsideTriangle(Next)==-1){
					addToDelone (History,Delane,FindTriangle(History,Left,Next),Next);
				}
				if (Right.IsInsideTriangle(Next)==-1){
					addToDelone (History,Delane,FindTriangle(History,Right,Next),Next);
				}
				if ((Right.IsInsideTriangle(Next)!=-1)&&(Left.IsInsideTriangle(Next)!=-1)){
					addToDelone (History,Delane,FindTriangle(History,Right,Next),Next);
				}
		}
	
}