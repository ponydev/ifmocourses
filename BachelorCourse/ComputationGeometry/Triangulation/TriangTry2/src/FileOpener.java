import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FileOpener {
	FileOpener(File InputFile) throws FileNotFoundException { 
		Scanner s = null;
		int x=0,y=0;
		boolean vx=false;
		HashSet<Point> q=new HashSet<Point>();
		try {
			s = new Scanner(new BufferedReader(new FileReader(InputFile.getAbsolutePath())));
			while (s.hasNext()) {
				String str=s.next();    
				if (str.charAt(0)=='x') {
					x=Integer.parseInt(str.substring(2));
					vx=true;
				}else {
					if ((str.charAt(0)=='y')&&(vx)) {
						y=Integer.parseInt(str.substring(2));
						vx=false;
						q.add(new Point (x,y));
					}else {
						String message = "Bad openfile format!";
					    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
					            JOptionPane.ERROR_MESSAGE);
					    break;
					}
				}  
			}
			s.close();
			new SetUpDisplay((Set <Point>)q,0);
		}catch (FileNotFoundException fnfe){
			String message = "There is no such file to open!";
		    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
		            JOptionPane.ERROR_MESSAGE);
		    new SetUpDisplay((Set <Point>)q,0);
		}

	}
}
