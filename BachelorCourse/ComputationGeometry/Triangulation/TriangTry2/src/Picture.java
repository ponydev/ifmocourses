import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;


public class Picture {
	int sizex;
	int sizey;
	
	//Constructure:size of a picture
	Picture(int sizex2, int sizey2) {
		sizex=sizex2;
		sizey=sizey2;
	}

	//PolygonDrawer
	public void drawPoly( Graphics g, Polygon poly ) {
		g.setColor(Color.green);
		g.drawPolygon( poly );
	}
	
	//PointDrawer
	public void drawPoint( Graphics g, Point P ) {
		g.setColor(Color.red);
		g.fillOval(P.x-2, -P.y-2, 4, 4);
	}
	
	//PointsDrawer
	public void drawPoints( Graphics g, Set<Point> SP ) {
		Iterator<Point> it = SP.iterator();
		while (it.hasNext()) {
			Point P = it.next();
			g.setColor(Color.red);
			g.fillOval(P.x-2, -P.y-2, 4, 4);
		}
	}
	
	//PointsAndTrianglesDrawer
	public void drawPandT (Graphics g, Point P ,Triangle poly ) {
		g.setColor(Color.green);
		poly.draw(g);
		g.setColor(Color.red);
		g.fillOval(P.x-2, -P.y-2, 4, 4);
	  }
	
	//TriangulationDrawer
	public void drawTriangulation (Graphics g, Set<Point> SP ,Set<Triangle> ST ) {
		g.setColor(Color.green);
		Iterator<Triangle> it1 = ST.iterator();
		while (it1.hasNext()) {
			Triangle poly = it1.next();
			poly.draw(g);
		}
		
		Iterator<Point> it2 = SP.iterator();
		while (it2.hasNext()) {
			Point P = it2.next();
			g.setColor(Color.red);
			g.fillOval(P.x-2, -P.y-2, 4, 4);
		}
	  }
	
	//CircumCirclesDrawer
	public void drawCircumCircles (Graphics g, Set<Triangle> ST) {
		Iterator<Triangle> it1 = ST.iterator();
		while (it1.hasNext()) {
			Triangle poly = it1.next();
			Point P = poly.Circumcenter();
			int r = poly.Circumradius();
			g.setColor(Color.blue);
			g.drawOval(P.x-r, -P.y-r, 2*r, 2*r);
			g.setColor(Color.pink);
			g.drawOval(P.x, -P.y, 2, 2);
		}				
	  }
	
	
	/*Principle of work
	 * If AdjacentEdges.containsKey(e1) true then it is not in convex hull so just connect centers
	 * If AdjacentEdges.containsKey(e1) false then
	 * 1)we have sharp-angled triangle then connect center and half-edge in direction from center
	 * 2)we have obtuse triangle then then connect center and half-edge in direction from half-edge
	 * 3)we have rectangular triangle then 
	 * 	a)try if point half-edge with x+1 lies in triangle and if does then take dot x=x-1000<--(random) .Then we need to our dot and third point of triangle lied on different sides of edge e.So we will choose y or -y.
	 *  b)like step a but x-1 in triangle and take x=x+1000
	 */
	
	//VoronoyDrawer
	public void drawVoronoy (Graphics g, HashMap<Edge, Triangle> AdjacentEdges) {
		g.setColor(Color.yellow);
		Edge e;
		Edge e1;
		Triangle t;
		Triangle t1;
		Entry<Edge, Triangle> k;
		Collection<Entry<Edge, Triangle>> adja=AdjacentEdges.entrySet();
		Iterator<Entry<Edge, Triangle>> it1 = adja.iterator();
		while (it1.hasNext()) {
			k=it1.next();
			e=k.getKey();
			t=k.getValue();
			e1=new Edge (e.b,e.a);
			Point Center=t.Circumcenter();
			if (AdjacentEdges.containsKey(e1)) {
				t1=AdjacentEdges.get(e1);
				Point Center1=t1.Circumcenter();	
				g.drawLine(Center.x, -Center.y, Center1.x, -Center1.y);	
			}else {
				Point z=t.ThirdPoint(e.a, e.b);
				if ((t.TupoyAngle(e,z)==1)&&(t.Distance(Center, new Point ((e.a.x+e.b.x)/2,(e.a.y+e.b.y)/2))>3)) {
					g.drawLine(Center.x, -Center.y, 100*Center.x-99*(e.a.x+e.b.x)/2,-100*Center.y+99*(e.a.y+e.b.y)/2);
				}else {
					if ((t.TupoyAngle(e,z)==-1)&&(t.Distance(Center, new Point ((e.a.x+e.b.x)/2,(e.a.y+e.b.y)/2))>3)) {
						g.drawLine(Center.x, -Center.y, 50*(e.a.x+e.b.x)-99*Center.x, -50*(e.a.y+e.b.y)+99*Center.y);
					}else {
					
						double koff;
						int x1,y1,x2,y2;
						if (e.b.x!=e.a.x) {
							koff=(double)(e.b.y-e.a.y)/(double)(e.b.x-e.a.x);
							if (koff!=0) {koff=-1/koff;}
							double bet=(double)(e.b.y+e.a.y)/2-koff*((double)(e.b.x+e.a.x)/2);
							x1=(e.a.x+e.b.x)/2-5;
							x2=(e.a.x+e.b.x)/2+5;
							y1=(int)(koff*(x1)+bet);
							y2=(int)(koff*(x2)+bet);
							if (e.a.x==e.b.x) {
								x1=(x1+x2)/2;
								if ((y1-z.y)>0) {
									y1=y1+1000;
								}else {
									y1=y1-1000;
								}
								g.drawLine(Center.x, -Center.y, x1, -y1);
							}else {
								if (e.a.y==e.b.y) {
									x1=(x1+x2)/2;
									if ((y1-z.y)>0) {
										y1=y1+1000;
									}else {
										y1=y1-1000;
									}
									g.drawLine(Center.x, -Center.y, x1, -y1);
								}else {
							if ((Math.sqrt((z.x-x1)*(z.x-x1)+(z.y-y1)*(z.y-y1))>(Math.sqrt((z.x-x2)*(z.x-x2)+(z.y-y2)*(z.y-y2))))) {
								x1=x1-1000;
								y1=(int)(koff*(x1)+bet);
								g.drawLine(Center.x, -Center.y, x1, -y1);
							}else {
								x2=x2+1000;
								y2=(int)(koff*(x2)+bet);
								g.drawLine(Center.x, -Center.y, x2, -y2);
							}
							}
							}
						}		
					}
				}
			}
		}
	}

	
	//Clearer
	public void clear (Graphics g) {
		g.setColor(Color.black);
	}
	  
}
