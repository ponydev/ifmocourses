import java.awt.Point;
import java.util.HashSet;
import java.util.Set;


public class Main {
	public static void main(String[] args) {
		Set<Point> q=new HashSet<Point>();
		boolean testmode=false;
		for (int i=0;i<args.length;i++) {
			if (args[i].equals("--test")) {
				testmode=true;
				int n=0;
				try 
				{
					n=new Integer (args[i+1]).intValue();
					new SetUpDisplay(q,n);
			    }
			    catch (NumberFormatException e)
			    {
			    	System.out.println("Number of test in bad format! ");
			    }
			    catch (ArrayIndexOutOfBoundsException e)
			    {
			    	System.out.println("No arguments for testing! ");
			    }
				
			}
		}
		if (!testmode) {
			new SetUpDisplay(q,0);
		}
	}
}