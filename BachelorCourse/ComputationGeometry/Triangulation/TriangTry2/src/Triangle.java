import java.awt.*;
import java.awt.geom.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Triangle implements Shape	{
	Polygon poly;
	Point a;
	Point b;
	Point c;
	Edge first;
	Edge second;
	Edge third;
	Edge Sfirst;
	Edge Ssecond;
	Edge Sthird;

  public Triangle( Polygon p )	{
	  poly = p;
  }

  public Triangle( Point p1, Point p2, Point p3 )	{
	  poly = new Polygon();
	  poly.addPoint( p1.x, p1.y );
	  poly.addPoint( p2.x, p2.y );
	  poly.addPoint( p3.x, p3.y );
	  a=p1;
	  b=p2;
	  c=p3;
	  first = new Edge (p1,p2);
	  Sfirst = new Edge (p2,p1);
	  second = new Edge (p2,p3);
	  Ssecond = new Edge (p3,p2);
	  third = new Edge (p3,p1);
	  Sthird = new Edge (p1,p3);
  }
  /*
  public Triangle( Edge f, Edge s, Edge t )	{
  	first=f;
	second=s;
	third=t;
	  
	poly = new Polygon();
	poly.addPoint( first.a.x, first.a.y );
	poly.addPoint( second.a.x, second.a.y );
	poly.addPoint( third.a.x, third.a.y );
	a= first.a;
	b=second.a;
	c=third.a;
	    
	Sfirst= new Edge (b,a);
	Ssecond= new Edge (c,b);
	Sthird= new Edge (a,c);
  }*/
  

  
  //Printing Triangle
  void GetCoord()	{
	  int j=0; 
	  while (j < poly.npoints) { 
		  System.out.printf("x[%d]=%d\ty[%d]=%d\n", j, poly.xpoints[j], j, poly.ypoints[j]);
		  j = j + 1;
	  }
	  System.out.printf("\n");
	  poly.invalidate();   //after each manipulating with poly.points..just in case..
  }
  
  //Turn triangle counterclockwise
  void TurnCCW () {
	  if (poly.xpoints[0]*poly.ypoints[1]+poly.xpoints[1]*poly.ypoints[2]+poly.xpoints[2]*poly.ypoints[0]-poly.ypoints[1]*poly.xpoints[2]-poly.ypoints[2]*poly.xpoints[0]-poly.ypoints[0]*poly.xpoints[1]>0) {
	  }else {
		  int temp;
		  temp=poly.xpoints[1];
		  poly.xpoints[1]=poly.xpoints[2];
		  poly.xpoints[2]=temp;
		  temp=poly.ypoints[1];
		  poly.ypoints[1]=poly.ypoints[2];
		  poly.ypoints[2]=temp;
		  Point temp1;
		  temp1=c;
		  c=b;
		  b=temp1;
	
		  first = new Edge (a,c);
		  Sfirst = new Edge (c,a);
		  second = new Edge (c,b);
		  Ssecond = new Edge (b,c);
		  third = new Edge (a,b);
		  Sthird = new Edge (b,a);			
	  }
	  poly.invalidate();	
  } 
  
  //Calcultating Triangle Area
  float CalcTriArea(Point v1, Point v2, Point v3)	{
	  float det = 0.0f;
	  det = Math.abs(((v1.x - v3.x) * (v2.y - v3.y)) - ((v2.x - v3.x) * (v1.y - v3.y)));
	  return (det / 2.0f);
  }
  
  //test if a is inside triangle.Returns -1-in,0-on side,1-out
  int IsInsideTriangle(Point a) {
	  float TotalArea = CalcTriArea(this.a, this.b, this.c);
	  float Area1 = CalcTriArea(a, this.b, this.c);
	  float Area2 = CalcTriArea(a, this.a, this.c);
	  float Area3 = CalcTriArea(a, this.a, this.b);
	  if((Area1 + Area2 + Area3) > TotalArea) {
		  return 1;
	  }
	  if(((Area1 + Area2 + Area3) == TotalArea)&& (Area3==0 || Area2==0 || Area1==0)) {
		  return 0;
	  }
	  return -1;
  }
  
  //test if a is inside circumcircle of triangle.Returns -1-in,0-on circle,1-out
  int  IsInsideCircumcircle(Point p) {
	  long[][] mat = new long[3][3];
	  mat[0][0]=this.a.x-p.x;
	  mat[0][1]=this.a.y-p.y;
	  mat[0][2]=(this.a.x*this.a.x-p.x*p.x)+(this.a.y*this.a.y-p.y*p.y);
	  mat[1][0]=this.b.x-p.x;
	  mat[1][1]=this.b.y-p.y;
	  mat[1][2]=(this.b.x*this.b.x-p.x*p.x)+(this.b.y*this.b.y-p.y*p.y);
	  mat[2][0]=this.c.x-p.x;
	  mat[2][1]=this.c.y-p.y;
	  mat[2][2]=(this.c.x*this.c.x-p.x*p.x)+(this.c.y*this.c.y-p.y*p.y);
	  poly.invalidate();
	  if (determinant (mat) > 0) {
		  return -1;	
	  }
	  if (determinant (mat) < 0) {
		  return 1;	
	  }
	  return 0;	
  }
  
  //evaulating determinant of matrix
  public long determinant(long[][] mat) {
	  long result = 0;
	 
	  if(mat.length == 1) {
		  result = mat[0][0];
		  return result;
	  }
	  
	  if(mat.length == 2) {
		  result = mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
		  return result;
	  }

	  for(int i = 0; i < mat[0].length; i++) {
		  long temp[][] = new long[mat.length - 1][mat[0].length - 1];
		  	for(int j = 1; j < mat.length; j++) {
		  		System.arraycopy(mat[j], 0, temp[j-1], 0, i);
		  		System.arraycopy(mat[j], i+1, temp[j-1], i, mat[0].length-i-1);
		  	}
		  	result += mat[0][i] * Math.pow(-1, i) * determinant(temp);
	  }
	  return result;
  }
  
  //turn y-axe from up-down to down-up
  void ChC () {
	  poly.ypoints[0]=-poly.ypoints[0];
	  poly.ypoints[1]=-poly.ypoints[1];
	  poly.ypoints[2]=-poly.ypoints[2];
	  poly.invalidate();
  }
  
  //check if Triangle is Right  
  boolean CheckValidaty () {
	  if (((this.a.x==this.b.x)&&(this.a.y==this.b.y)) || ((this.a.x==this.c.x)&&(this.a.y==this.c.y)) || ((this.b.x==this.c.x)&&(this.b.y==this.c.y))) {
		  return false;
	  }
	  return true;
  }
  
  //Distance among 2 dots  
  double Distance (Point p1,Point p2) {
	  return (Math.sqrt((long)((p2.x-p1.x)*(p2.x-p1.x))+(long)((p2.y-p1.y)*(p2.y-p1.y))));
  }
  
  //Circumcircle radius
  int Circumradius (){
	  double x=Distance(a,b);
	  double y=Distance(b,c);
	  double z=Distance(c,a);
	  double p=(x+y+z);
	  return (int)((x*y*z)/((Math.sqrt(p*(p-2*x)*(p-2*y)*(p-2*z)))));
  }
  
  //Circumcircle Center
  Point Circumcenter () {
	  Point Center=new Point();
	  
	  double mid1x = (double)((double)(a.x+b.x)/2);
	  double mid1y = (double)((double)(a.y+b.y)/2);
	  double mid2x = (double)((double)(b.x+c.x)/2);
	  double mid2y = (double)((double)(b.y+c.y)/2);
	  double deltax1 = a.x-b.x;
	  double deltay1 = a.y-b.y;
	  double deltax2 = b.x-c.x;
	  double deltay2 = b.y-c.y;
	  double kt1=0;
	  double kt2=0;
	  double k1=0;
	  double k2=0;
	  double b1=0 ;
	  double b2=0 ;
	  double Cx = 0;
	  double Cy = 0;
	  if ((deltax1!=0) && (deltax2!=0) && (deltay1!=0) && (deltay2!=0)) {
		  kt1 = deltay1/deltax1;
		  k1 = -1/kt1;
		  kt2 = deltay2/deltax2;
		  k2 = -1/kt2;
		  b1 = -k1*mid1x+mid1y;
		  b2 = -k2*mid2x+mid2y;
		  Cx = (b1-b2)/(k2-k1);
		  Cy = k1*Cx+b1;
	  } else {
		  if (((deltax1==0) && (deltay2==0))||((deltax2==0) && (deltay1==0))) {
			  Cx=(a.x+c.x)/2;
			  Cy=(a.y+c.y)/2;
		  }else {
			  if ((deltax1==0) && (deltax2!=0)) {
				  k1 = 0;
				  kt2 = deltay2/deltax2;
				  k2 = -1/kt2;
				  b1 = mid1y;
				  b2 = -k2*mid2x+mid2y;
				  Cx = (b1-b2)/(k2-k1);
				  Cy=mid1y;
			  }else {
				  if ((deltay1==0) && (deltay2!=0)) {
					  k1 = 0;
					  kt2 = deltay2/deltax2;
					  k2 = -1/kt2;
					  b2 =  -k2*mid2x+mid2y;
					  Cx=mid1x;
					  Cy = k2*Cx+b2;

				  }else {
					  if ((deltax1!=0) && (deltax2==0)) {
						  k2 = 0;
						  kt1 = deltay1/deltax1;
						  k1 = -1/kt1;
						  b1 = -k1*mid1x+mid1y;
						  b2 = mid2y;
						  Cx = (b1-b2)/(k2-k1);
						  Cy=mid2y;
					  }else {
						  if ((deltay1!=0) && (deltay2==0)) {
							  k2 = 0;
							  kt1 = deltay1/deltax1;
							  k1 = -1/kt1;
							  b1 =  -k1*mid1x+mid1y;
							  Cx=mid2x;
							  Cy = k1*Cx+b1;
						  }
					  }
				  }
			  }
		  }
	  }	
	  if (((deltax1==0) && (deltay1==0))||(deltax2==0) && (deltay2==0) || (deltax1==0) && (deltax2==0) || (deltay1==0) && (deltay2==0))  {	  
	  }else{
		  Center.x=(int)Cx;
		  Center.y=(int)Cy;
	  }
	  return Center;
  }
  
  //equal Triangles
  boolean equal (Triangle gor) {
	  if (this.a==gor.a) {
		  if (this.b==gor.b) {
			  if (this.c==gor.c) {
				  return true;
			  }
		  }
	  }
	  if (this.a==gor.a) {
		  if (this.b==gor.c) {
			  if (this.c==gor.b) {
				  return true;
			  }
		  }
	  }
	  if (this.a==gor.b) {
		  if (this.b==gor.a) {
			  if (this.c==gor.c) {
				  return true;
			  }
		  }
	  }
	  if (this.a==gor.b) {
		  if (this.b==gor.c) {
			  if (this.c==gor.a) {
				  return true;
			  }
		  }
	  }
	  if (this.a==gor.c) {
		  if (this.b==gor.b) {
			  if (this.c==gor.a) {
				  return true;
			  }
		  }
	  }	
	  if (this.a==gor.c) {
		  if (this.b==gor.a) {
			  if (this.c==gor.b) {
				  return true;
			  }
		  }
	  }
	  return false;
}
  
  //Return third point of Triangle
  Point ThirdPoint (Point m,Point n) {
	  if ((a.x==m.x)&&(a.y==m.y)) {
		  if ((b.x==n.x)&&(b.y==n.y)) {
			  return c;
		  }
		  if ((c.x==n.x)&&(c.y==n.y)) {
			  return b;
		  }
	  }
	  if ((a.x==n.x)&&(a.y==n.y)) {
		  if ((b.x==m.x)&&(b.y==m.y)) {
			  return c;
		  }
		  if ((c.x==m.x)&&(c.y==m.y)) {
			  return b;
		  }
	  }	
	  if ((b.x==n.x)&&(b.y==n.y)) {
		  if ((a.x==m.x)&&(a.y==m.y)) {
			  return c;
		  }
		  if ((c.x==m.x)&&(c.y==m.y)) {
			  return a;
		  }
	  }
	  if ((b.x==m.x)&&(b.y==m.y)) {
		  if ((a.x==n.x)&&(a.y==n.y)) {
			  return c;
		  }
		  if ((c.x==n.x)&&(c.y==n.y)) {
			  return a;
		  }
	  }	
	  if ((c.x==n.x)&&(c.y==n.y)) {
		  if ((a.x==m.x)&&(a.y==m.y)) {
			  return b;
		  }
		  if ((b.x==m.x)&&(b.y==m.y)) {
			  return a;
		  }
	  }
	  if ((c.x==m.x)&&(c.y==m.y)) {
		  if ((a.x==n.x)&&(a.y==n.y)) {
			  return b;
		  }
		  if ((b.x==n.x)&&(b.y==n.y)) {
			  return a;
		  }
	  }
	  String message = "Very strange just happens..please save dots on what you were working and send to me on viktor.killer@gmail.com.";  
	  JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
	            JOptionPane.ERROR_MESSAGE);
	  return new Point (100000,100000);
  }

  //Return -1-if angle anti Edge e - <90,0=90,>90otherwise
  public int TupoyAngle (Edge e,Point c) {
	  double a=Distance(e.a,e.b);
	  double b=Distance(e.b,c);
	  double k=Distance(e.a,c);
	  if (((int)(k*k)+(int)(b*b)==(int)(a*a))) {return 0;};
	  double cos = (b*b+k*k-a*a)/(2*b*k);
	  if (cos<-0.087) {return 1;}  //0.087
	  if (cos>0.087) {return -1;}
	  return 0;
	//  String message = "Very strange just happens..please save dots on what you were working and send to me on viktor.killer@gmail.com.";  
	// JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
	 //           JOptionPane.ERROR_MESSAGE);
	 // System.exit(0);
	  //return 0;
  }
  
  //Draw Polygon method
  public void draw(Graphics g)	{
	  g.drawPolygon(poly);
  }
  
  //fill Polygon method
  public void fill(Graphics g)	{
	  g.fillPolygon(poly);
  }

  
  
  
  
  
  
  
  
  
  
  
  
  //methods implemented for interface Shape

  public Rectangle getBounds()
  {
    return poly.getBounds();
  }

  public Rectangle2D getBounds2D()
  {
    return poly.getBounds2D();
  }

  public boolean contains(double x, double y)
  {
    return poly.contains(x, y);
  }

  public boolean contains(Point2D p)
  {
    return poly.contains(p);
  }

  public boolean intersects(double x, double y, double w, double h)
  {
    return poly.intersects(x, y, w, h);
  }
  

  public boolean intersects(Rectangle2D r)
  {
    return poly.intersects(r);
  }

  public boolean contains(double x, double y, double w, double h)
  {
    return poly.contains(x, y, w, h);
  }

  public boolean contains(Rectangle2D r)
  {
    return poly.intersects(r);
  }

  public PathIterator getPathIterator(AffineTransform at)
  {
    return poly.getPathIterator(at);
  }

  public PathIterator getPathIterator(AffineTransform at, double flatness)
  {
    return poly.getPathIterator(at, flatness);
  }

}