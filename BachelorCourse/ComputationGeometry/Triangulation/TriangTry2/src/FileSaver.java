import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FileSaver {
	FileSaver(File SaveFile,Set<Point> q) throws IOException { 
		BufferedWriter s;
		s = new BufferedWriter(new FileWriter(SaveFile.getAbsolutePath()));
		Iterator<Point> it = q.iterator();
		while (it.hasNext()) {
			Point p=it.next();  
			String str="x="+p.x+" y="+p.y+"\n";
			try {
				s.write(str);
			} catch (IOException e) {
				String message = "Cant write to specified file!";
			    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
			            JOptionPane.ERROR_MESSAGE);
			}
		}
		s.close();     
	}
}
