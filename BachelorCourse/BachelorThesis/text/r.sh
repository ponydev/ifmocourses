#!/bin/sh

if [ "$1" == "clean" ]
then
    rm -rf *.aux *.bbl *.bcf *.blg *.dvi *.log thesis.pdf *.ps *.toc *.run.xml
    cd pic
    rm -rf *.log *.mpx
    for (( i = 1; i <= 100; ++i ))
    do
        for j in *.$i
        do
            if [ -f "$j" ]
            then
                rm "$j"
            fi
        done
    done
else
    cd pic
    for i in *.mp
    do
        if [[ -f "$i" ]]
        then
            mpost $i
        fi
    done
    cd ..

    pdflatex thesis.tex
    biber thesis
    pdflatex thesis.tex
    pdflatex thesis.tex
fi

