ppp <-function() {
  d <- read.table('AverageResultsT.txt',header=T)
  d["Generation"] <- seq(1,2000,by=10)
  plot(d$EdmonsCarp~d$Generation,ann=FALSE,type="n",xlim=c(0,2000),ylim=c(0,2000))
  lines(d$EdmonsCarp~d$Generation,lwd=2,col=rainbow(7)[1])
  lines(d$Dinic~d$Generation,lwd=2,col=rainbow(7)[7])
  lines(d$Scaling2000~d$Generation,lwd=2,col=rainbow(7)[3])
  #lines(d$Scaling5000~d$Generation,lwd=2,col=rainbow(7)[4])
  #lines(d$Scaling10000~d$Generation,lwd=2,col=rainbow(7)[5])
  lines(d$Scaling20000~d$Generation,lwd=2,col=rainbow(7)[6])
  title("������",xlab="����� ���������",ylab="�������� �������-�������")
  legend("bottomright", c("������-����", "�����", "��������������� 20000","��������������� 2000"),
         lty = c(1, 1, 1), col = c(rainbow(7)[1], rainbow(7)[7],rainbow(7)[3],rainbow(7)[6]),cex = 0.8,text.width = 750)
}
