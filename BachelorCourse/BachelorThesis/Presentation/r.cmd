@echo off

if "%1"=="clean" goto clean
pdflatex bachelor
pdflatex bachelor

goto finish

:clean

del *.log *.aux *.bbl *.blg *.snm *.toc *.nav *.out
del bachelor.pdf

:finish
echo "done."
