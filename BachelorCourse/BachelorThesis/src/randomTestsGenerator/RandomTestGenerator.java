package randomTestsGenerator;

import genetics.FlowNetworkFactory;
import mfp.FlowNetwork;
import mfp.algo.augpath.Dinic3;
import mfp.algo.augpath.EdmonsCarp;
import mfp.algo.augpath.ScalingSimple;

import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 08.05.13
 * To change this template use File | Settings | File Templates.
 */
public class RandomTestGenerator {
    public static void main(String[] args) {
        FlowNetworkFactory ff = new FlowNetworkFactory(100, 5000, 10000);
        int randNumber = 100;
        int i = 0, i1 = 0, i2 = 0, i3 = 0, i4 = 0, i5 = 0, i6 = 0, j = 0;
        for (int z = 0; z < randNumber; z++) {
            System.err.println(z);
            List zz = ff.generateRandomCandidate(new Random());
            // List zz = ZadehTest.generateTest(100, 47);
            FlowNetwork fn = new FlowNetwork(zz, 100);
            i += new EdmonsCarp().solve(fn);
            fn.resetFlows();
            i1 += new ScalingSimple(10000).solve(fn);
            i2 += new ScalingSimple(5000).solve(fn);
            i3 += new ScalingSimple(2000).solve(fn);
            i4 += new ScalingSimple(1000).solve(fn);
            i5 += new ScalingSimple(500).solve(fn);
            i6 += new ScalingSimple(100).solve(fn);
            j += new Dinic3(zz, 100).maxFlow();
        }
        System.err.println(i / randNumber + " " + i1 / randNumber + " " + i2 / randNumber + " " + i3 / randNumber + " " + i4 / randNumber + " " + i5 / randNumber + " " + i6 / randNumber + " " + j / randNumber);
    }
}
