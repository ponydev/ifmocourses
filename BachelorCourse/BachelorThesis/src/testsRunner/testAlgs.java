package testsRunner;

import mfp.FlowNetwork;
import mfp.algo.augpath.Dinic3;
import mfp.algo.augpath.EdmonsCarp;
import mfp.algo.augpath.ScalingSimple;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 10.05.13
 * To change this template use File | Settings | File Templates.
 */
public class testAlgs {
    private static ObjectInputStream[] os;
    private static String[] bestFolderss;
    private static String outputDir;
    private static File[][] dir;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        bestFolderss = new String[10];
        dir = new File[10][2];
        os = new ObjectInputStream[10];
        int EdmonsCarpFitness = 0;
        int DinicFitness = 0;
        int Scaling20000Fitness = 0;
        int Scaling10000Fitness = 0;
        int Scaling5000Fitness = 0;
        int Scaling2000Fitness = 0;

        for (int i = 0; i < 10; i++)
            bestFolderss[i] = "c://bach/objects/" + "/" + "2013-05-11-02-39-40" + "/" + "Tournament90" + "/" + i;
        outputDir = "c://bach/results/Tournament90";
        PrintWriter pw = new PrintWriter(new FileWriter(new File(outputDir, "AverageResults.txt")));
        for (int i = 0; i < 10; i++) {
            dir[i] = new File(bestFolderss[i]).listFiles();
        }

        for (int i = 0; i < 10; i++)
            os[i] = new ObjectInputStream(
                    new FileInputStream(dir[i][0]));
        for (int i = 0; i < 200; i++) {
            EdmonsCarpFitness = 0;
            DinicFitness = 0;
            Scaling20000Fitness = 0;
            Scaling10000Fitness = 0;
            Scaling5000Fitness = 0;
            Scaling2000Fitness = 0;
            for (int j = 0; j < 10; j++) {
                FlowNetwork fn = (FlowNetwork) os[j].readObject();
                EdmonsCarpFitness += new EdmonsCarp().solve(fn);
                DinicFitness += new Dinic3(fn).maxFlow();
                Scaling20000Fitness += new ScalingSimple(20000).solve(fn);
                Scaling10000Fitness += new ScalingSimple(10000).solve(fn);
                Scaling5000Fitness += new ScalingSimple(5000).solve(fn);
                Scaling2000Fitness += new ScalingSimple(2000).solve(fn);
            }

            pw.print(EdmonsCarpFitness / 10);
            pw.print("          ");
            pw.print(DinicFitness / 10);
            pw.print("          ");
            pw.print(Scaling20000Fitness / 10);
            pw.print("          ");
            pw.print(Scaling10000Fitness / 10);
            pw.print("          ");
            pw.print(Scaling5000Fitness / 10);
            pw.print("          ");
            pw.print(Scaling2000Fitness / 10);
            pw.println();
            pw.flush();


        }
        for (int j = 0; j < 10; j++)
            os[j].close();
        pw.close();
    }

}
