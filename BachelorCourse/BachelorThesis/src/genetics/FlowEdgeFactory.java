package genetics;

import mfp.FlowEdge;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.Random;

public class FlowEdgeFactory extends AbstractCandidateFactory<FlowEdge> {

    private final int maxCapacity;
    private final int V;

    public FlowEdgeFactory(int V, int maxCapacity) {
        this.maxCapacity = maxCapacity;
        this.V = V;
    }

    @Override
    public FlowEdge generateRandomCandidate(Random r) {
        int v = r.nextInt(V);
        int w = r.nextInt(V);
        if (v == w)
            return generateRandomCandidate(r);
        int capacity = 1 + r.nextInt(maxCapacity);
        return new FlowEdge(v, w, capacity);
    }
}
