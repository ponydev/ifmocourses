package genetics;

import mfp.FlowEdge;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlowNetworkFactory extends
        AbstractCandidateFactory<List<FlowEdge>> {
    private int E;
    private final FlowEdgeFactory edgeFactory;

    public FlowNetworkFactory(int V, int E, int maxCapacity) {
        this.E = E;
        this.edgeFactory = new FlowEdgeFactory(V, maxCapacity);
    }

    @Override
    public List<FlowEdge> generateRandomCandidate(Random r) {
        List<FlowEdge> edges = new ArrayList<>();

        for (int i = 0; i < E; i++) {
            edges.add(edgeFactory.generateRandomCandidate(r));
        }
        return edges;

    }

    public FlowEdgeFactory getEdgeFactory() {
        return edgeFactory;
    }
}
