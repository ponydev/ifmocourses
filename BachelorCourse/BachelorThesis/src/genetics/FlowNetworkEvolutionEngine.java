package genetics;

import org.uncommons.watchmaker.framework.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 07.05.13
 * To change this template use File | Settings | File Templates.
 */
public class FlowNetworkEvolutionEngine<T> extends AbstractEvolutionEngine<T> {

    private final EvolutionaryOperator<T> evolutionScheme;
    private final FitnessEvaluator<? super T> fitnessEvaluator;
    private final SelectionStrategy<? super T> selectionStrategy;

    protected FlowNetworkEvolutionEngine(CandidateFactory<T> candidateFactory,
                                         EvolutionaryOperator<T> evolutionScheme,
                                         FitnessEvaluator<? super T> fitnessEvaluator,
                                         SelectionStrategy<? super T> selectionStrategy,
                                         Random rng) {
        super(candidateFactory, fitnessEvaluator, rng);
        this.evolutionScheme = evolutionScheme;
        this.fitnessEvaluator = fitnessEvaluator;
        this.selectionStrategy = selectionStrategy;
        // setSingleThreaded(true);
    }

    @Override
    protected List<EvaluatedCandidate<T>> nextEvolutionStep(List<EvaluatedCandidate<T>> evaluatedPopulation,
                                                            int eliteCount,
                                                            Random rng) {
        List<T> population = new ArrayList<>(evaluatedPopulation.size());

        List<T> elite = new ArrayList<>(eliteCount);
        EvaluatedCandidate<T> t;
        Iterator<EvaluatedCandidate<T>> iterator = evaluatedPopulation.iterator();
        while (elite.size() < eliteCount) {
            t = iterator.next();
            elite.add(t.getCandidate());
        }
        population.addAll(selectionStrategy.select(evaluatedPopulation,
                fitnessEvaluator.isNatural(),
                evaluatedPopulation.size() - eliteCount,
                rng));

        population = evolutionScheme.apply(population, rng);
        population.addAll(elite);
        return evaluatePopulation(population);
    }
}

