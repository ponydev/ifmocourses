package genetics;

import mfp.FlowEdge;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionEngine;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.SelectionStrategy;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.termination.GenerationCount;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class GeneticsMain implements Runnable {

    private static PrintWriter pw;
    private static ObjectOutputStream os;
    private static DateFormat dateFormat = new SimpleDateFormat(
            "yyyy-MM-dd-HH-mm-ss");
    private static String logPath;
    private static String logPaths;
    private static int numberOfExperiments = 10;
    private static int V;
    private static int E;
    private static int maxCapacity;
    private static double mutationProbability;
    private static int generationCount;
    private static int inGeneration;
    private static int elitismNumber;
    private static double percentBestObjects;
    private static String bestFolders;
    private static String bestFolderss;
    private static String bestFolder;
    private static String date;
    private int experimentNumber;
    private static CountDownLatch latch = new CountDownLatch(1);
    private static SelectionStrategy<Object> selectionStrategy;
    private static String selectionStrategyName;

    public static void main(String[] args) throws IOException,
            InterruptedException {

        loadProperties();

//
//        date = dateFormat.format(Calendar.getInstance().getTime())
//                .toString();
//        selectionStrategyName = "Roulette";
//        logPaths = logPath + "/" + date + "/" + selectionStrategyName ;
//        bestFolders = bestFolder + "/" + date + "/" + selectionStrategyName;
//        if (!new File(logPaths).mkdirs()) {
//            System.err.println("failed creation folder");
//        }
//        if (!new File(bestFolders).mkdirs()) {
//            System.err.println("failed creation folder");
//        }
//        selectionStrategy = new RouletteWheelSelection();
//        for (int i = 6; i < numberOfExperiments; i++) {  //0
//            bestFolderss = bestFolders + "/" + i;
//            if (!new File(bestFolderss).mkdirs()) {
//                System.err.println("failed creation folder");
//            }
//            Thread th = new Thread(new GeneticsMain(i));
//            latch = new CountDownLatch(1);
//            th.start();
//            latch.await();
//
//        }
//        new Thread(new runsAverager(logPath + "/" + date + "/" + selectionStrategyName)).start();
//
//
//        selectionStrategyName = "sigma";
//        logPaths = logPath + "/" + date + "/" + selectionStrategyName ;
//        bestFolders = bestFolder + "/" + date + "/" + selectionStrategyName;
//        if (!new File(logPaths).mkdirs()) {
//            System.err.println("failed creation folder");
//        }
//        if (!new File(bestFolders).mkdirs()) {
//            System.err.println("failed creation folder");
//        }
//        selectionStrategy = new SigmaScaling();
//        for (int i = 0; i < numberOfExperiments; i++) {
//            bestFolderss = bestFolders + "/" + i;
//            if (!new File(bestFolderss).mkdirs()) {
//                System.err.println("failed creation folder");
//            }
//
//            Thread th = new Thread(new GeneticsMain(i));
//            latch = new CountDownLatch(1);
//            th.start();
//            latch.await();
//
//
//        }
//        new Thread(new runsAverager(logPath + "/" + date + "/" + selectionStrategyName)).start();
//
//
//        selectionStrategyName = "Tournament90";
//
//        logPaths = logPath + "/" + date + "/" + selectionStrategyName ;
//        bestFolders = bestFolder + "/" + date + "/" + selectionStrategyName;
//        if (!new File(logPaths).mkdirs()) {
//            System.err.println("failed creation folder");
//        }
//        if (!new File(bestFolders).mkdirs()) {
//            System.err.println("failed creation folder");
//        }
//        selectionStrategy = new TournamentSelection(new Probability(0.9));
//        for (int i = 0; i < numberOfExperiments; i++) {
//            bestFolderss = bestFolders + "/" + i;
//            if (!new File(bestFolderss).mkdirs()) {
//                System.err.println("failed creation folder");
//            }
//            Thread th = new Thread(new GeneticsMain(i));
//            latch = new CountDownLatch(1);
//            th.start();
//            latch.await();
//        }
//        new Thread(new runsAverager(logPath + "/" + date + "/" + selectionStrategyName)).start();
        //  new Thread(new runsAverager(logPath + "/" + "2013-05-11-02-39-40" + "/" + "Roulette")).start();
        //  new Thread(new runsAverager(logPath + "/" + "2013-05-11-02-39-40" + "/" + "sigma")).start();
        new Thread(new runsAverager(logPath + "/" + "2013-05-11-02-39-40" + "/" + "Tournament90")).start();

    }

    public GeneticsMain(int experimentNumber) {
        this.experimentNumber = experimentNumber;
    }

    private static void loadProperties() throws IOException {
        Properties prop = new Properties();
        String fileName = "app.config";
        InputStream is = new FileInputStream(fileName);

        prop.load(is);

        V = Integer.parseInt(prop.getProperty("vertex.number"));
        E = Integer.parseInt(prop.getProperty("edges.number"));
        maxCapacity = Integer.parseInt(prop.getProperty("maximal.capacity"));
        mutationProbability = Double.parseDouble(prop
                .getProperty("mutation.probability"));
        generationCount = Integer
                .parseInt(prop.getProperty("generation.count"));
        inGeneration = Integer.parseInt(prop
                .getProperty("individuals.per.generation"));
        elitismNumber = Integer.parseInt(prop
                .getProperty("individuals.elitism"));
        logPath = prop.getProperty("folder.logger");
        bestFolder = prop.getProperty("folder.individuals");
        numberOfExperiments = Integer.parseInt(prop
                .getProperty("number.experiments"));
        percentBestObjects = Double.parseDouble(prop
                .getProperty("percent.best.objects"));
        is.close();

    }

    @Override
    public void run() {
        try {

            System.out.println("StartTime: " + date);

            pw = new PrintWriter(new FileWriter(File.createTempFile(
                    "Experiment", date + ".txt", new File(logPaths))));
            os = new ObjectOutputStream(
                    new FileOutputStream(File.createTempFile("generation", "",
                            new File(bestFolderss))));
            FlowNetworkFactory networkFactory = new FlowNetworkFactory(V, E,
                    maxCapacity);
            List<EvolutionaryOperator<List<FlowEdge>>> operators = new ArrayList<EvolutionaryOperator<List<FlowEdge>>>();
            operators.add(new FlowNetworkCrossover());
            operators.add(new FlowNetworkMutation(networkFactory,
                    new Probability(mutationProbability)));

            FlowNetworkEvaluator evaluator = new FlowNetworkEvaluator(V);
            EvolutionEngine<List<FlowEdge>> engine = new FlowNetworkEvolutionEngine<>(                       //GenerationalEvolutionEngine<> FlowNetworkEvolutionEngine<>
                    networkFactory, new EvolutionPipeline<>(
                    operators), evaluator,
                    selectionStrategy, new MersenneTwisterRNG());                                     // new RouletteWheelSelection()  new SigmaScaling()

            engine.addEvolutionObserver(new EvolutionLogger<List<FlowEdge>>(pw, os,
                    bestFolders, generationCount));
            engine.evolve(inGeneration, elitismNumber, new GenerationCount(
                    generationCount));
            pw.close();
            os.close();
            System.out.println("EndTime:" + dateFormat.format(Calendar.getInstance().getTime())
                    .toString());
            latch.countDown();
        } catch (IOException e) {
            System.err.println("IOException occured!");
        }

    }
}
