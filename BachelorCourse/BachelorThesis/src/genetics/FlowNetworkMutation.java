package genetics;

import mfp.FlowEdge;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlowNetworkMutation implements
        EvolutionaryOperator<List<FlowEdge>> {
    private final FlowNetworkFactory networkFactory;
    private final Probability mutationProbability;

    public FlowNetworkMutation(FlowNetworkFactory networkFactory,
                               Probability mutationProbability) {
        this.networkFactory = networkFactory;
        this.mutationProbability = mutationProbability;
    }

    @Override
    public List<List<FlowEdge>> apply(List<List<FlowEdge>> selectedCandidates,
                                      Random rng) {
        List<List<FlowEdge>> mutatedCandidates = new ArrayList<>(
                selectedCandidates.size());
        for (List<FlowEdge> network : selectedCandidates) {
            mutatedCandidates.add(mutate(network, rng));
        }

        return mutatedCandidates;
    }


    private List<FlowEdge> mutate(List<FlowEdge> network, Random rng) {
        List<FlowEdge> mutatedNetwork = new ArrayList<>(network.size());

        for (FlowEdge edge : network) {
            mutatedNetwork.add(mutateElem(edge, rng));
        }

        return mutatedNetwork;
    }

    private FlowEdge mutateElem(FlowEdge edge, Random rng) {
        return edge.mutate(rng, mutationProbability,
                networkFactory.getEdgeFactory());
    }
}
