package genetics;

import mfp.FlowEdge;
import mfp.FlowNetwork;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.islands.IslandEvolutionObserver;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class EvolutionLogger<T> implements IslandEvolutionObserver<T> {
    private PrintWriter pw;
    private String folder;
    private int generationCount;
    private ObjectOutputStream os;

    public EvolutionLogger(PrintWriter pw, ObjectOutputStream os, String folder, int generationCount) {
        this.pw = pw;
        this.os = os;
        this.folder = folder;
        this.generationCount = generationCount;
    }

    @Override
    public void populationUpdate(PopulationData<? extends T> data) {
        try {

            pw.println("Generation " + data.getGenerationNumber() + "||"    //TODO back pw
                    + "Best Candidate Fitness " + " "
                    + data.getBestCandidateFitness() + " ||" + "Mean fitness "
                    + data.getMeanFitness() + "||" + "\n");

            pw.flush();

            if (data.getGenerationNumber() % 10 == 0) {
                os.writeObject(new FlowNetwork((ArrayList<FlowEdge>) data.getBestCandidate(), 100));
                os.flush();
            }
        } catch (IOException e) {
            System.err.println("Logger Broke!");
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void islandPopulationUpdate(int islandIndex,
                                       PopulationData<? extends T> populationData) {
    }
}