package genetics;

import mfp.FlowEdge;
import mfp.FlowNetwork;
import mfp.algo.augpath.EdmonsCarp;
import org.uncommons.watchmaker.framework.FitnessEvaluator;

import java.util.List;

public class FlowNetworkEvaluator implements FitnessEvaluator<List<FlowEdge>> {
    private int V;

    public FlowNetworkEvaluator(int V) {
        this.V = V;

    }

    @Override
    public double getFitness(List<FlowEdge> lfe,
                             List<? extends List<FlowEdge>> arg1) {
        return new EdmonsCarp().solve(new FlowNetwork(lfe, V));
    }

    @Override
    public boolean isNatural() {
        return true;
    }
}
