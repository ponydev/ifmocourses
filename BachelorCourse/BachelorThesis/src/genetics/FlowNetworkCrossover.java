package genetics;

import mfp.FlowEdge;
import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlowNetworkCrossover extends AbstractCrossover<List<FlowEdge>> {

    protected FlowNetworkCrossover() {
        this(2);
    }

    public FlowNetworkCrossover(int crossoverPoints) {
        super(crossoverPoints);
    }

    @Override
    protected List<List<FlowEdge>> mate(List<FlowEdge> lfe1,
                                        List<FlowEdge> lfe2, int points, Random random) {
        List<List<FlowEdge>> temp = new ArrayList<>();
        int swapLength = random.nextInt(Math.min(lfe1.size(), lfe2.size()));
        int offset1 = random.nextInt(lfe1.size() - swapLength);
        int offset2 = random.nextInt(lfe2.size() - swapLength);
        List<FlowEdge> newA = new ArrayList<>();
        List<FlowEdge> newB = new ArrayList<>();

        for (FlowEdge e : lfe1) {
            newA.add(new FlowEdge(e));
        }
        for (FlowEdge e : lfe2) {
            newB.add(new FlowEdge(e));
        }
        for (int t = 0; t < swapLength; ++t) {
            FlowEdge tmp = new FlowEdge(newA.get(offset1 + t));
            FlowEdge tmp1 = new FlowEdge(newB.get(offset2 + t));

            newA.set(offset1 + t, tmp1);
            newB.set(offset2 + t, tmp);
        }
        temp.add(newA);
        temp.add(newB);

        return temp;
    }

}
