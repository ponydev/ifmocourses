package genetics;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 08.05.13
 * To change this template use File | Settings | File Templates.
 */
public class runsAverager implements Runnable {
    private static PrintWriter pw;
    private List<File> files = new ArrayList<>();

    public runsAverager(String directoryName) throws IOException {
        File directory = new File(directoryName);

        files.addAll(Arrays.asList(directory.listFiles()));

        this.pw = new PrintWriter(new FileWriter(new File(directoryName, "Average.txt")));
    }

    @Override
    public void run() {

        List<Scanner> sc = new ArrayList<>();
        for (File f : files) {
            try {
                sc.add(new Scanner(f));
            } catch (FileNotFoundException e) {
                System.err.println("Averager break");
                e.printStackTrace();
                System.exit(0);

            }
        }

        int i = 0;
        while (sc.get(0).hasNext()) {
            double sum = 0.0;
            for (Scanner s : sc) {
                s.next();
                s.findInLine("Fitness ");
                sum += Double.parseDouble(s.next());
                s.next();
                s.next();
                s.next();
            }

            pw.println((int) Math.round(sum / files.size()) + "\n");
            pw.flush();
            System.out.println(i);
            i++;
        }

        for (Scanner s : sc) {
            s.close();
        }
        pw.close();
    }
}
