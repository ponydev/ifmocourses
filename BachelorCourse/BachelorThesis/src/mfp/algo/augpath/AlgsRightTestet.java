package mfp.algo.augpath;

import genetics.FlowNetworkFactory;
import mfp.FlowNetwork;
import specialTestExample.ZadehTest;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Killer
 * Date: 11.05.13
 * Time: 23:18
 */
public class AlgsRightTestet {
    public static void main(String[] args) {
        FlowNetworkFactory ff = new FlowNetworkFactory(100, 5000, 10000);
        // List zz = ff.generateRandomCandidate(new Random());
        List zz = ZadehTest.generateTest(100, 47);
        FlowNetwork fn = new FlowNetwork(zz, 100);
        int i = new EdmonsCarp().solve(fn);
        fn.resetFlows();
        int i1 = new ScalingSimple(10000).solve(fn);
        int i2 = new ScalingSimple(5000).solve(fn);
        int i3 = new ScalingSimple(2000).solve(fn);
        int i4 = new ScalingSimple(1000).solve(fn);
        int i5 = new ScalingSimple(500).solve(fn);
        int i6 = new ScalingSimple(100).solve(fn);
        int j = new Dinic3(zz, 100).maxFlow();
        int j1 = new Dinic3(fn).maxFlow();
        System.err.println(i + " " + i1 + " " + i2 + " " + i3 + " " + i4 + " " + i5 + " " + i6 + " " + j + " " + j1);
    }

}
