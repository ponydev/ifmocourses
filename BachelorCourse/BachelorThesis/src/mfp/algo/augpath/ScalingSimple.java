package mfp.algo.augpath;

import mfp.FlowEdge;
import mfp.FlowNetwork;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Fold-Fulkerson algorithm with BFS augmenting path search = EdmondsKarp
 * algorithm
 */
public class ScalingSimple {

    /**
     * Array of already passed vertex for BFS.
     */
    private boolean[] marked;
    /**
     * For element i edgeTo[i] - edge to vertex i in path.
     */
    private FlowEdge[] edgeTo;
    /**
     * Value of maximum flow.
     */
    private int value;
    private int source;
    private int sink;
    private int fitness;
    private int delta = 10000;

    public ScalingSimple(int delta) {
        this.delta = delta;
    }

    public int solve(FlowNetwork G) {
        source = 0;
        fitness = 0;
        sink = G.V() - 1;
        value = excess(G, sink);
        while (delta > 0) {
            while (hasAugmentingPath(G, source, sink)) {
                int bottle = Integer.MAX_VALUE;
                int v = 0;

                for (v = sink; v != source; v = edgeTo[v].other(v)) {
                    bottle = Math.min(bottle, edgeTo[v].residualCapacityTo(v));
                }

                for (v = sink; v != source; v = edgeTo[v].other(v)) {
                    edgeTo[v].addResidualFlowTo(v, bottle);
                }
                value += bottle;
            }
            delta /= 2;
        }

        G.resetFlows();
        return fitness;
    }

    /**
     * @return Value of fitness.
     */
    public double fitnessValue() {
        return fitness;
    }

    private boolean hasAugmentingPath(FlowNetwork G, int s, int t) {
        edgeTo = new FlowEdge[G.V()];
        marked = new boolean[G.V()];
        fitness++;

        /** BFS. */
        Queue<Integer> q = new LinkedList<Integer>();
        q.add(s);
        marked[s] = true;
        while (!q.isEmpty()) {
            int v = q.poll();

            for (FlowEdge e : G.adj(v)) {
                int w = e.other(v);

                if (e.residualCapacityTo(w) > delta) {
                    if (!marked[w]) {
                        edgeTo[w] = e;
                        marked[w] = true;
                        q.add(w);
                    }
                }
            }
        }
        return marked[t];
    }

    /**
     * Excess in vertex.
     */
    private int excess(FlowNetwork G, int v) {
        int excess = 0;
        for (FlowEdge e : G.adj(v)) {
            if (v == e.getFrom())
                excess -= e.getFlow();
            else
                excess += e.getFlow();
        }
        return excess;
    }

}