package mfp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//TODO addlink!
/**
 * A class representing flow network consisting of flow edges #.
 * 
 * @author Viktor Arkhipov
 * @version 1.0
 */
public class FlowNetwork implements Serializable {
	private int V;
	private int E;
	private ArrayList<ArrayList<FlowEdge>> adj;

	/**
	 * Construct flow network of <tt>V</tt> vertex and no edges.
	 * 
	 * @param V
	 *            - number of vertex.
	 */
	public FlowNetwork(int V) {
		this.V = V;
		this.E = 0;
		adj = new ArrayList<>();
		for (int v = 0; v < V; v++)
			adj.add(v, new ArrayList<FlowEdge>());
	}

	/**
	 * Construct flow network of <tt>V</tt> vertex and <tt>E</tt> edges.
	 * 
	 * @param V
	 *            - number of vertex.
	 * @param E
	 *            - number of edges.
	 */
	public FlowNetwork(int V, int E) {
		this(V);
		Random r = new Random(System.currentTimeMillis());
		for (int i = 0; i < E; i++) {
			int v = r.nextInt(V);
			int w = r.nextInt(V);
			int capacity = r.nextInt(100);
			addEdge(new FlowEdge(v, w, capacity));
		}
	}
	
	
	public FlowNetwork(List<FlowEdge> edges, int V) {
			this(V);
			//System.out.println(edges);
			for (FlowEdge e : edges)
				addEdge(new FlowEdge(e));
	}
	
	
	/** Resets flow in flow network. */
	public void resetFlows() {
		for (int v = 0; v < V; v++)
			for (FlowEdge e : adj(v)) {
				e.resetFlow();
			}
	}

	/** Return number of vertex. */
	public int V() {
		return V;
	}

	/** Return number of edges. */
	public int E() {
		return E;
	}

	/**
	 * Adds an edge <tt>e</tt> to flow network.
	 * 
	 * @param e
	 *            - flow edge.
	 * */
	public void addEdge(FlowEdge e) {
		E++;
		int v = e.getFrom();
		int w = e.getTo();
		//if(adj.get(v).contains(e) || adj.get(w).contains(e) ) {System.out.println("FUCKINGWARNING!!!!!!!!!!!!!!");}
		adj.get(v).add(e);
		adj.get(w).add(e);
	}
	
	/**
	 * Remove an edge <tt>e</tt> to flow network.
	 * 
	 * @param e
	 *            - flow edge.
	 * */
	public void removeEdge(FlowEdge e) {
		E--;
		int v = e.getFrom();
		int w = e.getTo();
		adj.get(v).remove(e);
		adj.get(w).remove(e);
	}

	/** Return list of edges adjacent to <tt>Vertex</tt>. */
	public ArrayList<FlowEdge> adj(int Vertex) {
		return adj.get(Vertex);
	}

	/** Return list of all edges - excludes self loops. */
	public List<FlowEdge> edges() {
		List<FlowEdge> list = new ArrayList<FlowEdge>();
		for (int v = 0; v < V; v++)
			for (FlowEdge e : adj(v)) {
				if (e.getTo() != v)
					list.add(e);
			}
		return list;
	}



	/** Flow network string representation. */
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("Number of vertices: " + V() + " \n");
		s.append("Number of edges: " + E() + " \n");
		s.append("Edges: \n");
		for (FlowEdge e : edges())
			s.append(e + " \n");
		return s.toString();
	}
}
