package mfp;

import genetics.FlowEdgeFactory;

import java.io.Serializable;
import java.util.Random;

import org.uncommons.maths.random.Probability;

/**
 * A class representing directed flow edge in flow network.
 * 
 * @author Viktor Arkhipov
 */
public class FlowEdge implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3018811302216829124L;
	private int v;
	private int w;
	private int capacity;
	private int flow = 0;

	/**
	 * Flow edge constructor with default zero flow.
	 * 
	 * @param v
	 *            - "from" vertex.
	 * @param w
	 *            - "to" vertex.
	 * @param capacity
	 *            - capacity of vertex.
	 * @throws RuntimeException
	 *             if capacity is negative.
	 */
	public FlowEdge(int v, int w, int capacity) {
		if (capacity < 0)
			throw new RuntimeException("Negative edge capacity");
		this.v = v;
		this.w = w;
		this.capacity = capacity;
	}

	/**
	 * Flow edge constructor - with specified flow.
	 * 
	 * @param v
	 *            - "from" vertex.
	 * @param w
	 *            - "to" vertex.
	 * @param capacity
	 *            - capacity of edge.
	 * @param flow
	 *            - flow in edge.
	 * @throws RuntimeException
	 *             if capacity is negative.
	 */
	public FlowEdge(int v, int w, int capacity, int flow) {
		if (capacity < 0)
			throw new RuntimeException("Negative edge capacity");
		this.v = v;
		this.w = w;
		this.capacity = capacity;
		this.flow = flow;
	}

    public FlowEdge(FlowEdge e) {
        if (e.getCapacity() < 0)
            throw new RuntimeException("Negative edge capacity");
        this.v = e.getFrom();
        this.w = e.getTo();
        this.capacity = e.getCapacity();
        this.flow = e.getFlow();
    }

	/**
	 * Return edge outgoing vertex.
	 * 
	 * @return "from" vertex.
	 */
	public int getFrom() {
		return v;
	}

	/**
	 * Return edge incoming vertex.
	 * 
	 * @return "to" vertex.
	 */
	public int getTo() {
		return w;
	}

	/**
	 * Return edge capacity.
	 * 
	 * @return capacity of the edge.
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * Return edge current flow.
	 * 
	 * @return flow in the edge.
	 */
	public int getFlow() {
		return flow;
	}

	/**
	 * Return edge second vertex.
	 * 
	 * @param vertex
	 *            first vertex of the edge.
	 * @return second vertex of the edge.
	 * @throws RuntimeException
	 *             if strange end point is given.
	 */
	public int other(int vertex) {
		if (vertex == v)
			return w;
		else if (vertex == w)
			return v;
		else
			throw new RuntimeException("Illegal endpoint");
	}

	/**
	 * @param vertex
	 *            to which residual capacity is asked.
	 * @return Residual capacity to <tt>vertex</tt>.
	 * @throws RuntimeException
	 *             if strange end point is given.
	 */
	public int residualCapacityTo(int vertex) {
		if (vertex == v)
			return flow;
		else if (vertex == w)
			return capacity - flow;
		else
			throw new RuntimeException("Illegal endpoint");
	}

	/**
	 * Add residual flow to given <tt>vertex</tt> by flow <tt>delta</tt>.
	 * 
	 * @param vertex
	 *            - vertex to which flow is raised.
	 * @param delta
	 *            - value on which flow raised.
	 */
	public void addResidualFlowTo(int vertex, int delta) {
		if (vertex == v)
			flow -= delta;
		else if (vertex == w)
			flow += delta;
		else
			throw new RuntimeException("Illegal endpoint");
	}

	/** Resets flow on edge. */
	public void resetFlow() {
		this.flow = 0;
	}

	/** Edge string representation. */
	public String toString() {
		return v + "->" + w + " " + flow + "/" + capacity;
	}

	public FlowEdge mutate(Random rng, Probability mutationProbability,
			FlowEdgeFactory factory) {
		if (mutationProbability.nextEvent(rng)) {
			return factory.generateRandomCandidate(rng);
		} else {
			return new FlowEdge(this);
		}
	}

}
