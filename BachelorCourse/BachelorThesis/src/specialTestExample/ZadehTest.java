package specialTestExample;

import mfp.FlowEdge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Killer
 * Date: 12.05.13
 * Time: 1:16
 */
public class ZadehTest {

    public static List<FlowEdge> generateTest(int n, int k) {
        int p = (n - 2 * k - 2) / 4;
        int setSstart = 1;
        int setSend = k;
        int setTstart = k + 1;
        int setTend = 2 * k;
        int setUstart = 2 * k + 1;
        int setUend = 2 * k + 2 * p;
        int setVstart = 2 * k + 2 * p + 1;
        int setVend = 2 * k + 4 * p;
        List<FlowEdge> edges = new ArrayList<>();

        System.err.println("Started generation");
        for (int i = setSstart; i <= setSend; i++) {
            edges.add(new FlowEdge(0, i, k));
        }
        System.err.println();

        for (int i = setTstart; i <= setTend; i++) {
            edges.add(new FlowEdge(i, n - 1, k));
        }
        System.err.println("Phase 1 exits completed");

        for (int i = setSstart; i <= setSend; i++) {
            for (int j = setTstart; j <= setTend; j++) {
                edges.add(new FlowEdge(i, j, 1));
            }

        }
        System.err.println("Phase 2 S T completed");

        edges.add(new FlowEdge(0, setUstart, 10000));
        edges.add(new FlowEdge(setVstart, n - 1, 10000));

        System.err.println("Phase 3 exits inf completed");

        for (int j = setVend; j > setVstart; j--) {
            edges.add(new FlowEdge(j, j - 1, 10000));
        }
        for (int j = setUstart; j < setUend; j++) {
            edges.add(new FlowEdge(j, j + 1, 10000));
        }

        System.err.println("Phase 4 inf u v completed");


        for (int i = setUstart + 1; i <= setUend; i = i + 4) {
            for (int j = setTstart; j <= setTend; j++) {
                edges.add(new FlowEdge(i, j, k));
            }
        }

        System.err.println("Phase 5a completed");

        for (int i = setUstart + 3; i <= setUend; i = i + 4) {
            for (int j = setSstart; j <= setSend; j++) {
                edges.add(new FlowEdge(i, j, k));
            }
        }

        System.err.println("Phase 5b all completed");

        for (int i = setVstart + 1; i <= setVend; i = i + 4) {
            for (int j = setSstart; j <= setSend; j++) {
                edges.add(new FlowEdge(j, i, k));
            }
        }

        System.err.println("Phase 5c all completed");

        for (int i = setVstart + 3; i <= setUend; i = i + 4) {
            for (int j = setTstart; j <= setTend; j++) {
                edges.add(new FlowEdge(j, i, k));
            }
        }

        System.err.println("Phase 5d all completed");

        return edges;
    }
}
