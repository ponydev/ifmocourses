import TaskMakers.TaskMakerInterface;
import helpers.TaskValues;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.apache.commons.io.FileUtils.deleteQuietly;

/**
 * Created by killerforfun on 12.06.2015.
 */
public class ExperimentWorker implements Runnable {
    private ExecutorService es = Executors.newFixedThreadPool(10);
    private static final Logger log = LogManager.getLogger(ExperimentWorker.class);
    private String dir;
    private int experimentNum;
    private File[] files;
    private String mode;
    private BufferedWriter bw1;
    private double[] lastFitnessesOnExperiment;
    private Map<Integer, List<Long>> overallTimes;
    private Map<Integer, List<Long>> cpuTimes;
    private Map<Integer, List<Double>> load;
    double median;
    double p75;
    double p25;
    double p975;
    double p025;
    int maxExperNum;

    public ExperimentWorker(String dir, int experimentNum, File[] files, String mode, int maxExperNum) {
        this.dir = dir;
        this.experimentNum = experimentNum;
        this.files = files;
        this.mode = mode;
        this.maxExperNum = maxExperNum;
        File f = Paths.get(dir + "/" + "log-" + experimentNum + "-task1.log").toFile();
        if (f.exists()) {
            deleteQuietly(f);
        }
        try {
            bw1 = new BufferedWriter(new FileWriter(f));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
//            BufferedWriter writerTask2 = new BufferedWriter(new FileWriter(Paths.get(dir + "/" + "log-" + experimentNum + "-task1.log").toFile()));
        String currentLie = "";
        log.info("Working on experiment number {} with files {}", experimentNum, files.length);
        ArrayList<BufferedReader> brs = new ArrayList<>();
        int numberOfExperiments;

        TaskMakerFactory taskMakerFactory = TaskMakerFactory.getInstance();
        taskMakerFactory.setMode(mode);
        Set<Future<TaskValues>> futures = new HashSet<>();
        lastFitnessesOnExperiment = new double[maxExperNum];
        overallTimes = new HashMap<>();
        cpuTimes = new HashMap<>();
        load = new HashMap<>();

        for (int i = 0; i < 5; i++) {
            overallTimes.put(i, new ArrayList<Long>());
            cpuTimes.put(i, new ArrayList<Long>());
            load.put(i, new ArrayList<Double>());
        }
        try {
            for (int i = 0; i < files.length; i++) {
                futures.add(es.submit(taskMakerFactory.getTaskMaker(dir, files[i])));
            }
            int index = 0;
            double[] task = new double[5];

            for (Future<TaskValues> future : futures) {
                TaskValues tv = future.get();

                lastFitnessesOnExperiment[index] = tv.lastFitness;
                for (int i = 0; i < 5; i++) {
                    overallTimes.get(i).add(tv.overallTimes[i]);
                    cpuTimes.get(i).add(tv.cpuTimes[i]);
                    load.get(i).add(tv.load[i]);
                }
                task = tv.fitnessValues;
                log.debug("Calculated " + index);
//                log.debug(tv);
                index++;
            }

            calculatePercentDifferences(lastFitnessesOnExperiment);
            bw1.write("Median(50%): " + median);
            bw1.newLine();
            bw1.write("75%-25%: " + (p75 - p25));
            bw1.newLine();
            bw1.write("97.5%-2.5%: " + (p975 - p025));
            bw1.newLine();
            bw1.write("Values: ");
            bw1.newLine();


            for (int i = 0; i < 5; i++) {
                bw1.write(task[i] + ": " + " CPU: " + calculateMedian(cpuTimes.get(i)) + " All: " + calculateMedian(overallTimes.get(i)) + " load: " + calculateMedian(true, load.get(i)));
                bw1.newLine();
            }
            es.shutdown();
            bw1.close();
        } catch (FileNotFoundException e) {
            log.error("File not found", e);
        } catch (InterruptedException e) {
            log.error("Interrupted", e);
        } catch (ExecutionException e) {
            log.error("Execution problem", e);
        } catch (IOException e) {
            log.error("IOException", e);
        }
    }

    private void nthOccurrence(String str, char c, int[] positions, int maxpos) {
        int tempSave = 0;
        for (int pos = 0; pos < maxpos; pos++) {
            tempSave = str.indexOf(c, tempSave + 1);
            positions[pos] = tempSave;
        }
    }

    private void calculatePercentDifferences(double[] array) {
        Arrays.sort(array);
        if (array.length % 2 == 0)
            median = ((double) array[array.length / 2] + (double) array[array.length / 2 - 1]) / 2;
        else
            median = (double) array[array.length / 2];

        if (array.length % 4 == 0) {
            p25 = ((double) array[array.length / 4] + (double) array[array.length / 4 - 1]) / 2;
            p75 = ((double) array[array.length * 3 / 4] + (double) array[array.length * 3 / 4 - 1]) / 2;
        } else {
            p25 = (double) array[array.length / 4];
            p75 = (double) array[array.length * 3 / 4];
        }

        if (array.length % 40 == 0) {
            p025 = ((double) array[array.length / 40] + (double) array[array.length / 40 - 1]) / 2;
            p975 = ((double) array[array.length * 39 / 40] + (double) array[array.length * 39 / 40 - 1]) / 2;
        } else {
            p025 = (double) array[array.length / 40];
            p975 = (double) array[array.length * 39 / 40];
        }
    }

    private double calculateMedian(boolean z, List<Double> array) {
        double median;
        Collections.sort(array);

        array = array.subList(array.size() * 4 / 5, array.size());
        if (array.size() % 2 == 0)
            median = ((double) array.get(array.size() / 2) + (double) array.get(array.size() / 2 - 1)) / 2;
        else
            median = (double) array.get(array.size() / 2);

        return median;
    }

    private long calculateMedian(List<Long> array) {
        long median;
        Collections.sort(array);
        array = array.subList(array.size() * 4 / 5, array.size());
        if (array.size() % 2 == 0)
            median = ((long) array.get(array.size() / 2) + (long) array.get(array.size() / 2 - 1)) / 2;
        else
            median = (long) array.get(array.size() / 2);
        return median;
    }

    private long calculateTestSum(long[] array) {
        long sum = 0L;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    private double calculateTestSum(double[] array) {
        double sum = 0L;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

}
