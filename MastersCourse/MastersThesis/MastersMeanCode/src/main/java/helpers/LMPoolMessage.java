package helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

/**
 * Created by killerforfun on 12.06.2015.
 */
public class LMPoolMessage {
    private static final Logger log = LogManager.getLogger(LMPoolMessage.class);
    public double newFitness;
    public long threadCPUTime;
    public long threadClock;
    private HashMap<String, Long> mapOfThreadTimes;

    public LMPoolMessage() {
        mapOfThreadTimes = new HashMap<>();
    }


    public Long getThreadClockTime() {
        Long max = Long.MIN_VALUE;
        for (Long l : mapOfThreadTimes.values()) {
            max = Math.max(max, l);
        }
        return max;
    }


    public void addPoolMessage(TaskValues tv, String s) {
        int[] occurences = new int[4];
        nthOccurrence(s, ' ', occurences, 4);
        threadClock = Long.parseLong(s.substring(occurences[3] + 1));

        String threadName = s.substring(0, occurences[0]);
        newFitness = Double.parseDouble(s.substring(occurences[1] + 1, occurences[2]));
        this.threadCPUTime += Long.parseLong(s.substring(occurences[2] + 1, occurences[3]));

        if (newFitness < tv.fitnessValues[tv.currentValIndex]) {
//            tv.cpuTimes[tv.currentValIndex] = this.threadCPUTime;

//            if (mapOfThreadTimes.containsKey(threadName)) {
//                tv.overallTimes[tv.currentValIndex] = mapOfThreadTimes.get(threadName) + threadClock;
//            } else {
//                tv.overallTimes[tv.currentValIndex] = threadClock;
//            }
//            tv.load[tv.currentValIndex] = (double) tv.cpuTimes[tv.currentValIndex] / tv.overallTimes[tv.currentValIndex];
//            tv.currentValIndex++;
            tv.changed = true;

        }


        if (mapOfThreadTimes.containsKey(threadName)) {
            mapOfThreadTimes.put(threadName, mapOfThreadTimes.get(threadName) + threadClock);
        } else {
            mapOfThreadTimes.put(threadName, threadClock);
        }
        tv.lastFitness = newFitness;
    }

    private static void nthOccurrence(String str, char c, int[] positions, int maxpos) {
        int tempSave = 0;
        for (int pos = 0; pos < maxpos; pos++) {
            tempSave = str.indexOf(c, tempSave + 1);
            positions[pos] = tempSave;
        }
    }

}
