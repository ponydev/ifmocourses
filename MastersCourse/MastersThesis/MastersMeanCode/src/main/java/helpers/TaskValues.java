package helpers;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by killerforfun on 13.06.2015.
 */
public class TaskValues {
    public double lastFitness;
    public String function;
    public int experimentNum;
    public int size;
    public int currentValIndex;
    public boolean changed;

    public void init() {
        cpuTimes = new long[size];
        overallTimes  = new long[size];
        load = new double[size];
        currentValIndex = 0;
        changed = false;
    }

    public TaskValues() {
    }

    public TaskValues(double lastFitness, String function, int experimentNum, long[] cpuTimes, long[] overallTimes, double[] load) {
        this.lastFitness = lastFitness;
        this.function = function;
        this.experimentNum = experimentNum;
        this.cpuTimes = cpuTimes;
        this.overallTimes = overallTimes;
        this.load = load;
    }

    public double fitnessValues[];
    public long cpuTimes[];
    public long overallTimes[];
    public double load[];


    public String toString() {
        return Arrays.toString(cpuTimes) + " " + Arrays.toString(overallTimes) + " " + Arrays.toString(cpuTimes);
    }
}
