package helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

/**
 * Created by killerforfun on 12.06.2015.
 */
public class MyMainMessage {
    private static final Logger log = LogManager.getLogger(MyMainMessage.class);
    private HashMap<String, Long> mapOfThreadTimes;

    long generatePopulationAndCalculateFitnessCpuTime;
    long weightAndXMeanRecalculationCpuTime;
    long vectorsUpdateCpuTime;
    long sigmaUpdateCpuTime;
    String threadName = "";


    public MyMainMessage() {
        mapOfThreadTimes = new HashMap<>();
    }

    public void processMainMessage(TaskValues tv, String s) {
        int[] occurences = new int[11];
        nthOccurrence(s, ' ', occurences, 11);
        threadName = s.substring(0, occurences[0]);

        double fitnessValue = Double.parseDouble(s.substring(occurences[1] + 1, occurences[2]));

        Long totalTime;
        if (mapOfThreadTimes.containsKey(threadName)) {
            totalTime = mapOfThreadTimes.get(threadName);
        } else {
            totalTime = 0L;
        }

        this.generatePopulationAndCalculateFitnessCpuTime += Long.parseLong(s.substring(occurences[2] + 1, occurences[3]));
        totalTime += Long.parseLong(s.substring(occurences[3] + 1, occurences[4]));//generatePopulationAndCalculateFitnessClockTime
        totalTime += Long.parseLong(s.substring(occurences[4] + 1, occurences[5]));//waitingTime
        this.weightAndXMeanRecalculationCpuTime += Long.parseLong(s.substring(occurences[5] + 1, occurences[6]));
        totalTime += Long.parseLong(s.substring(occurences[6] + 1, occurences[7]));//weightAndXMeanRecalculationClockTime
        this.vectorsUpdateCpuTime += Long.parseLong(s.substring(occurences[7] + 1, occurences[8]));
        totalTime += Long.parseLong(s.substring(occurences[8] + 1, occurences[9]));//vectorsUpdateClockTime
        this.sigmaUpdateCpuTime += Long.parseLong(s.substring(occurences[9] + 1, occurences[10]));
        totalTime += Long.parseLong(s.substring(occurences[10] + 1));//sigmaUpdateClockTime

        mapOfThreadTimes.put(threadName, totalTime);

        tv.lastFitness = fitnessValue;

        if (fitnessValue < tv.fitnessValues[tv.currentValIndex] && threadName.contains("IC-0")) {
//            log.debug(threadName + tv);
            tv.cpuTimes[tv.currentValIndex] = sigmaUpdateCpuTime + vectorsUpdateCpuTime + weightAndXMeanRecalculationCpuTime + generatePopulationAndCalculateFitnessCpuTime;
            tv.overallTimes[tv.currentValIndex] = totalTime;
            tv.load[tv.currentValIndex] = (double) tv.cpuTimes[tv.currentValIndex] / tv.overallTimes[tv.currentValIndex];
            tv.currentValIndex++;
        }

    }

    private static void nthOccurrence(String str, char c, int[] positions, int maxpos) {
        int tempSave = 0;
        for (int pos = 0; pos < maxpos; pos++) {
            tempSave = str.indexOf(c, tempSave + 1);
            positions[pos] = tempSave;
        }
    }

}
