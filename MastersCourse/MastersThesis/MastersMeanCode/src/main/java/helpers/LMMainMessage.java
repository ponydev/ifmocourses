package helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by killerforfun on 12.06.2015.
 */
public class LMMainMessage {
    private static final Logger log = LogManager.getLogger(LMMainMessage.class);
    double lastFitness;
    ArrayList<Double> fitnessValues = new ArrayList<>();
    ArrayList<Long> totalClockTime = new ArrayList<>();
    ArrayList<Long> totalCPUTime = new ArrayList<>();
    ArrayList<Double> totalLoad = new ArrayList<>();

    long threadWorkCpuTime;
    long threadWorkClockTime;
    long weightAndXMeanRecalculationCpuTime;
    long weightAndXMeanRecalculationClockTime;
    long vectorsUpdateCpuTime;
    long vectorsUpdateClockTime;
    long sigmaUpdateCpuTime;
    long sigmaUpdateClockTime;

    public LMMainMessage() {
    }

    public double getLastFitness() {
        return lastFitness;
    }

    public void addPoolMessage(TaskValues tv, LMPoolMessage pool) {
        threadWorkCpuTime += pool.threadCPUTime;
        threadWorkClockTime += pool.getThreadClockTime();
    }

    public void addMainMessage(TaskValues tv, String s) {
        int[] occurences = new int[6];
        nthOccurrence(s, ' ', occurences, 6);
        this.weightAndXMeanRecalculationCpuTime += Long.parseLong(s.substring(occurences[0] + 1, occurences[1]));
        this.weightAndXMeanRecalculationClockTime += Long.parseLong(s.substring(occurences[1] + 1, occurences[2]));
        this.vectorsUpdateCpuTime += Long.parseLong(s.substring(occurences[2] + 1, occurences[3]));
        this.vectorsUpdateClockTime += Long.parseLong(s.substring(occurences[3] + 1, occurences[4]));
        this.sigmaUpdateCpuTime += Long.parseLong(s.substring(occurences[4] + 1, occurences[5]));
        this.sigmaUpdateClockTime += Long.parseLong(s.substring(occurences[5] + 1));


        if (tv.changed) {
            tv.cpuTimes[tv.currentValIndex] = sigmaUpdateCpuTime + vectorsUpdateCpuTime + weightAndXMeanRecalculationCpuTime + threadWorkCpuTime;
            tv.overallTimes[tv.currentValIndex ] = sigmaUpdateClockTime + vectorsUpdateClockTime + weightAndXMeanRecalculationClockTime + threadWorkClockTime;
            tv.load[tv.currentValIndex ] =(double) tv.cpuTimes[tv.currentValIndex] / tv.overallTimes[tv.currentValIndex];
            tv.changed = false;
            tv.currentValIndex++;
        }

    }

    private static void nthOccurrence(String str, char c, int[] positions, int maxpos) {
        int tempSave = 0;
        for (int pos = 0; pos < maxpos; pos++) {
            tempSave = str.indexOf(c, tempSave + 1);
            positions[pos] = tempSave;
        }
    }

}
