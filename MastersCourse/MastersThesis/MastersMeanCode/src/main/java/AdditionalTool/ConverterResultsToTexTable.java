package AdditionalTool;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

/**
 * Created by killerforfun on 13.06.2015.
 */
public class ConverterResultsToTexTable {
    public static void main(String[] args) {
        while (true) {

            Scanner scanner = new Scanner(System.in);
            NumberFormat df = DecimalFormat.getInstance();
            df.setMinimumFractionDigits(1);
            df.setMaximumFractionDigits(2);
            df.setRoundingMode(RoundingMode.DOWN);

            NumberFormat df1 = DecimalFormat.getInstance();
            df1.setMinimumFractionDigits(1);
            df1.setMaximumFractionDigits(1);
            df1.setRoundingMode(RoundingMode.DOWN);
            String subs = "";
            scanner.nextLine();
            String s = scanner.nextLine();
            String sum = "";

            for (int i = 0; i < 5; i++) {
                subs = s.substring(s.indexOf("All: ") + 5, s.length());
                subs = subs.replace(" load: ", "/");
                Double time = Double.parseDouble(subs.substring(0, subs.indexOf("/"))) / 1000000000;
                Double cpu = Double.parseDouble(subs.substring(subs.indexOf("/") + 1));
                sum = sum + " & " + df.format(time) + "/" + df1.format(cpu);
                s = scanner.nextLine();
            }
            sum = sum.replace(",", ".");
            System.out.println(sum);
        }

    }
}
