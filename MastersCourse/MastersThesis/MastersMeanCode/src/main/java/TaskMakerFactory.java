import TaskMakers.LMTaskMaker;
import TaskMakers.MyTaskMaker;
import TaskMakers.TaskMakerInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;

/**
 * Created by killerforfun on 12.06.2015.
 */
public class TaskMakerFactory {
    private static final Logger log = LogManager.getLogger(TaskMakerFactory.class);
    private static TaskMakerFactory INSTANCE = new TaskMakerFactory();
    public String mode;

    private TaskMakerFactory() {

    }

    public static TaskMakerFactory getInstance() {
        return INSTANCE;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public TaskMakerInterface getTaskMaker(String directory, File file) {
        if ("LM".equals(mode)) {
            return new LMTaskMaker(file);
        } else {
            return new MyTaskMaker(file);
        }
    }

}
