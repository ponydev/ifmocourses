package TaskMakers;

import helpers.TaskValues;

import java.util.concurrent.Callable;

/**
 * Created by killerforfun on 12.06.2015.
 */
public interface TaskMakerInterface extends Callable<TaskValues>{

    public void calcTasks();

    public String toString();

}
