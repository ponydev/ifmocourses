package TaskMakers;

import helpers.MyMainMessage;
import helpers.TaskValues;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.apache.commons.io.FileUtils.deleteQuietly;

/**
 * Created by killerforfun on 12.06.2015.
 */
public class MyTaskMaker implements TaskMakerInterface {
    private static final Logger log = LogManager.getLogger(MyTaskMaker.class);
    BufferedReader br;
    double[] fitnessValues;
    String functionName;


    private TaskValues tv = new TaskValues();

    public MyTaskMaker(File file) {
        try {
            if (file.getName().contains("Sphere")) {
                fitnessValues = new double[]{1, 3e-2, 1e-5, 3e-7, 1e-10};
                functionName = "Sphere";
//                log.debug(Arrays.toString(fitnessValues));

            }
            if (file.getName().contains("Rastrigin")) {
                fitnessValues = new double[]{1, 3e-2, 1e-5, 3e-7, 1e-10};
                functionName = "Rastrigin";
            }
            if (file.getName().contains("Rosenbrock")) {
                fitnessValues = new double[]{3000, 100, 1e-2, 1e-6, 1e-10};
                functionName = "Rosenbrock";
            }

            if (file.getName().contains("Ellipsoid")) {
                fitnessValues = new double[]{3000, 100, 1e-2, 1e-6, 1e-10};
                functionName = "Ellipsoid";
            }
            br = new BufferedReader(new FileReader(file));
        } catch (IOException e) {
            log.error("IOEcxeption!", e);
        }
    }

    @Override
    public void calcTasks() {
//        log.info("Processing Task");
        boolean skipLastPools = false;
        tv.size = fitnessValues.length;
        tv.init();
        tv.fitnessValues = fitnessValues;
        tv.function = functionName;
        try {
            String str = br.readLine();
            MyMainMessage myMainMessage = new MyMainMessage();
            while (str != null && tv.currentValIndex < 5) {
                if (!str.startsWith("Start") && !str.contains("Target") && !str.contains("Maximum"))
                    myMainMessage.processMainMessage(tv, str);
                str = br.readLine();
            }
            log.info("Task processed");
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public TaskValues call() throws Exception {
        calcTasks();
        return tv;
    }

    @Override
    public String toString() {
        return "MyTask";
    }
}
