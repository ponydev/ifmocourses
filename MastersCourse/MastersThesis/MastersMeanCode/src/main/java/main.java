import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

public class main {
//    private static final Logger log = LogManager.getLogger(main.class);

    public static void main(String[] args) throws IOException {
        System.out.println(Math.floor(3 * Math.log(300)));
        String dir = args[0];
        int numberOfThreads = 10;
        String mode; //1 2 3
        mode = args[1];//LM or MY
        int maxExperNum = Integer.parseInt(args[2]);//100 or 50
        File directoryToSum = Paths.get(dir).toFile();
        ExecutorService es = Executors.newFixedThreadPool(numberOfThreads);

        for (int experimentNum = 0; experimentNum < 70; experimentNum++) {
            File[] files = listFilesMatching(directoryToSum, "log-" + experimentNum + "-.*-.*-.*.log");
            if (files.length != 0) {
                es.submit(new ExperimentWorker(dir, experimentNum, files, mode, maxExperNum));
            }
        }
        es.shutdown();


    }


    public static File[] listFilesMatching(File root, String regex) {
        if (!root.isDirectory()) {
            throw new IllegalArgumentException(root + " is not directory.");
        }
        final Pattern p = Pattern.compile(regex); // careful: could also throw an exception!
        return root.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return p.matcher(file.getName()).matches();
            }
        });
    }

}
