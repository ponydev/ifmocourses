import AlgorithmParameters.AlgorithmStaticParameters;
import IndividualCalculation.IndividualCalculationController;
import Utility.TestCaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.util.Properties;

/**
 * Created by killerforfun on 24.04.15.
 */

public class CMAEvolutionStrategy {
    private static final Logger log = LogManager.getLogger(CMAEvolutionStrategy.class);

    private final Properties properties;
    private IndividualCalculationController individualCalculationController;


    public CMAEvolutionStrategy(Properties properties) {
        this.properties = properties;
    }


    public void startAlgorithmParallel() throws InterruptedException, TestCaseException {
        initialize();
        individualCalculationController.startNIndividualCalculations();
    }

    public void initialize() throws TestCaseException {
        int additionalComplexityE;
        int additionalComplexityV;
        int dimension;
        int threadNumber;
        double targetSigma;
        double targetFitness;
        try {
            dimension = Integer.parseInt(properties.getProperty("dimension"));
        } catch (NumberFormatException e) {
            throw new TestCaseException("Bad dimension format in testcase " + ThreadContext.get("testcase") + ". Skipping testcase.");
        }
        try {
            targetFitness = Double.parseDouble(properties.getProperty("targetfitness"));
        } catch (NumberFormatException e) {
            throw new TestCaseException("Bad targetFitness format in testcase " + ThreadContext.get("testcase") + ". Skipping testcase.");
        }
        try {
            targetSigma = Double.parseDouble(properties.getProperty("targetsigma"));
        } catch (NumberFormatException e) {
            throw new TestCaseException("Bad targetSigma format in testcase " + ThreadContext.get("testcase") + ". Skipping testcase.");
        }
        try {
            threadNumber = Integer.parseInt(properties.getProperty("threadnumber"));
        } catch (NumberFormatException e) {
            throw new TestCaseException("Bad threadNumber format in testcase " + ThreadContext.get("testcase") + ". Skipping testcase.");
        }

        AlgorithmStaticParameters.initialize(dimension, targetFitness, targetSigma);
        if ("Y".equals(properties.getProperty("autoAdjustAdditionalComplexity"))) {
            log.info("AutoAdjusting is on");
            log.info("Starting adjusting with threads: {}", threadNumber);
//            StartAdjusting(properties.getProperty("additionalComplexityV"), properties.getProperty("additionalComplexityE"), threadNumber);
            additionalComplexityV = 200;
            additionalComplexityE = 20000;
        } else {
            try {
                additionalComplexityV = Integer.parseInt(properties.getProperty("additionalComplexityV"));
            } catch (NumberFormatException e) {
                throw new TestCaseException("Bad additionalComplexityV format in testcase " + ThreadContext.get("testcase") + ". Skipping testcase.");
            }
            try {
                additionalComplexityE = Integer.parseInt(properties.getProperty("additionalComplexityE"));
            } catch (NumberFormatException e) {
                throw new TestCaseException("Bad additionalComplexityE format in testcase " + ThreadContext.get("testcase") + ". Skipping testcase.");
            }
        }

        log.info("Start parameters: dimension = {}, targetfFitness = {}, targetSigma = {}, actualSystemThreads = {}, threadNumber = {}, additionalComplexityV = {}, additionalComplexityE = {}, function = {}", dimension, targetFitness, targetSigma, Runtime.getRuntime().availableProcessors(), threadNumber, additionalComplexityV, additionalComplexityE, properties.getProperty("function"));
        individualCalculationController = new IndividualCalculationController(properties.getProperty("function"), threadNumber, additionalComplexityV, additionalComplexityE);
    }


}
