import AlgorithmParameters.AlgorithmStaticParameters;
import IndividualCalculation.IndividualGenerationLMCMA;
import IndividualCalculation.ObjectiveFunction.*;
import Utility.AdditionalWeight.AdjMatrix;
import Utility.AdditionalWeight.BellmanFord;
import Utility.AdditionalWeight.DirectedEdge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import IndividualCalculation.IndividualLog;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.*;
import java.util.concurrent.*;

/**
 * This class is adopted from c++ LMCMA - https://www.lri.fr/~ilya/publications/GECCO2014_LMCMA.pdf , source get from https://sites.google.com/site/lmcmaeses/ by Viktor Arkhipov for testing purposes.
 */
public class LMCMA {
    private static final Logger log = LogManager.getLogger(LMCMA.class);
    private ExecutorService service;
    private int V;
    private int E;
    private List<DirectedEdge> edges = new ArrayList<>();

    private String dateTimeThreadContext;
    private String testcaseThreadContext;
    private String startNumberThreadContext;
    private String numberOfExperimentThreadContext;
    private ObjectiveFunctionToCalculation[] functionsToCalculate;

    public LMCMA(int threadNumber,
                 ExecutorService service,
                 String function,
                 int ver,
                 int edg,
                 String dateTimeThreadContext,
                 String testcaseThreadContext,
                 String startNumberThreadContext,
                 String numberOfExperimentThreadContext) {
        this.service = service;
        this.V = ver;
        this.E = edg;
        if (this.V > 0) {
            for (int i = 0; i < E; i++) {
                int vx = (int) (V * Math.random());
                int wx = (int) (V * Math.random());
                double weightx = Math.round(100 * (Math.random() - 0.15)) / 100.0;
                if (vx == wx) edges.add(new DirectedEdge(vx, wx, Math.abs(weightx)));
                else edges.add(new DirectedEdge(vx, wx, weightx));
            }
        }
        this.dateTimeThreadContext = dateTimeThreadContext;
        this.testcaseThreadContext = testcaseThreadContext;
        this.startNumberThreadContext = startNumberThreadContext;
        this.numberOfExperimentThreadContext = numberOfExperimentThreadContext;
        this.functionsToCalculate = new ObjectiveFunctionToCalculation[lambda];
        preInitFloyds(function);

    }


    private ObjectiveFunctionToCalculation calculationFunction;
    int dimension = AlgorithmStaticParameters.getDimension();
    int lambda = AlgorithmStaticParameters.getLambda();
    int mu = AlgorithmStaticParameters.getMu();
    double ccov = AlgorithmStaticParameters.getCcov();
    double xmin = AlgorithmStaticParameters.getXmin();
    double xmax = AlgorithmStaticParameters.getXmax();
    int nvectors = AlgorithmStaticParameters.getNvectors();
    int maxsteps = AlgorithmStaticParameters.getMaxsteps();
    double cc = AlgorithmStaticParameters.getCc();
    double sigma = AlgorithmStaticParameters.getSigma();
    double c_s = AlgorithmStaticParameters.getC_s();
    double[] v_arr = new double[dimension * nvectors];
    double[] pc_arr = new double[dimension * nvectors];
    double[] Lj_arr = new double[nvectors];
    double[] Nj_arr = new double[nvectors];
    double[] arx = new double[dimension * lambda];
    double[] arfitness = new double[lambda];
    int counteval = 1;
    double BestF = Double.MAX_VALUE;
    ObjectiveFunctionToCalculation objectiveFunction;


    double[] Av = new double[dimension];
    int[] iterator = new int[nvectors];
    double K = 1 / Math.sqrt(1 - ccov);
    double M = Math.sqrt(1 - ccov);
    int iterator_sz = 0;
    double[] xmean = new double[dimension];


    final ThreadMXBean bean =
            ManagementFactory.getThreadMXBean();

    public int[] myqsort(int size, double[] arfitness) {
        class Helper implements Comparable<Helper> {
            public Helper(int index, Double fitness) {
                this.index = index;
                this.fitness = fitness;
            }

            int index;
            Double fitness;

            @Override
            public int compareTo(Helper o) {
                return this.fitness.compareTo(o.fitness);
            }
        }
        int arr[] = new int[size];
        Helper[] helpers = new Helper[size];

        for (int i = 0; i < size; i++)
            helpers[i] = new Helper(i, arfitness[i]);

        Arrays.sort(helpers);

        for (int i = 0; i < size; i++)
            arr[i] = helpers[i].index;

        return arr;
    }

    private void invAz() {
        for (int j = 0; j < iterator_sz; j++) {
            int jcur = iterator[j];
            double v_j_mult_Av = 0;
            for (int p = 0; p < dimension; p++)
                v_j_mult_Av += v_arr[jcur * dimension + p] * Av[p];
            v_j_mult_Av = Lj_arr[jcur] * v_j_mult_Av;
            for (int p = 0; p < dimension; p++)
                Av[p] = K * Av[p] - v_j_mult_Av * v_arr[jcur * dimension + p];
        }
    }


    public void LMCMAfixed() throws ExecutionException, InterruptedException {


        // n
        double[] pc = new double[dimension];
        double[] xold = new double[dimension];
        // lambda, mu, nvectors
        double[] weights = new double[mu];
        double[] prev_arfitness = new double[lambda];
        int[] arindex = new int[lambda];
        double[] mixed = new double[2 * lambda];
        int[] ranks = new int[2 * lambda];
        int[] ranks_tmp = new int[2 * lambda];
        int[] t = new int[nvectors];
        int[] vec = new int[nvectors];
        double val_target = 0.25;
        double targetFitness = AlgorithmStaticParameters.getTarget_f();
        double targetSigma = AlgorithmStaticParameters.getTarget_sigma();
        long targetEvals = AlgorithmStaticParameters.getTarget_evals();
        long weightAndXMeanRecalculationCpuTime;
        long weightAndXMeanRecalculationClockTime;
        long vectorsUpdateCpuTime;
        long vectorsUpdateClockTime;
        long sigmaUpdateCpuTime;
        long sigmaUpdateClockTime;


        Random random = new Random(System.nanoTime());

        double sum_weights = 0;
        for (int i = 0; i < mu; i++) {
            weights[i] = Math.log((double) (mu + 0.5)) - Math.log((double) (1 + i));
            sum_weights = sum_weights + weights[i];
        }
        double mueff = 0;
        for (int i = 0; i < mu; i++) {
            weights[i] = weights[i] / sum_weights;
            mueff = mueff + weights[i] * weights[i];
        }
        mueff = 1 / mueff;

        for (int i = 0; i < dimension; i++)
            pc[i] = 0;


        for (int i = 0; i < dimension; i++)
            xmean[i] = xmin + (xmax - xmin) * random.nextDouble();


        double s = 0;
        int stop = 0;
        int itr = 0;
        int indx = 0;


        while (stop == 0) {

            startParallelGeneration(arx, arfitness);


            weightAndXMeanRecalculationCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId());
            weightAndXMeanRecalculationClockTime = System.nanoTime();

            arindex = myqsort(lambda, arfitness);

            for (int i = 0; i < dimension; i++) {
                xold[i] = xmean[i];
                xmean[i] = 0;
            }

            for (int i = 0; i < mu; i++) {
                for (int j = 0; j < dimension; j++)
                    xmean[j] += weights[i] * arx[arindex[i] * dimension + j];
            }

            weightAndXMeanRecalculationCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId()) - weightAndXMeanRecalculationCpuTime;
            weightAndXMeanRecalculationClockTime = System.nanoTime() - weightAndXMeanRecalculationClockTime;


            vectorsUpdateCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId());
            vectorsUpdateClockTime = System.nanoTime();

            for (int i = 0; i < dimension; i++)
                pc[i] = (1 - cc) * pc[i] + Math.sqrt(cc * (2 - cc) * mueff) * (xmean[i] - xold[i]) / sigma;

            int imin = 1;
            if (itr < nvectors) {
                t[itr] = itr;
            } else {
                int dmin = vec[t[1]] - vec[t[0]];

                for (int j = 1; j < (nvectors - 1); j++) {
                    int dcur = vec[t[j + 1]] - vec[t[j]];
                    if (dcur < dmin) {
                        dmin = dcur;
                        imin = j + 1;
                    }
                }
                if (dmin >= maxsteps)
                    imin = 0;
                if (imin != (nvectors - 1)) {
                    int sav = t[imin];
                    for (int j = imin; j < (nvectors - 1); j++)
                        t[j] = t[j + 1];
                    t[nvectors - 1] = sav;
                }
            }

            iterator_sz = itr + 1;
            if (iterator_sz > nvectors) iterator_sz = nvectors;
            for (int i = 0; i < iterator_sz; i++)
                iterator[i] = t[i];
            int newidx = t[iterator_sz - 1];
            vec[newidx] = itr;

            for (int i = 0; i < dimension; i++)
                pc_arr[newidx * dimension + i] = pc[i];

            // this procedure recomputes v vectors correctly, in the original LM-CMA-ES they were outdated/corrupted.
            // the procedure allows to improve the performance on some problems (up to 20% on Ellipsoid with D=128..512)
            // and sometimes better on other problems
//		for(int i=0; i<iterator_sz; i++) // makes the loop ca. 2 times slower
            if (imin == 1) imin = 0;
            for (int i = imin; i < iterator_sz; i++) {
//			int indx = iterator[i]; // makes the loop ca. 2 times slower
                indx = t[i];
                for (int j = 0; j < dimension; j++)
                    Av[j] = pc_arr[indx * dimension + j];
                invAz();

                for (int j = 0; j < dimension; j++)
                    v_arr[indx * dimension + j] = Av[j];

                double nv = 0;
                for (int j = 0; j < dimension; j++)
                    nv += v_arr[indx * dimension + j] * v_arr[indx * dimension + j];
                Nj_arr[indx] = (Math.sqrt(1 - ccov) / nv) * (Math.sqrt(1 + (ccov / (1 - ccov)) * nv) - 1);
                Lj_arr[indx] = (1 / (Math.sqrt(1 - ccov) * nv)) * (1 - (1 / Math.sqrt(1 + ((ccov) / (1 - ccov)) * nv)));
            }
            // end of procedure

            vectorsUpdateCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId()) - vectorsUpdateCpuTime;
            vectorsUpdateClockTime = System.nanoTime() - vectorsUpdateClockTime;

            sigmaUpdateCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId());
            sigmaUpdateClockTime = System.nanoTime();


            if (itr > 0) {
                for (int i = 0; i < lambda; i++) {
                    mixed[i] = arfitness[i];
                    mixed[lambda + i] = prev_arfitness[i];
                }
                ranks = myqsort(2 * lambda, mixed);
                double meanprev = 0;
                double meancur = 0;
                for (int i = 0; i < 2 * lambda; i++)
                    ranks_tmp[i] = ranks[i];
                for (int i = 0; i < 2 * lambda; i++)
                    ranks[ranks_tmp[i]] = i;
                for (int i = 0; i < lambda; i++) {
                    meanprev = meanprev + ranks[i];
                    meancur = meancur + ranks[lambda + i];
                }
                meanprev = meanprev / lambda;
                meancur = meancur / lambda;
                double diffv = (meancur - meanprev) / lambda;
                double z1 = diffv - val_target;
                s = (1 - c_s) * s + c_s * z1;
                double d_s = 2.0 * (dimension - 1.0) / dimension;//1;
                sigma = sigma * Math.exp(s / d_s);
            }

            for (int i = 0; i < lambda; i++)
                prev_arfitness[i] = arfitness[i];


            sigmaUpdateCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId()) - sigmaUpdateCpuTime;
            sigmaUpdateClockTime = System.nanoTime() - sigmaUpdateClockTime;


            log.info("{} {} {} {} {} {} {}",
                    Thread.currentThread().getName()
                    , weightAndXMeanRecalculationCpuTime
                    , weightAndXMeanRecalculationClockTime
                    , vectorsUpdateCpuTime
                    , vectorsUpdateClockTime
                    , sigmaUpdateCpuTime
                    , sigmaUpdateClockTime);


            if (arfitness[0] < targetFitness) {
                stop = 1;
                log.info(Thread.currentThread().getName() + ": " + "Target fitness accomplished.");
            }
            itr = itr + 1;

            if (sigma < targetSigma) {
                stop = 1;
                log.info(Thread.currentThread().getName() + ": " + "Maximum sigma accomplished.");
            }

            if (counteval > targetEvals) {
                stop = 1;
                log.info(Thread.currentThread().getName() + ": " + "Maximum maxsteps accomplished.");
            }
        }
    }

    private BellmanFord generateFloyd() {
        if (V > 0) {
            AdjMatrix G = new AdjMatrix(V);
            if (edges.size() == 0) {
                return null;
            } else
                for (DirectedEdge e : edges) {
                    if (e.getFrom() == e.getTo())
                        G.addEdge(new DirectedEdge(e.getFrom(), e.getTo(), Math.abs(e.getWeight())));
                    else G.addEdge(new DirectedEdge(e.getFrom(), e.getTo(), e.getWeight()));
                }
            return new BellmanFord(G);
        }
        return null;
    }

    //Generating uorshells
    public void preInitFloyds(String function) {
        for (int i = 0; i < lambda; i++) {
            ObjectiveFunctionToCalculation local;
            if ("Sphere".equals(function)) {
                local = new SphereObjectiveFunction(generateFloyd());
            } else {
                if ("Rosenbrock".equals(function)) {
                    local = new RosenbrockObjectiveFunction(generateFloyd());
                } else {
                    if ("Ellipsoid".equals(function)) {
                        local = new EllipsoidObjectiveFunction(generateFloyd());
                    } else {
                        if ("Rastrigin".equals(function)) {
                            local = new RastriginObjectiveFunction(generateFloyd());
                        } else {
                            throw new RuntimeException("Unknown function for calculation. For now can be Rosenbrock or Sphere");
                        }
                    }
                }
            }
            functionsToCalculate[i] = local;
        }
    }


    public void startParallelGeneration(double[] arx, double[] arfitness) throws ExecutionException, InterruptedException {
        Collection<Future<IndividualLog>> futures = new LinkedList<>();
        for (int i = 0; i < lambda; i++) {
            futures.add(service.submit(new IndividualGenerationLMCMA(arx, arfitness, i, iterator_sz, sigma, pc_arr, v_arr, Nj_arr, xmean, iterator, functionsToCalculate[i])));
        }
        for (Future<IndividualLog> future : futures) {
            IndividualLog individualLog = future.get();
            if (counteval == 1) BestF = individualLog.fitness;
            if (individualLog.fitness < BestF) BestF = individualLog.fitness;
            log.info("{} {} {} {} {}", individualLog.threadName
                    , counteval++
                    , BestF
                    , individualLog.threadCPUTime
                    , individualLog.threadClockTime
            );
        }

    }

}
