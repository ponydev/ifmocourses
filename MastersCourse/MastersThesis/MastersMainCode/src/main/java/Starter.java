import AlgorithmParameters.AlgorithmStaticParameters;
import Utility.TestCaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.ThreadContext;


/**
 * Created by killerforfun on 05.04.15.
 */
public class Starter {
    private static final Logger log = LogManager.getLogger(Starter.class);
    private Properties prop;
    private int testCaseNumber = 0;
    private int numberOfStarts = 0;

    public void run(String fileName) throws ExecutionException, InterruptedException {
        loadTestProperties(fileName);
        initialize();
        int numberOfExperiment = 0;
        String dateTimeThreadContext = "" + System.currentTimeMillis() / 1000L;
        ThreadContext.put("datetime", dateTimeThreadContext);
        ExecutorService executor = Executors.newFixedThreadPool(Integer.parseInt(prop.getProperty("threadnumber")));
        for (int i = Integer.parseInt(prop.getProperty("experimentStartNumber")); i < testCaseNumber; i++) {
            for (int j = 0; j < numberOfStarts; j++) {
                if (prop.getProperty("dimension" + i) == null) {
                    log.error("Dimension not specified for testcase = {}. Skipping testCase.", i);
                    break;
                } else {
                    prop.setProperty("dimension", prop.getProperty("dimension" + i));
                }
                if (prop.getProperty("function" + i) == null) {
                    log.error("Function not specified for testcase = {}. Skipping testCase.", i);
                    break;
                } else {
                    prop.setProperty("function", prop.getProperty("function" + i));
                }


                if ("N".equals(prop.getProperty("autoAdjustAdditionalComplexity" + i)) || (prop.getProperty("autoAdjustAdditionalComplexity" + i) == null)) {
                    if (prop.getProperty("additionalComplexityV" + i) == null || prop.getProperty("additionalComplexityE") == null) {
                        log.error("Neither (AdditionalComplexityV; AdditionalComplexityE) nor autoAdjustAdditionalComplexity specified for testcase {}. Skipping testCase.", i);
                        break;
                    } else {
                        prop.setProperty("autoAdjustAdditionalComplexity", "N");
                        prop.setProperty("additionalComplexityV", prop.getProperty("additionalComplexityV" + i));
                        prop.setProperty("additionalComplexityE", prop.getProperty("additionalComplexityE"));
                    }
                } else {
                    prop.setProperty("autoAdjustAdditionalComplexity", "Y");
                }

                ThreadContext.put("dimension", prop.getProperty("dimension"));
                ThreadContext.put("testcase", "" + i);
                ThreadContext.put("function", prop.getProperty("function"));
                ThreadContext.put("startnumber", "" + j);
                ThreadContext.put("numberofexperiment", "" + numberOfExperiment);


                if (prop.getProperty("LMCMA") == null) {
                    CMAEvolutionStrategy cmaEvolutionStrategy = new CMAEvolutionStrategy(prop);
                    try {
                        cmaEvolutionStrategy.startAlgorithmParallel();
                    } catch (TestCaseException e) {
                        log.error("Bad parameters format for case {}. Skipping.", i, j);
                        log.error("More info: {}.", e.getMessage());
                        break;
                    } catch (InterruptedException e) {
                        log.error("Something wrong happen during testcase {} in start {}. Check its log for details.", i, j);
                    }
                } else {
                    AlgorithmStaticParameters.initialize(Integer.parseInt(prop.getProperty("dimension")), Double.parseDouble(prop.getProperty("targetfitness")), Double.parseDouble(prop.getProperty("targetsigma")));
                    LMCMA lmcma = new LMCMA(Integer.parseInt(prop.getProperty("threadnumber")), executor,
                            prop.getProperty("function"),
                            Integer.parseInt(prop.getProperty("additionalComplexityV")),
                            Integer.parseInt(prop.getProperty("additionalComplexityE")),
                            dateTimeThreadContext, "" + i, "" + j, "" + numberOfExperiment);
                    lmcma.LMCMAfixed();
                }
                numberOfExperiment++;
                System.out.println("Calculated " + (i + 1) + "/" + testCaseNumber + " testcases and " + (j + 1) + "/" + numberOfStarts + " starts.");
            }
        }

        executor.shutdown();
    }


    private void initialize() {
        try {
            testCaseNumber = Integer.parseInt(prop.getProperty("testcasenumber"));
        } catch (NumberFormatException e) {
            log.error("Bad property testcasenumber specified! It need to be integer. Check CMATestCases.properties.");
            System.exit(1);
        }
        try {
            numberOfStarts = Integer.parseInt(prop.getProperty("numberofstarts"));
        } catch (NumberFormatException e) {
            log.error("Bad property numberofstarts specified! It need to be integer. Check CMATestCases.properties.");
            System.exit(1);
        }
    }


    private void loadTestProperties(String fileName) {
        try {
            prop = new Properties();
            InputStream is = new FileInputStream(new File(fileName));
            prop.load(is);
            is.close();
        } catch (IOException e) {
            log.error("Error, while reading file CMATestCases.properties. Check that this file is in path.");
            System.exit(1);
        }
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        if (args.length == 0) {
            log.error("No configuration file provided.");
            System.exit(1);
        }

        new Starter().run(args[0]);
    }

}
