package Utility.FitnessTree;

/**
 * Created by killerforfun on 17.05.15.
 */
public class FitnessTreeElement implements Comparable<FitnessTreeElement> {
    private double fitness;
    public double[] arx;

    public FitnessTreeElement(double fitness, double[] arx) {
        this.fitness = fitness;
        this.arx = arx;
    }

    public double[] getArx() {
        return arx;
    }

    public double getFitness() {
        return fitness;
    }

    @Override
    public int compareTo(FitnessTreeElement o) {
        if (this.getFitness() > o.getFitness()) return 1;
        if (this.getFitness() < o.getFitness()) return -1;
        return 0;
    }

//    @Override
//    public String toString() {
//        return "Fitness = " + fitness + "; Arx = " + Arrays.toString(arx);
//    }

    @Override
    public String toString() {
        return "" + fitness;
    }
}
