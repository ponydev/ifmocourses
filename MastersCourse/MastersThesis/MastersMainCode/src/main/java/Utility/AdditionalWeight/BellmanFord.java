package Utility.AdditionalWeight;

import java.util.Arrays;
import java.util.Random;

public class BellmanFord {
    private double[][] distTo;  // distTo[v][w] = length of shortest v->w path
    private DirectedEdge[][] edgeTo;  // edgeTo[v][w] = last edge on shortest v->w path
    private int V;


    public BellmanFord(AdjMatrix G) {
        V = G.V();
        distTo = new double[V][V];
        edgeTo = new DirectedEdge[V][V];

        for (int v = 0; v < V; v++) {
            for (int w = 0; w < V; w++) {
                distTo[v][w] = Double.POSITIVE_INFINITY;
            }
        }

        for (int v = 0; v < G.V(); v++) {
            for (DirectedEdge e : G.adj(v)) {
                distTo[e.getFrom()][e.getTo()] = e.getWeight();
                edgeTo[e.getFrom()][e.getTo()] = e;
            }
            if (distTo[v][v] >= 0.0) {
                distTo[v][v] = 0.0;
                edgeTo[v][v] = null;
            }
        }


    }

    public void calculate() {
        double[][] distToTempSave = distTo;
        DirectedEdge[][] edgeToTempSave = edgeTo;
        for (int i = 0; i < V; i++) {
            for (int v = 0; v < V; v++) {
                for (int w = 0; w < V; w++) {
                    if (distTo[v][w] > distTo[v][i] + distTo[i][w]) {
                        distTo[v][w] = distTo[v][i] + distTo[i][w];
                        edgeTo[v][w] = edgeTo[i][w];
                    }
                }
                if (distTo[v][v] < 0.0) {
                    return;
                }
            }
        }
        distTo = distToTempSave;
        edgeTo = edgeToTempSave;
    }


//15 100 - 600-800
//25 100 - 1000-1500

    public static void main(String[] args) {
//        int V = 700;
//        int E = 10000;
//        AdjMatrix G = new AdjMatrix(V);
//        for (int i = 0; i < E; i++) {
//            int v = (int) (V * Math.random());
//            int w = (int) (V * Math.random());
//            double weight = Math.round(100 * (Math.random() - 0.15)) / 100.0;
//            if (v == w) G.addEdge(new DirectedEdge(v, w, Math.abs(weight)));
//            else G.addEdge(new DirectedEdge(v, w, weight));
//        }
//        BellmanFord spt = new BellmanFord(G);
//        long timeStart = System.nanoTime();
//        spt.calculate();
//        System.out.println(System.nanoTime() - timeStart);

        Random random = new Random(System.nanoTime());
        double[] testArray = new double[8];
        for (int i = 0; i < testArray.length; i++)
            testArray[i] = random.nextDouble();
        System.out.println(Arrays.toString(testArray));
        System.out.println(Arrays.toString(myqsort(testArray.length, testArray)));

    }

    public static int[] myqsort(int size, double[] arfitness) {
        class Helper implements Comparable<Helper> {
            public Helper(int index, Double fitness) {
                this.index = index;
                this.fitness = fitness;
            }

            int index;
            Double fitness;

            @Override
            public int compareTo(Helper o) {
                return this.fitness.compareTo(o.fitness);
            }
        }
        int arr[] = new int[size];
        Helper[] helpers = new Helper[size];

        for (int i = 0; i < size; i++)
            helpers[i] = new Helper(i, arfitness[i]);

        Arrays.sort(helpers);

        for (int i = 0; i < size; i++)
            arr[i] = helpers[i].index;

        return arr;
    }

}