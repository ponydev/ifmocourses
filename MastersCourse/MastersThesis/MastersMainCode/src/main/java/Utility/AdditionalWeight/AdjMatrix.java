package Utility.AdditionalWeight;

/**
 * Created by killerforfun on 24.05.15.
 */

import java.util.Iterator;
import java.util.NoSuchElementException;

public class AdjMatrix {
    private int V;
    private int E;
    private DirectedEdge[][] adj;

    public AdjMatrix(int V) {
        this.V = V;
        this.E = 0;
        this.adj = new DirectedEdge[V][V];
    }

    public int V() {
        return V;
    }

    public int E() {
        return E;
    }

    public void addEdge(DirectedEdge e) {
        int v = e.getFrom();
        int w = e.getTo();
        if (adj[v][w] == null) {
            E++;
            adj[v][w] = e;
        }
    }

    public Iterable<DirectedEdge> adj(int v) {
        return new AdjIterator(v);
    }


    private class AdjIterator implements Iterator<DirectedEdge>, Iterable<DirectedEdge> {
        private int v, w = 0;

        public AdjIterator(int v) {
            this.v = v;
        }

        public Iterator<DirectedEdge> iterator() {
            return this;
        }

        public boolean hasNext() {
            while (w < V) {
                if (adj[v][w] != null) return true;
                w++;
            }
            return false;
        }

        public DirectedEdge next() {
            if (hasNext()) {
                return adj[v][w++];
            } else {
                throw new NoSuchElementException();
            }
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }


}
