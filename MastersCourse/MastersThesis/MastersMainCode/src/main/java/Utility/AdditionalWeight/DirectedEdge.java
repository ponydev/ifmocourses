package Utility.AdditionalWeight;

/**
 * Created by killerforfun on 24.05.15.
 */
public class DirectedEdge {
    private final int from;
    private final int to;
    private final double weight;


    public DirectedEdge(int v, int w, double weight) {
        this.from = v;
        this.to = w;
        this.weight = weight;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public double getWeight() {
        return weight;
    }


}
