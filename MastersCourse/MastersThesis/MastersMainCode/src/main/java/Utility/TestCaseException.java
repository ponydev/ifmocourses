package Utility;

/**
 * Created by killerforfun on 27.05.15.
 */
public class TestCaseException extends Exception {

    private String message;

    public TestCaseException(String s) {
        super(s);
        this.message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
