package IndividualCalculation;

import IndividualCalculation.ObjectiveFunction.ObjectiveFunctionToCalculation;

/**
 * Created by killerforfun on 28.05.15.
 */
public class IndividualLog {
    public String threadName;
    public long threadCPUTime;
    public long threadClockTime;
    public double fitness;
    public ObjectiveFunctionToCalculation objectiveFunctionToCalculation;

    public IndividualLog(String threadName, long threadCPUTime, long threadClockTime, double fitness, ObjectiveFunctionToCalculation objectiveFunctionToCalculation) {
        this.threadName = threadName;
        this.threadCPUTime = threadCPUTime;
        this.threadClockTime = threadClockTime;
        this.fitness = fitness;
        this.objectiveFunctionToCalculation = objectiveFunctionToCalculation;
    }

    public String toString() {
        return "ThreadName = " + threadName + "; Fitness = " + fitness;
    }
}
