package IndividualCalculation;

import AlgorithmParameters.AlgorithmDynamicParameters;
import IndividualCalculation.ObjectiveFunction.IndividualCalculationFactory;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.ThreadFactory;

/**
 * Created by killerforfun on 02.05.15.
 */

public class IndividualCalculationController {
    private static final Logger log = LogManager.getLogger(IndividualCalculationController.class);
    private final ThreadFactory threadFactory;
    private IndividualCalculationFactory individualCalculationFactory;
    private int threadNumber;

    public IndividualCalculationController(String function, int threadNumber, int v, int e) {

        this.threadNumber = threadNumber;


        threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("IC-%d")
                .setDaemon(false)
                .build();

        individualCalculationFactory = new IndividualCalculationFactory(function, new AlgorithmDynamicParameters(), v, e);
    }

    public void startNIndividualCalculations() throws InterruptedException {
        ArrayList<Thread> threadList = new ArrayList<>(threadNumber);
        for (int i = 0; i < threadNumber; i++)
            threadList.add(getIndividualCalculationThread());
        for (int i = 0; i < threadNumber; i++)
            threadList.get(i).start();
        for (int i = 0; i < threadNumber; i++)
            threadList.get(i).join();
    }

    public Thread getIndividualCalculationThread() {
        return threadFactory.newThread(individualCalculationFactory.getIndividualCalculation());

    }
}
