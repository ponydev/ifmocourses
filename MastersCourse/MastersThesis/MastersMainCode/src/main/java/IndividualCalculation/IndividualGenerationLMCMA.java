package IndividualCalculation;

import AlgorithmParameters.AlgorithmStaticParameters;
import IndividualCalculation.ObjectiveFunction.ObjectiveFunctionToCalculation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created by killerforfun on 28.05.15.
 */
public class IndividualGenerationLMCMA implements Callable<IndividualLog> {
    final ThreadMXBean bean =
            ManagementFactory.getThreadMXBean();
    private static final Logger log = LogManager.getLogger(IndividualGenerationLMCMA.class);

    private double[] arfitness;
    private double[] arx;
    private int i;
    private int iterator_sz;
    private double sigma;
    private int dimension = AlgorithmStaticParameters.getDimension();
    private double M = AlgorithmStaticParameters.getM();
    double[] Az = new double[dimension];
    Random random = new Random(System.nanoTime());
    double[] z = new double[dimension];
    double[] pc_arr = new double[dimension];
    double[] v_arr = new double[dimension];
    double[] Nj_arr = new double[dimension];
    ObjectiveFunctionToCalculation calculationFunction;

    public IndividualGenerationLMCMA(double[] arx,
                                     double[] arfitness,
                                     int i,
                                     int iterator_sz,
                                     double sigma,
                                     double[] pc_arr,
                                     double[] v_arr,
                                     double[] nj_arr,
                                     double[] xmean,
                                     int[] iterator,
                                     ObjectiveFunctionToCalculation calculationFunction) {
        this.arfitness = arfitness;
        this.arx = arx;
        this.i = i;
        this.iterator_sz = iterator_sz;
        this.sigma = sigma;
        this.pc_arr = pc_arr;
        this.v_arr = v_arr;
        Nj_arr = nj_arr;
        this.xmean = xmean;
        this.iterator = iterator;
        this.calculationFunction = calculationFunction;
    }

    double[] xmean = new double[dimension];
    int[] iterator = new int[dimension];
    long generatePopulationAndCalculateFitnessCpuTime;
    long generatePopulationAndCalculateFitnessClockTime;

    private double calculateFitness(double[] arrayForCalculation, int startElement, int endElement) {
        return calculationFunction.valueOfLMCMA(arrayForCalculation, startElement, endElement);
    }

    @Override
    public IndividualLog call() throws Exception {
        generatePopulationAndCalculateFitnessCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId());
        generatePopulationAndCalculateFitnessClockTime = System.nanoTime();
        for (int k = 0; k < dimension; k++)    // O(n)
        {
            z[k] = random.nextGaussian();
            Az[k] = z[k];
        }
        for (int k = 0; k < iterator_sz; k++) {
            int jcur = iterator[k];
            double v_j_mult_z = 0;
            for (int p = 0; p < dimension; p++)
                v_j_mult_z = v_j_mult_z + v_arr[jcur * dimension + p] * z[p];
            v_j_mult_z = Nj_arr[jcur] * v_j_mult_z;
            for (int p = 0; p < dimension; p++)
                Az[p] = M * Az[p] + v_j_mult_z * pc_arr[jcur * dimension + p];
        }
        for (int k = 0; k < dimension; k++)    // O(n)
            arx[i * dimension + k] = xmean[k] + sigma * Az[k];


        arfitness[i] = calculateFitness(arx, i * dimension, (i + 1) * dimension);

        generatePopulationAndCalculateFitnessCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId()) - generatePopulationAndCalculateFitnessCpuTime;
        generatePopulationAndCalculateFitnessClockTime = System.nanoTime() - generatePopulationAndCalculateFitnessClockTime;

        return new IndividualLog(Thread.currentThread().getName(), generatePopulationAndCalculateFitnessCpuTime, generatePopulationAndCalculateFitnessClockTime, arfitness[i], calculationFunction);

    }
}
