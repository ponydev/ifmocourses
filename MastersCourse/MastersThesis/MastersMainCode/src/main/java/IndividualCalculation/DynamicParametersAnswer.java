package IndividualCalculation;

/**
 * Created by killerforfun on 20.05.15.
 */
public class DynamicParametersAnswer {
    public boolean stopFlag;
    public String stopCondition;
    public long iterationNumber;
    public double bestFitness;

    public long startCpuTime;
    public long startClockTime;
    public long waitingForLockClockTime;
    public long weightAndXMeanRecalculationClockTime;
    public long weightAndXMeanRecalculationCpuTime;
    public long vectorsUpdateCpuTime;
    public long vectorsUpdateClockTime;
    public long sigmaUpdateCpuTime;
    public long sigmaUpdateClockTime;


    public DynamicParametersAnswer() {

    }
}
