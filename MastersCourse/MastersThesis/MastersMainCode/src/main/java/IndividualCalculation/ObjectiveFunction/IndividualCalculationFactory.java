package IndividualCalculation.ObjectiveFunction;

import AlgorithmParameters.AlgorithmDynamicParameters;
import IndividualCalculation.IndividualCalculation;
import Utility.AdditionalWeight.AdjMatrix;
import Utility.AdditionalWeight.BellmanFord;
import Utility.AdditionalWeight.DirectedEdge;
import org.apache.logging.log4j.ThreadContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by killerforfun on 12.05.15.
 */
public class IndividualCalculationFactory {
    private ObjectiveFunctionToCalculation objectiveFunction;
    private final AlgorithmDynamicParameters algorithmDynamicParameters;
    private final String function;
    private final int V;
    private final int E;
    private List<DirectedEdge> edges = new ArrayList<>();

    public IndividualCalculationFactory(String function, AlgorithmDynamicParameters algorithmDynamicParameters, int ver, int edg) {

        this.algorithmDynamicParameters = algorithmDynamicParameters;
        this.function = function;
        this.V = ver;
        this.E = edg;
        if (this.V > 0) {
            for (int i = 0; i < E; i++) {
                int vx = (int) (V * Math.random());
                int wx = (int) (V * Math.random());
                double weightx = Math.round(100 * (Math.random() - 0.15)) / 100.0;
                if (vx == wx) edges.add(new DirectedEdge(vx, wx, Math.abs(weightx)));
                else edges.add(new DirectedEdge(vx, wx, weightx));
            }
        }

    }        //Starting new logging


    public IndividualCalculation getIndividualCalculation() {
        if ("Sphere".equals(function)) {
            this.objectiveFunction = new SphereObjectiveFunction(generateFloyd());
        } else {
            if ("Rosenbrock".equals(function)) {
                this.objectiveFunction = new RosenbrockObjectiveFunction(generateFloyd());
            } else {
                if ("Ellipsoid".equals(function)) {
                    this.objectiveFunction = new EllipsoidObjectiveFunction(generateFloyd());
                } else {
                    if ("Rastrigin".equals(function)) {
                        this.objectiveFunction = new RastriginObjectiveFunction(generateFloyd());
                    } else {
                        throw new RuntimeException("Unknown function for calculation. For now can be Rosenbrock or Sphere");
                    }
                }
            }
        }

        return new IndividualCalculation(objectiveFunction
                , algorithmDynamicParameters
                , ThreadContext.get("dimension")
                , ThreadContext.get("testcase")
                , ThreadContext.get("startnumber")
                , ThreadContext.get("numberofexperiment"));
    }


    private BellmanFord generateFloyd() {
        if (V > 0) {
            AdjMatrix G = new AdjMatrix(V);
            if (edges.size() == 0) {
                return null;
            } else
                for (DirectedEdge e : edges) {
                    if (e.getFrom() == e.getTo())
                        G.addEdge(new DirectedEdge(e.getFrom(), e.getTo(), Math.abs(e.getWeight())));
                    else G.addEdge(new DirectedEdge(e.getFrom(), e.getTo(), e.getWeight()));
                }
            return new BellmanFord(G);
        }
        return null;
    }

}
