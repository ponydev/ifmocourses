package IndividualCalculation.ObjectiveFunction;


import Utility.AdditionalWeight.BellmanFord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by killerforfun on 04.05.15.
 */
public class SphereObjectiveFunction implements ObjectiveFunctionToCalculation {
    private static final Logger log = LogManager.getLogger(SphereObjectiveFunction.class);
    private BellmanFord additionalComplexity;

    public SphereObjectiveFunction(BellmanFord complexity) {
        this.additionalComplexity = complexity;
    }

    @Override
    public double valueOf(double[] x) {
        double Fit = 0;
        for (double aX : x) Fit += aX * aX;
        if (additionalComplexity != null)
            additionalComplexity.calculate();

        return Fit;
    }

    @Override
    public double valueOfLMCMA(double[] x, int startPos, int endPos) {
        double Fit = 0;
        for (int i = startPos; i < endPos; i++)
            Fit += x[i] * x[i];
        if (additionalComplexity != null)
            additionalComplexity.calculate();

        return Fit;
    }
}
