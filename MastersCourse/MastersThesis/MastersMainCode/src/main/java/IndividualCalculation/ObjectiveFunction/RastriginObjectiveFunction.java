package IndividualCalculation.ObjectiveFunction;

import Utility.AdditionalWeight.BellmanFord;

/**
 * Created by killerforfun on 28.05.15.
 */
public class RastriginObjectiveFunction implements ObjectiveFunctionToCalculation {
    private BellmanFord additionalComplexity;

    public RastriginObjectiveFunction(BellmanFord addcompl) {
        this.additionalComplexity = addcompl;
    }
    @Override
    public double valueOf(double[] x) {
        double res = 10 * x.length;
        for (int i = 0; i < x.length; i++)
            res += x[i] * x[i] - 10 * Math.cos(2 * Math.PI * x[i]);

        if (additionalComplexity != null)
            additionalComplexity.calculate();
        return res;
    }

    @Override
    public double valueOfLMCMA(double[] x, int startPos, int endPos) {
        double res = 10 * x.length;
        for (int i = startPos; i < endPos; i++)
            res += x[i] * x[i] - 10 * Math.cos(2 * Math.PI * x[i]);

        if (additionalComplexity != null)
            additionalComplexity.calculate();
        return res;
    }
}
