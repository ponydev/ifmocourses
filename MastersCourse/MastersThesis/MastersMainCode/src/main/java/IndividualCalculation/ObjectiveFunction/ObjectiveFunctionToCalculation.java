package IndividualCalculation.ObjectiveFunction;


/**
 * Created by killerforfun on 04.05.15.
 */
public interface ObjectiveFunctionToCalculation {
    public double valueOf(double[] pop);
    public double valueOfLMCMA(double[] pop, int startPos, int endPos);
}
