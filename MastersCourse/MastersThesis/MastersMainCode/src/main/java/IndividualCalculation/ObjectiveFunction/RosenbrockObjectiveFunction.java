package IndividualCalculation.ObjectiveFunction;


import Utility.AdditionalWeight.BellmanFord;

public class RosenbrockObjectiveFunction implements ObjectiveFunctionToCalculation {
    private BellmanFord additionalComplexity;

    public RosenbrockObjectiveFunction(BellmanFord addcompl) {
        this.additionalComplexity = addcompl;
    }

    @Override
    public double valueOf(double[] x) {
        double res = 0;
        for (int i = 0; i < x.length - 1; ++i)
            res += 100 * (x[i] * x[i] - x[i + 1]) * (x[i] * x[i] - x[i + 1]) +
                    (x[i] - 1.) * (x[i] - 1.);

        if (additionalComplexity != null)
            additionalComplexity.calculate();

        return res;
    }

    @Override
    public double valueOfLMCMA(double[] x, int startPos, int endPos) {
        double res = 0;
        for (int i = startPos; i < endPos - 1; i++)
            res += 100 * (x[i] * x[i] - x[i + 1]) * (x[i] * x[i] - x[i + 1]) +
                    (x[i] - 1.) * (x[i] - 1.);

        if (additionalComplexity != null)
            additionalComplexity.calculate();

        return res;
    }
}
