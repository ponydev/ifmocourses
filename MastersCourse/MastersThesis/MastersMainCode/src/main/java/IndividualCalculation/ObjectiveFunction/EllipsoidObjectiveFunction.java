package IndividualCalculation.ObjectiveFunction;

import Utility.AdditionalWeight.BellmanFord;

/**
 * Created by killerforfun on 28.05.15.
 */
public class EllipsoidObjectiveFunction implements ObjectiveFunctionToCalculation {
    private BellmanFord additionalComplexity;

    public EllipsoidObjectiveFunction(BellmanFord addcompl) {
        this.additionalComplexity = addcompl;
    }

    @Override
    public double valueOf(double[] x) {
        double res = 0;
        double resTemp = 0;

        for (int i = 0; i < x.length ; i++) {
            resTemp = 0;
            for (int j = 0; j < i; j++) {
                resTemp += x[j];
            }
            res += resTemp * resTemp;
        }

        if (additionalComplexity != null)
            additionalComplexity.calculate();

        return res;
    }

    @Override
    public double valueOfLMCMA(double[] x, int startPos, int endPos) {
        double res = 0;
        double resTemp = 0;

        for (int i = startPos; i < endPos ; i++) {
            resTemp = 0;
            for (int j = startPos; j < i; j++) {
                resTemp += x[j];
            }
            res += resTemp * resTemp;
        }

        if (additionalComplexity != null)
            additionalComplexity.calculate();

        return res;
    }

}
