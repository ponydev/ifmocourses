package IndividualCalculation;

import AlgorithmParameters.AlgorithmDynamicParameters;
import AlgorithmParameters.AlgorithmStaticParameters;
import IndividualCalculation.ObjectiveFunction.ObjectiveFunctionToCalculation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Random;

/**
 * Created by killerforfun on 30.04.15.
 */
public class IndividualCalculation implements Runnable {
    private static final Logger log = LogManager.getLogger(IndividualCalculation.class);

    private int id;
    private int dimension;
    private ObjectiveFunctionToCalculation calculationFunction;
    private AlgorithmDynamicParameters algorithmDynamicParameters;

    private double[] z;
    private double[] Az;
    private double[] arx;
    private boolean stopFlag;
    private int iterator_sz;
    private double fitness;
    private double sigma;
    private double M;
    private double[] pc_j;
    private double[] v_j;
    private double[] xMean;
    private double[] Nj_arr;
    private int[] iterator;

    private String threadContextDimension;
    private String threadContextTestcase;
    private String threadContextstartnumber;
    private String threadContextnumberofexperiment;
    private DynamicParametersAnswer dynamicParametersAnswer;
    private long generatePopulationAndCalculateFitnessCpuTime;
    private long generatePopulationAndCalculateFitnessClockTime;


    final ThreadMXBean bean =
            ManagementFactory.getThreadMXBean();

    public IndividualCalculation(ObjectiveFunctionToCalculation calculationFunction,
                                 AlgorithmDynamicParameters algorithmDynamicParameters,
                                 String threadContextDimension,
                                 String threadContextTestcase,
                                 String threadContextstartnumber,
                                 String threadContextnumberofexperiment) {
        this.dimension = AlgorithmStaticParameters.getDimension();
        this.calculationFunction = calculationFunction;
        this.algorithmDynamicParameters = algorithmDynamicParameters;
        this.threadContextDimension = threadContextDimension;
        this.threadContextnumberofexperiment = threadContextnumberofexperiment;
        this.threadContextstartnumber = threadContextstartnumber;
        this.threadContextTestcase = threadContextTestcase;

    }

    @Override
    public void run() {
        initWorker();
        while (!stopFlag) {
            stopFlag = processOne();
        }
        log.info(Thread.currentThread().getName() + ": " + dynamicParametersAnswer.stopCondition);
        destroyObjects();
    }

    private void destroyObjects() {
    }

    private void initWorker() {
        arx = new double[dimension];//*lambda
        z = new double[dimension];
        Az = new double[dimension];
        stopFlag = false;
        ThreadContext.put("dimension", threadContextDimension);
        ThreadContext.put("testcase", threadContextTestcase);
        ThreadContext.put("startnumber", threadContextstartnumber);
        ThreadContext.put("numberofexperiment", threadContextnumberofexperiment);

    }

    private boolean processOne() {
        generatePopulationAndCalculateFitnessCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId());
        generatePopulationAndCalculateFitnessClockTime = System.nanoTime();
        iterator_sz = algorithmDynamicParameters.getIterator_sz().get();
        xMean = algorithmDynamicParameters.getXmean();
        Nj_arr = algorithmDynamicParameters.getNj_arr();
        iterator = algorithmDynamicParameters.getIterator();
        pc_j = algorithmDynamicParameters.getPc_arr();
        v_j = algorithmDynamicParameters.getV_arr();
        sigma = algorithmDynamicParameters.getSigma();
        fitness = 0.0;
        Random random = new Random(System.nanoTime());
        M = AlgorithmStaticParameters.getM();

        for (int k = 0; k < dimension; k++) {
            z[k] = random.nextGaussian();
            Az[k] = z[k];
        }
        for (int k = 0; k < iterator_sz; k++) {
            int jcur = iterator[k];
            double v_j_mult_z = 0;
            for (int p = 0; p < dimension; p++)
                v_j_mult_z = v_j_mult_z + v_j[jcur * dimension + p] * z[p];
            v_j_mult_z = Nj_arr[jcur] * v_j_mult_z;
            for (int p = 0; p < dimension; p++)
                Az[p] = M * Az[p] + v_j_mult_z * pc_j[jcur * dimension + p];
        }


        for (int k = 0; k < dimension; k++) {
            arx[k] = xMean[k] + sigma * Az[k];

        }
        fitness = calculateFitness(arx);

        generatePopulationAndCalculateFitnessCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId()) - generatePopulationAndCalculateFitnessCpuTime;
        generatePopulationAndCalculateFitnessClockTime = System.nanoTime() - generatePopulationAndCalculateFitnessClockTime;


        dynamicParametersAnswer = algorithmDynamicParameters.phase2(arx.clone(), fitness, System.nanoTime());
        log.info("{} {} {} {} {} {} {} {} {} {} {} {}", Thread.currentThread().getName()
                , dynamicParametersAnswer.iterationNumber
                , dynamicParametersAnswer.bestFitness
                , generatePopulationAndCalculateFitnessCpuTime
                , generatePopulationAndCalculateFitnessClockTime
                , dynamicParametersAnswer.waitingForLockClockTime
                , dynamicParametersAnswer.weightAndXMeanRecalculationCpuTime
                , dynamicParametersAnswer.weightAndXMeanRecalculationClockTime
                , dynamicParametersAnswer.vectorsUpdateCpuTime
                , dynamicParametersAnswer.vectorsUpdateClockTime
                , dynamicParametersAnswer.sigmaUpdateCpuTime
                , dynamicParametersAnswer.sigmaUpdateClockTime
        );
        return dynamicParametersAnswer.stopFlag;
    }


    private double calculateFitness(double[] arrayForCalculation) {
        return calculationFunction.valueOf(arrayForCalculation);
    }


}
