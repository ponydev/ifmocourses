package AlgorithmParameters;

/**
 * Created by killerforfun on 06.05.15.
 */

public class AlgorithmStaticParameters {
    private static int dimension;
    private static int lambda; // 	population size, e.g., 4+floor(3*log(N));
    private static int nvectors; // 	number of stored direction vectors, e.g., nvectors = 4+floor(3*log(N))
    private static int maxsteps; // 	target number of generations between vectors, e.g., maxsteps = nvectors
    private static int mu;    // 	number of parents, e.g., floor(lambda/2);
    private static double ccov;// 	learning rate for covariance matrix, e.g., 1/(10*log(N+1))
    private static double xmin;//	x parameters lower bound
    private static double xmax;//	x parameters upper bound
    private static double cc; // learning rate for mean vector's evolution path, e.g., cc = 1/nvectors
    private static double sigma;    // initial step-size, e.g., 0.5
    private static double c_s;    //	decay factor for step-size adaptation, e.g., 0.3
    private static double[] weights;
    private static double K;
    private static double target_f;
    private static double target_sigma;
    private static long target_evals;
    private static int additionalComplexity;

    public static long getTarget_evals() {
        return target_evals;
    }

    private static double M;
    private static double mueff;
    private static boolean initialized = false;


    private AlgorithmStaticParameters() {
    }


    public static int getDimension() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return dimension;
    }

    public static int getLambda() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return lambda;
    }

    public static int getMu() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return mu;
    }

    public static double getCcov() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return ccov;
    }

    public static double getXmin() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return xmin;
    }

    public static double getXmax() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return xmax;
    }

    public static double getCc() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return cc;
    }


    public static double getSigma() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return sigma;
    }

    public static double getC_s() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return c_s;
    }


    public static double[] getWeights() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return weights;
    }

    public static double getK() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return K;
    }

    public static int getMaxsteps() {
        return maxsteps;
    }

    public static double getM() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return M;
    }

    public static double getMueff() {
        if (!initialized) {
            throw new ExceptionInInitializerError("Please, first initialize static param");
        }
        return mueff;
    }

    public static void initialize(int dim, double tf, double sgm) {
        dimension = dim;
        target_f = tf;
        target_sigma = sgm;
        target_evals = 1000000;
        setParameters();
        initialized = true;

    }

    public static double getTarget_f() {
        return target_f;
    }

    public static double getTarget_sigma() {
        return target_sigma;
    }

    public static int getNvectors() {
        return nvectors;
    }

    private static void setParameters() {
        lambda = (int) (4 + Math.floor(3 * Math.log(dimension)));
        mu = lambda / 2;

        ccov = 1 / (10 * Math.log(dimension + 1.0));
        xmin = -5;
        xmax = 5;
        cc = 1.0 / lambda;

        sigma = 0.5 * (xmax - xmin);
        c_s = 0.3;

        nvectors = lambda;	//	number of stored direction vectors, e.g., nvectors = 4+floor(3*log(N))
        maxsteps = nvectors;

        weights = new double[mu];

        double sum_weights = 0;
        for (int i = 0; i < mu; i++) {
            weights[i] = Math.log(mu + 0.5) - Math.log(1 + i);
            sum_weights = sum_weights + weights[i];
        }
        mueff = 0;
        for (int i = 0; i < mu; i++) {
            weights[i] = weights[i] / sum_weights;
            mueff = mueff + weights[i] * weights[i];
        }
        mueff = 1 / mueff;

        K = 1 / Math.sqrt(1 - ccov);
        M = Math.sqrt(1 - ccov);

    }

}
