package AlgorithmParameters;

import IndividualCalculation.DynamicParametersAnswer;
import Utility.FitnessTree.FitnessTree;
import Utility.FitnessTree.FitnessTreeElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by killerforfun on 07.05.15.
 */

public class AlgorithmDynamicParameters {
    private static final Logger log = LogManager.getLogger(AlgorithmDynamicParameters.class);
    private final Lock lock = new ReentrantLock();
    private double[] xmean; //wrapper
    private double[] xold;
    private double[] v_arr;
    private double[] pc_arr;
    private double[] Av;
    private double[] pc;
    private double[] v_j;
    private int[] t;
    private int[] vec;
    private double[] Nj_arr;
    private double[] Lj_arr;
    private AtomicInteger iterator_sz;


    public double[] getNj_arr() {
        return Nj_arr;
    }

    public int[] getIterator() {
        return iterator;
    }

    private int[] iterator;


    public double[] getV_arr() {
        return v_arr;
    }

    public double[] getPc_arr() {
        return pc_arr;
    }

    public AtomicInteger getIterator_sz() {
        return iterator_sz;
    }


    public double[] getXmean() {
        return xmean;
    }

    private final int lambda;
    private final int dimension;
    private final double cc;
    private final double ccov;
    private final double c_s;
    private final double mueff;
    private double sigma;
    private double mu;
    private double K;
    private int itr;
    private final double[] weights;
    private double s;
    private boolean lambda_succ;
    private double pSucc;
    private double pTargetSucc;
    private double d;
    private double c_p;
    private boolean stopFlag = false;

    public double getSigma() {
        return sigma;
    }

    FitnessTree fitness;
    private long currTime;

    public int getCounteval() {
        return itr;
    }

    private double bestF = Double.MAX_VALUE;
    private long totalTime;

    public long getTotalTime() {
        return totalTime;
    }

    public double getBestF() {
        return bestF;
    }

    private double targetFitness;
    private double targetSigma;
    private String stopCondition;
    private DynamicParametersAnswer answer;


    final ThreadMXBean bean =
            ManagementFactory.getThreadMXBean();

    public AlgorithmDynamicParameters() {
        Random random = new Random(System.nanoTime());
        this.dimension = AlgorithmStaticParameters.getDimension();
        this.lambda = AlgorithmStaticParameters.getLambda();
        this.cc = AlgorithmStaticParameters.getCc();
        this.ccov = AlgorithmStaticParameters.getCcov();
        this.c_s = AlgorithmStaticParameters.getC_s();
        this.sigma = AlgorithmStaticParameters.getSigma();
        this.mueff = AlgorithmStaticParameters.getMueff();
        this.weights = AlgorithmStaticParameters.getWeights();
        this.mu = AlgorithmStaticParameters.getMu();
        this.targetFitness = AlgorithmStaticParameters.getTarget_f();
        this.targetSigma = AlgorithmStaticParameters.getTarget_sigma();
        this.mu = AlgorithmStaticParameters.getMu();

        xmean = new double[dimension];

        for (int i = 0; i < dimension; i++)
            xmean[i] = AlgorithmStaticParameters.getXmin() + (AlgorithmStaticParameters.getXmax() - AlgorithmStaticParameters.getXmin()) * random.nextDouble();
        xold = new double[dimension];
        Av = new double[dimension];
        pc = new double[dimension];
        Lj_arr = new double[dimension];
        Nj_arr = new double[dimension];
        v_j = new double[dimension];

        iterator = new int[lambda];

        pc_arr = new double[lambda * dimension];
        v_arr = new double[lambda * dimension];

        t = new int[lambda];
        vec = new int[lambda];
        K = AlgorithmStaticParameters.getK();
        iterator_sz = new AtomicInteger(0);
        fitness = new FitnessTree(dimension, weights, xmean);
        itr = 0;
        sigma = AlgorithmStaticParameters.getSigma();
        pTargetSucc = 2.0 / 11.0;
        d = 1 + dimension / 2.0;
        c_p = 1.0 / 12.0;
        pSucc = 0.0;
    }


    public DynamicParametersAnswer phase2(double[] arx, double fitnessValue, long currentTimeCome) {

        lock.lock();
        try {
            answer = new DynamicParametersAnswer();
            answer.startCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId());
            answer.startClockTime = System.nanoTime();
            answer.waitingForLockClockTime = System.nanoTime() - currentTimeCome;

            lambda_succ = false;
            if (fitness.totalElements < mu) {
                fitness.insert(new FitnessTreeElement(fitnessValue, arx));
            } else {
                if (fitness.getLargest().getElement().getFitness() > fitnessValue) {
                    System.arraycopy(xmean, 0, xold, 0, xmean.length);
                    lambda_succ = true;
                    fitness.deleteLargest();
                    fitness.insert(new FitnessTreeElement(fitnessValue, arx));
                }
            }
            System.arraycopy(fitness.getXmean(), 0, xmean, 0, xmean.length);

            if (fitnessValue < bestF) bestF = fitnessValue;

            answer.weightAndXMeanRecalculationCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId()) - answer.startCpuTime;
            answer.weightAndXMeanRecalculationClockTime = System.nanoTime() - answer.startClockTime;


            for (int i = 0; i < dimension; i++) {
                pc[i] = (1 - cc) * pc[i] + Math.sqrt(cc * (2 - cc) * mueff) * (xmean[i] - xold[i]) / sigma;
            }


            int imin = 1;

            if (itr < lambda) {
                t[itr] = itr;
            } else {
                int dmin = vec[t[1]] - vec[t[0]];

                for (int j = 1; j < (lambda - 1); j++) {
                    int dcur = vec[t[j + 1]] - vec[t[j]];
                    if (dcur < dmin) {
                        dmin = dcur;
                        imin = j + 1;
                    }
                }
                if (dmin >= lambda)
                    imin = 0;
                if (imin != (lambda - 1)) {
                    int sav = t[imin];
                    System.arraycopy(t, imin + 1, t, imin, lambda - 1 - imin);
                    t[lambda - 1] = sav;
                }
            }

            if (iterator_sz.get() >= lambda) {
                iterator_sz.set(lambda);
            } else iterator_sz.set(itr + 1);
            int temp_iterator_sz = iterator_sz.get();
            System.arraycopy(t, 0, iterator, 0, temp_iterator_sz);
            int newidx = t[temp_iterator_sz - 1];
            vec[newidx] = itr;

            System.arraycopy(pc, 0, pc_arr, newidx * dimension, dimension);

            if (imin == 1) imin = 0;

            for (int i = imin; i < iterator_sz.get(); i++) {
                int indx = t[i];
                System.arraycopy(pc_arr, indx * dimension, Av, 0, dimension);
                invAz();
                System.arraycopy(Av, 0, v_arr, indx * dimension, dimension);
                double nv = 0;
                for (int j = 0; j < dimension; j++)
                    nv += v_arr[indx * dimension + j] * v_arr[indx * dimension + j];
                Nj_arr[indx] = (Math.sqrt(1 - ccov) / nv) * (Math.sqrt(1 + (ccov / (1 - ccov)) * nv) - 1);
                Lj_arr[indx] = (1 / (Math.sqrt(1 - ccov) * nv)) * (1 - (1 / Math.sqrt(1 + ((ccov) / (1 - ccov)) * nv)));
            }

            answer.vectorsUpdateCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId()) - answer.weightAndXMeanRecalculationCpuTime - answer.startCpuTime;
            answer.vectorsUpdateClockTime = System.nanoTime() - answer.weightAndXMeanRecalculationClockTime - answer.startClockTime;


            if (itr > 0) {
                updateSigma();
            }
            itr++;

            answer.sigmaUpdateCpuTime = bean.getThreadCpuTime(Thread.currentThread().getId()) - answer.vectorsUpdateCpuTime - answer.weightAndXMeanRecalculationCpuTime - answer.startCpuTime;
            answer.sigmaUpdateClockTime = System.nanoTime() - answer.vectorsUpdateClockTime - answer.weightAndXMeanRecalculationClockTime - answer.startClockTime;

            if (bestF < targetFitness) {
                stopFlag = true;
                stopCondition = "Target fitness accomplished.";
            }
            if (sigma < targetSigma) {
                stopFlag = true;
                stopCondition = "Maximum sigma accomplished.";
            }

            answer.stopFlag = stopFlag;
            answer.stopCondition = stopCondition;
            answer.iterationNumber = itr;
            answer.bestFitness = bestF;

            return answer;
        } finally {
            lock.unlock();
        }
    }

    private void updateSigma() {
        pSucc = (1 - c_p) * pSucc + (lambda_succ ? c_p : 0);
        sigma = sigma * Math.exp(1 / d * (pSucc - (pTargetSucc / (1 - pTargetSucc)) * (1 - pSucc)));
    }

    private void invAz() {
        for (int j = 0; j < iterator_sz.get(); j++) {
            int jcur = iterator[j];
            double v_j_mult_Av = 0;
            for (int p = 0; p < dimension; p++)
                v_j_mult_Av += v_arr[jcur * dimension + p] * Av[p];
            v_j_mult_Av = Lj_arr[jcur] * v_j_mult_Av;
            for (int p = 0; p < dimension; p++)
                Av[p] = K * Av[p] - v_j_mult_Av * v_arr[jcur * dimension + p];
        }
    }

}
