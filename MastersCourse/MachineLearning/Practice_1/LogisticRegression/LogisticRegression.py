from __future__ import division
import math
import random
import os

from numpy import loadtxt, asarray


alpha = 0.05
lamb = 0.08
theta = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]


def h(p):
    return 1.0 / (1.0 + math.exp(-z(p)))


# as polynom of 2 order
def z(p):
    (x, y) = p
    return theta[0] + theta[1] * x + theta[2] * y + theta[3] * x * y + theta[4] * x * x + theta[5] * y * y


def gradient_step():
    step = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    for x, y, q in training_set:
        k = h((x, y)) - q
        # print "k = " + str(k)
        step[0] += k
        step[1] += k * x
        step[2] += k * y
        step[3] += k * x * y
        step[4] += k * x * x
        step[5] += k * y * y

    step[0] *= alpha
    for i in range(1, 6):
        step[i] = alpha * (step[i] + lamb * theta[i])
    return step


points = []


# Load
test_file = "TestFiles/chips.txt"
script_dir = os.path.dirname(__file__)
data = loadtxt(os.path.join(script_dir, test_file), delimiter=',')

split = 0.66
training_set = []
test_set = []

for x in range(len(data) - 1):
    if random.random() < split:
        training_set.append(data[x])
    else:
        test_set.append(data[x])
test_set = asarray(test_set)
training_set = asarray(training_set)
print "Test set len: " + str(test_set.__len__())
print "Training set len: " + str(training_set.__len__())

max_steps = 5000
step_num = 0
while step_num < max_steps:
    step = gradient_step()
    theta = [a - b for a, b in zip(theta, step)]
    step_num += 1

correct = 0
for x, y, q in test_set:
    w = 0 if h((x, y)) <= 0.5 else 1
    if q == w:
        correct += 1

print "Correctness: " + str(correct / len(test_set) * 100) + "%"


