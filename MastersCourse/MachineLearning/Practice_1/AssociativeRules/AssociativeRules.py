from __future__ import division

__author__ = 'killerforfun'

import arff
import os
import time
import itertools
from collections import defaultdict


def read_arff():
    test_file = "TestFiles/supermarket.arff"
    script_dir = os.path.dirname(__file__)
    dataset = arff.load(open(os.path.join(script_dir, test_file), 'rb'))
    return dataset


def tokenize(file_name):
    return [sorted(list(set(e.split()))) for e in
            open(file_name).read().strip().split('\n')]


def find_subsets(x, m):
    if m == 1:
        return [i[0] for i in list(itertools.combinations(x, m))]
    return list(itertools.combinations(x, m))


def do_steps(L1, category_to_ids, ids_to_category, supp_threshold, k, Lo):
    Lk = []
    LkNew = []
    supp = {}
    for items in itertools.combinations(L1[0], k):
        min_item = items[0]
        for item in items:
            min_item = min(min_item, item, key=L1[0].get)
        for ident in category_to_ids.get(min_item):
            if set(items) <= set(ids_to_category.get(ident)):
                if items in supp:
                    supp[items] += 1
                else:
                    supp[items] = 1
    print ("k = " + str(k))
    print "|Ck| = " + str(len(supp))
    Lk.append({k: v for k, v in supp.iteritems() if v >= supp_threshold})
    Lo.append({frozenset(k): v for k, v in supp.iteritems() if v >= supp_threshold})
    print "|Lk| = " + str(len(Lk[0]))
    if (len(Lk[0])) != 0:
        (LkNew, k) = do_steps(L1, category_to_ids, ids_to_category, supp_threshold, k + 1, Lo)
    if not LkNew and len(filter(None, Lk)):
        return LkNew, k
    return Lk, k


def frequent_itemsets(transactions, supp_threshold):
    L1 = []
    Lo = []
    category_to_ids = defaultdict(list)
    C1 = {}

    for identifier, product_categories in transactions.iteritems():
        for product_category in list(set(product_categories)):
            if not (identifier in category_to_ids[product_category]):
                category_to_ids[product_category].append(identifier)
            if product_category in C1:
                C1[product_category] += 1
            else:
                C1[product_category] = 1
    print "|C1| = " + str(len(C1))
    L1.append({k: v for k, v in C1.iteritems() if v >= supp_threshold})
    Lo.append({k: v for k, v in C1.iteritems() if v >= supp_threshold})
    print "|L1| = " + str(len(L1[0]))
    (Lk, k) = do_steps(L1, category_to_ids, transactions, supp_threshold, 2, Lo)
    print("Ended")

    return Lk, k, Lo


def main():
    start_time = time.time()
    dataset = read_arff().__getitem__('data')
    end_time = time.time()
    print "Time spent reading file = {:.2f} seconds.".format(
        end_time - start_time)
    dd = defaultdict(list)
    # generate id to transactions list
    map(lambda x: dd[x.pop()].append(x[1]), dataset)
    # print dd


    start_time = time.time()
    (Lk, k, Lo) = frequent_itemsets(dd, 500)
    end_time = time.time()
    print "Time spent finding frequent itemsets = {:.2f} seconds.".format(
        end_time - start_time)

    start_time = time.time()

    rules = []

    for ii in range(1, k - 1):
        for i in Lo[ii]:
            for y in (range(1, len(i))):
                subsetq = find_subsets(i, y)
                for zz in subsetq:
                    icopy = set(i)
                    if isinstance(zz, basestring):
                        icopy2 = icopy - {zz}
                        zzcost = Lo[0].get(zz)
                    else:
                        icopy2 = icopy - set(zz)
                        zzcost = Lo[len(zz) - 1].get(frozenset(zz))
                    if len(icopy) == 1:
                        icopycost = Lo[0].get(list(icopy)[0])
                    else:
                        icopycost = Lo[len(icopy) - 1].get(frozenset(icopy))
                    rules.append(str(zz) + " ---> " + str(icopy2)
                                 + " with confidence - " + str(icopycost / zzcost)
                                 + " " + str(icopycost) + " " + str(zzcost))
    print "Total rules: " + str(len(rules))
    for i in rules:
        print str(i)

    end_time = time.time()
    print "Time spent generating rule = {:.2f} seconds.".format(
        end_time - start_time)


if __name__ == "__main__":
    main()