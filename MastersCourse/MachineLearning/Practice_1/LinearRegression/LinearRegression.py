from __future__ import division
import os
import random

from numpy import loadtxt, zeros, ones, array, mean, std, asarray


# Evaluate the linear regression

def normalize(X):
    mean_r = []
    std_r = []

    X_norm = X

    n_c = X.shape[1]
    for i in range(n_c):
        m = mean(X[:, i])
        s = std(X[:, i])
        mean_r.append(m)
        std_r.append(s)
        X_norm[:, i] = (X_norm[:, i] - m) / s
    return X_norm, mean_r, std_r


def compute_cost(X, y, theta):
    # Squered error cost function
    m = y.size

    predictions = X.dot(theta)
    sqErrors = (predictions - y)

    J = (1.0 / m) * sqErrors.T.dot(sqErrors)
    return J


def batch_gradient_descent(X, y, theta, alpha, num_iters):
    # Gradient descent with alpha-num_iters
    m = y.size
    J_history = zeros(shape=(num_iters, 1))

    for i in range(num_iters):

        hyphotesis = X.dot(theta)

        theta_size = theta.size

        for it in range(theta_size):
            temp = X[:, it]
            # print "temp + " + str(temp)
            temp.shape = (m, 1)
            errors_x1 = (hyphotesis - y) * temp

            theta[it][0] -= alpha * (1.0 / m) * errors_x1.sum()

        J_history[i, 0] = compute_cost(X, y, theta)

    return theta, J_history


def batch_squre_test_result_error(mean_r, std_r, theta, test_set):
    X = asarray(test_set[:, :2])
    y = asarray(test_set[:, 2])
    m = y.size

    it = ones(shape=(m, 3))
    it[:, 1:3] = X

    it[:, 1] = (it[:, 1] - array(mean_r[0])) / std_r[0]
    it[:, 2] = (it[:, 2] - array(mean_r[1])) / std_r[1]

    predictions = it.dot(theta)
    predictions = predictions.reshape(1, m)

    return (abs(predictions - y) / y).sum() / m * 100


def main():
    # Load
    test_file = "TestFiles/prices.txt"
    script_dir = os.path.dirname(__file__)
    data = loadtxt(os.path.join(script_dir, test_file), delimiter=',')

    split = 0.66
    training_set = []
    test_set = []

    for x in range(len(data) - 1):
        if random.random() < split:
            training_set.append(data[x])
        else:
            test_set.append(data[x])
    test_set = asarray(test_set)
    training_set = asarray(training_set)
    print "Test set len: " + str(test_set.__len__())
    print "Training set len: " + str(training_set.__len__())

    X = training_set[:, :2]
    y = training_set[:, 2]

    m = y.size

    y.shape = (m, 1)

    # Scale
    x, mean_r, std_r = normalize(X)

    # Interception data
    # y = b0 + b1wi + b2xi +ei
    it = ones(shape=(m, 3))
    it[:, 1:3] = x

    # Number of iteration and learning rate - step
    iterations = 500
    alpha = 0.01

    theta = zeros(shape=(3, 1))

    theta, J_history = batch_gradient_descent(it, y, theta, alpha, iterations)

    print "Result theta: "
    print str(theta)

    house_room = 3
    house_meters = 1650

    #Predict price of a 1650 sq-ft 3 br house
    price = array([1.0, ((house_meters - mean_r[0]) / std_r[0]), ((house_room - mean_r[1]) / std_r[1])]).dot(theta)
    print 'Predicted price of a ' + str(house_meters) + 'sq-ft, ' + str(house_room) + ' room house: %f' % (price)

    print "Average error on test samples:  " + str(batch_squre_test_result_error(mean_r, std_r, theta, test_set)) + "%"


main()