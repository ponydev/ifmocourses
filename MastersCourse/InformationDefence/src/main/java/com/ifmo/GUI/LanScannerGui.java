package com.ifmo.GUI;

import com.ifmo.Logic.Port.PortScanner;
import com.ifmo.Logic.SNMP.SNMPTrapReceiver2;
import com.ifmo.Logic.TimeTasks.TimeArpBroadcast;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class LanScannerGui {
    public static int scanFrequency = 0;
    public static ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    private static int timeoutArpPackets = 10 * 1000;
    private static HashMap<String, String> whiteList = new HashMap<String, String>();
    private static Set<Integer> openedPorts = new HashSet<>();


    public static void main(String[] args) {
        /* Use an appropriate Look and Feel */
        System.setProperty("java.library.path", System.getProperty("user.dir") + "\\jnetpcap.dll");
        System.out.println(System.getProperty("java.library.path"));

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        //Schedule a job for the event-dispatching thread:
        //adding TrayIcon.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI() {
        //Check the SystemTray support
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return;
        }
        final PopupMenu popup = new PopupMenu();
        final TrayIcon trayIcon =
                new TrayIcon(createImage("ss.gif"));


        final SystemTray tray = SystemTray.getSystemTray();

        // Create a popup menu components
        MenuItem startAddressScan = new MenuItem("Запуск сканирования адресов");
        MenuItem startPortScan = new MenuItem("Запуск сканирования портов ЛМ");
        MenuItem addressFile = new MenuItem("Файл эталонной конфигурации адресов");
        MenuItem portFile = new MenuItem("Файл эталонной конфигурации портов ЛМ");
        Menu configuration = new Menu("Настройки");
        Menu configScanTimeout = new Menu("Таймаут ARP пакетов");
        ButtonGroup group = new ButtonGroup();
        MenuItem scanTimeout10 = new MenuItem("10 секунд");
        MenuItem scanTimeout20 = new MenuItem("20 секунд");
        MenuItem scanTimeout30 = new MenuItem("30 секунд");
        Menu configScanFrequency = new Menu("Частота автоматического сканирования");
        MenuItem scanFrequency0 = new MenuItem("Не сканировать");
        MenuItem scanFrequency10 = new MenuItem("10 минут");
        MenuItem scanFrequency30 = new MenuItem("30 минут");
        MenuItem exitItem = new MenuItem("Выход");
        MenuItem SNMPTrap = new MenuItem("Включить SNMP Trap");


        //Add components to popup menu

        popup.add(startAddressScan);
        popup.add(startPortScan);
        popup.add(SNMPTrap);
        popup.addSeparator();
        popup.add(addressFile);
        popup.add(portFile);
        popup.addSeparator();
        popup.add(configuration);
        configuration.add(configScanFrequency);
        configuration.add(configScanTimeout);
        configScanFrequency.add(scanFrequency0);
        configScanFrequency.add(scanFrequency10);
        configScanFrequency.add(scanFrequency30);
        configScanTimeout.add(scanTimeout10);
        configScanTimeout.add(scanTimeout20);
        configScanTimeout.add(scanTimeout30);
        popup.addSeparator();
        popup.add(exitItem);

        trayIcon.setPopupMenu(popup);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
            return;
        }

        trayIcon.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "This dialog box is run from System Tray");
            }
        });


        startAddressScan.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                trayIcon.displayMessage("Lan Scanner",
                        "Запущено сканирование адресов!", TrayIcon.MessageType.INFO);
                new Thread(new TimeArpBroadcast(trayIcon, timeoutArpPackets, whiteList)).start();

            }
        });

        startPortScan.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                trayIcon.displayMessage("Lan Scanner",
                        "Запущено сканирование портов!", TrayIcon.MessageType.INFO);
                new Thread(new PortScanner(trayIcon, openedPorts)).start();
            }
        });


        scanTimeout10.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timeoutArpPackets = 10 * 1000;
                initAutoScan(trayIcon, scanFrequency);
                trayIcon.displayMessage("Lan Scanner",
                        "Таймаут Арп пакетов 10 секунд!", TrayIcon.MessageType.INFO);
            }
        });
        scanTimeout20.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timeoutArpPackets = 20 * 1000;
                initAutoScan(trayIcon, scanFrequency);
                trayIcon.displayMessage("Lan Scanner",
                        "Таймаут Арп пакетов 20 секунд!", TrayIcon.MessageType.INFO);
            }
        });
        scanTimeout30.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timeoutArpPackets = 30 * 1000;
                initAutoScan(trayIcon, scanFrequency);
                trayIcon.displayMessage("Lan Scanner",
                        "Таймаут Арп пакетов 30 секунд!", TrayIcon.MessageType.INFO);
            }
        });


        scanFrequency0.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scanFrequency = 0;
                initAutoScan(trayIcon, scanFrequency);
                trayIcon.displayMessage("Lan Scanner",
                        "Планировщик сканирований отключен!", TrayIcon.MessageType.INFO);
            }
        });

        scanFrequency10.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scanFrequency = 10;
                initAutoScan(trayIcon, scanFrequency);
                trayIcon.displayMessage("Lan Scanner",
                        "Сканирование каждые 10 минут!", TrayIcon.MessageType.INFO);
            }
        });

        scanFrequency30.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scanFrequency = 30;
                initAutoScan(trayIcon, scanFrequency);
                trayIcon.displayMessage("Lan Scanner",
                        "Сканирование каждые 30 минут!", TrayIcon.MessageType.INFO);
            }
        });
        SNMPTrap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SNMPTrapReceiver2().run();
                trayIcon.displayMessage("Lan Scanner",
                        "Прием SNMP включен.", TrayIcon.MessageType.INFO);
            }
        });


        addressFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser(System.getProperty("user.dir"));
                int ret = fileopen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    whiteList.clear();
                    BufferedReader br = null;
                    try {
                        br = new BufferedReader(new FileReader(file));
                        String line;
                        while ((line = br.readLine()) != null) {
                            whiteList.put(line.split(" ")[0], line.split(" ")[1]);
                        }
                        br.close();
                    } catch (IOException e1) {
                        System.err.println("Something wrong happen during loading file");
                    }
                    System.out.println(whiteList);
                }
            }
        });

        portFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser(System.getProperty("user.dir"));
                int ret = fileopen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    openedPorts.clear();
                    BufferedReader br = null;
                    try {
                        br = new BufferedReader(new FileReader(file));
                        String line;
                        while ((line = br.readLine()) != null) {
                            openedPorts.add(Integer.parseInt(line));
                        }
                        br.close();
                    } catch (IOException e1) {
                        System.err.println("Something wrong happen during loading file");
                    }
                    System.out.println(openedPorts);
                }
            }
        });
//
//        cb2.addItemListener(new ItemListener() {
//            public void itemStateChanged(ItemEvent e) {
//                int cb2Id = e.getStateChange();
//                if (cb2Id == ItemEvent.SELECTED) {
//                    trayIcon.setToolTip("Sun TrayIcon");
//                } else {
//                    trayIcon.setToolTip(null);
//                }
//            }
//        });
//
//        ActionListener listener = new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                MenuItem item = (MenuItem) e.getSource();
//                //TrayIcon.MessageType type = null;
//                System.out.println(item.getLabel());
//                if ("Error".equals(item.getLabel())) {
//                    //type = TrayIcon.MessageType.ERROR;
//                    trayIcon.displayMessage("Sun TrayIcon Demo",
//                            "This is an error message", TrayIcon.MessageType.ERROR);
//
//                } else if ("Warning".equals(item.getLabel())) {
//                    //type = TrayIcon.MessageType.WARNING;
//                    trayIcon.displayMessage("Sun TrayIcon Demo",
//                            "This is a warning message", TrayIcon.MessageType.WARNING);
//
//                } else if ("Info".equals(item.getLabel())) {
//                    //type = TrayIcon.MessageType.INFO;
//                    trayIcon.displayMessage("Sun TrayIcon Demo",
//                            "This is an info message", TrayIcon.MessageType.INFO);
//
//                } else if ("None".equals(item.getLabel())) {
//                    //type = TrayIcon.MessageType.NONE;
//                    trayIcon.displayMessage("Sun TrayIcon Demo",
//                            "This is an ordinary message", TrayIcon.MessageType.NONE);
//                }
//            }
//        };
//
//        errorItem.addActionListener(listener);
//        infoItem.addActionListener(listener);
//        noneItem.addActionListener(listener);

        exitItem.addActionListener(new

                                           ActionListener() {
                                               public void actionPerformed(ActionEvent e) {
                                                   tray.remove(trayIcon);
                                                   System.exit(0);
                                               }
                                           }

        );


    }

    public static void initAutoScan(TrayIcon trayIcon, int scanFrequency) {
        if (scanFrequency > 0) {
            executor.shutdown();
            executor = Executors.newScheduledThreadPool(1);
            executor.scheduleAtFixedRate(new TimeArpBroadcast(trayIcon, timeoutArpPackets, whiteList), 0, scanFrequency, TimeUnit.MINUTES);
        } else {
            executor.shutdown();
        }
    }


    //Obtain the image URL
    protected static Image createImage(String resourceName) {
//        URL imageURL = LanScannerGui.class.getResource("image/" + resourceName);
        URL imageURL = Thread.currentThread().getContextClassLoader().getResource("image/" + resourceName);
        if (imageURL == null) {
            System.err.println("Resource not found: " + "image/" + resourceName);
            return null;
        } else {
            return (new ImageIcon(imageURL, "tray icon")).getImage();
        }
    }


}