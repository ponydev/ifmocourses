package com.ifmo.HelpPackage;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 21.01.2015
 * To change this template use File | Settings | File Templates.
 */
public class Utils {

    public static String macByteToString(byte[] mac) {
        StringBuilder sb = new StringBuilder(18);
        for (byte b : mac) {
            if (sb.length() > 0)
                sb.append(':');
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public static String ipByteToString(byte[] rawBytes) {
        int i = 4;
        String ipAddress = "";
        for (byte raw : rawBytes) {
            ipAddress += (raw & 0xFF);
            if (--i > 0) {
                ipAddress += ".";
            }
        }

        return ipAddress;
    }

    public static byte[] ipStringToByte(String ipAdd) throws UnknownHostException {
        InetAddress ip = InetAddress.getByName(ipAdd);
        return ip.getAddress();
    }
}
