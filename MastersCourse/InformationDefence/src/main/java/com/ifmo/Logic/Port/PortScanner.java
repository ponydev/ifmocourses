package com.ifmo.Logic.Port;

import java.awt.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 26.02.2015
 * To change this template use File | Settings | File Templates.
 */
public class PortScanner implements Runnable {
    private Set<Integer> openedPorts;
    private final TrayIcon trayIcon;

    public PortScanner(TrayIcon trayIcon, Set<Integer> openedPorts) {
        this.openedPorts = openedPorts;
        this.trayIcon = trayIcon;
    }

    @Override
    public void run() {
        final ExecutorService es = Executors.newFixedThreadPool(50);
        final String ip = "127.0.0.1";
        final int timeout = 200;
        final ArrayList<Future<Boolean>> futures = new ArrayList<>();
        for (int port = 0; port <= 65535; port++) {
            futures.add(portIsOpen(es, ip, port + 1, timeout));
        }
        es.shutdown();
        int openPorts = 0;
        String msg = "";
        for (int port = 0; port < 65536; port++) {
            try {
                if (futures.get(port).get()) {
                    if (!openedPorts.contains(port)) {
                        msg += port + " ";
                        openPorts++;
                    }
                }
            } catch (InterruptedException e) {
                System.out.println(e.getLocalizedMessage());
            } catch (ExecutionException e) {
                System.out.println(e + "1");
            }
        }
        if (openPorts == 0)
            msg += "Открытых портов нет!";
        else
            msg = "Открытые порты: " + msg;
        trayIcon.displayMessage("Lan Scanner",
                msg, TrayIcon.MessageType.WARNING);
    }


    protected static Future<Boolean> portIsOpen(final ExecutorService es, final String ip, final int port, final int timeout) {
        return es.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                try {
                    Socket socket = new Socket();
                    socket.connect(new InetSocketAddress(ip, port), timeout);
                    socket.close();
                    return true;
                } catch (Exception ex) {
                    return false;
                }
            }
        });
    }
}
