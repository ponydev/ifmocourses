package com.ifmo.Logic.ARP;

import com.ifmo.HelpPackage.Utils;
import org.jnetpcap.Pcap;
import org.jnetpcap.packet.JMemoryPacket;
import org.jnetpcap.protocol.JProtocol;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Arp;

import java.net.UnknownHostException;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 21.01.2015
 * To change this template use File | Settings | File Templates.
 */
public class ArpBroadcaster implements Runnable {
    private byte[] macAddressFrom;
    private byte[] ipAddressFrom;
    private Pcap pcap;
    private String scanLevel;
    private int waitingTime;

    public ArpBroadcaster(Pcap pcap, byte[] macAddressFrom, byte[] ipAddressFrom, String scanLevel, int waitingTime) {
        this.ipAddressFrom = ipAddressFrom;
        this.macAddressFrom = macAddressFrom;
        this.pcap = pcap;
        this.scanLevel = scanLevel;
        this.waitingTime = waitingTime;
    }

    //TODO change scanLevel ip
    @Override
    public void run() {
        System.out.println("Scan Started");
        String testPacket = " c4 04 15 6d  50 d5 c8 be  19 89 ad 2b  08 06 00 01 " +
                " 08 00 06 04  00 01 c8 be  19 89 ad 2b  c0 a8 03 0a " +
                " 00 00 00 00  00 00 c0 a8  03 06";

        JMemoryPacket jp = new JMemoryPacket(JProtocol.ETHERNET_ID, testPacket);

        Arp ar = jp.getHeader(new Arp());
        Ethernet et = jp.getHeader(new Ethernet());

        jp.scan(JProtocol.ETHERNET_ID);

        ar.setByteArray(ar.spaOffset() / 8, ipAddressFrom);
        ar.setByteArray(ar.spaOffset() / 8 - 6, macAddressFrom);

        et.source(macAddressFrom);
        et.destination(new byte[]{(byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255});

        String formulateIp;
        for (int jj = 0; jj < 256; jj++) {
            try {
                formulateIp = Utils.ipByteToString(ipAddressFrom).substring(0, Utils.ipByteToString(ipAddressFrom).lastIndexOf('.') + 1) + jj;
                System.out.println(formulateIp);
                ar.setByteArray(ar.tpaOffset() / 8,
                        Utils.ipStringToByte(formulateIp));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            jp.scan(JProtocol.ETHERNET_ID);
            pcap.sendPacket(jp);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Scan Completed");
        try {
            Thread.sleep(waitingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pcap.breakloop();
    }
}
