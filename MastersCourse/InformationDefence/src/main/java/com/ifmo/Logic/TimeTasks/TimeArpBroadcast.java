package com.ifmo.Logic.TimeTasks;

import com.ifmo.Logic.ARP.ArpSniffer;

import java.awt.*;
import java.util.HashMap;

public class TimeArpBroadcast implements Runnable {

    private final TrayIcon trayIcon;
    private int timeoutArpPackets;
    private HashMap<String, String> whiteList;

    public TimeArpBroadcast(TrayIcon trayIcon, int timeoutArpPackets, HashMap<String, String> whiteList) {
        this.trayIcon = trayIcon;
        this.timeoutArpPackets = timeoutArpPackets;
        this.whiteList = whiteList;
    }

    public void run() {
        trayIcon.displayMessage("Lan Scanner",
                "Запущено сканирование адресов!", TrayIcon.MessageType.INFO);
        ArpSniffer as = new ArpSniffer(timeoutArpPackets);
        Thread asThread = new Thread(as);
        asThread.start();
        try {
            asThread.join();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        String all = "";
        String msg = "";
        boolean problemsNotFound = true;
        for (String ip : as.getScannedList().keySet()) {
            all += ip + " | " + as.getScannedList().get(ip) + "\n";
            if (whiteList != null)
                if (whiteList.containsKey(ip)) {
                    if (!whiteList.get(ip).equals(as.getScannedList().get(ip))) {
                        msg += ip + " зарегистрирован под адресом -  " + whiteList.get(ip) + "но использован " + as.getScannedList().get(ip) + "\n";
                        problemsNotFound = false;
                    }
                } else {
                    msg += ip + " не зарегистрирован!" + "\n";
                    problemsNotFound = false;
                }
        }
        if (problemsNotFound)
            msg += "Все нормально!";
        if (whiteList != null && whiteList.isEmpty())
            msg += "Лист разрешенных пар не сконфигурирован. Найдены: " + all + "\n";
        trayIcon.displayMessage("Lan Scanner",
                msg, TrayIcon.MessageType.WARNING);

    }
}