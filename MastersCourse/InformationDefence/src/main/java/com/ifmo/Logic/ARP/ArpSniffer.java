package com.ifmo.Logic.ARP;


import com.ifmo.HelpPackage.Utils;
import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.network.Arp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 20.01.2015
 * To change this template use File | Settings | File Templates.
 */


public class ArpSniffer implements Runnable {
    private HashMap<String, String> ipToMacReceived;
    private final int timeoutArpPackets;
    public HashMap<String, String> getScannedList() {
        return ipToMacReceived;
    }

    public ArpSniffer (int timeoutArpPackets) {
      this.timeoutArpPackets = timeoutArpPackets;
    }

    @Override
    public void run() {
        try {
            ipToMacReceived = new HashMap<String, String>();

            //TODO do somethign with usable device
            // This list is going to return all the devices which can be used for the capture
            List<PcapIf> alldevs = new ArrayList<PcapIf>();

            // For error messages
            StringBuilder errbuf = new StringBuilder();

            // This loops is used to calculate the list of devices that can be used
            int r = Pcap.findAllDevs(alldevs, errbuf);
            System.out.println(r);

            if (r != Pcap.OK) {
                System.err.printf("Can't read list of devices, error is %s", errbuf.toString());
                return;
            }

            System.out.println("Network devices found:");

            int i = 0; // Counter for the loop


            for (PcapIf device : alldevs) {
                String description = (device.getDescription() != null) ? device.getDescription() : "No description available";
                System.out.printf("#%d: %s [%s]\n", i++, device.getName(), description);
            }

            System.out.println("choose the one device from above list of devices");
            PcapIf device = alldevs.get(0);   //ch

            int snaplen = 64 * 1024;           // Capture all packets, no truncation
            int flags = Pcap.MODE_PROMISCUOUS; // capture all packets
            int timeout = 10 * 1000;           // 10 seconds in milliseconds

            //Open the selected device to capture packets
            final Pcap pcap = Pcap.openLive(device.getName(), snaplen, flags, timeoutArpPackets, errbuf);

            if (pcap == null) {
                System.err.printf("Error while opening device for capture: "
                        + errbuf.toString());
                return;
            }
            System.out.println("device opened");

            final byte[] ipOut = device.getAddresses().get(0).getAddr().getData();
            final byte[] macOut = device.getHardwareAddress();

            new Thread(new ArpBroadcaster(pcap, macOut, ipOut, "", timeoutArpPackets)).start();
            PcapPacketHandler<String> handler = new PcapPacketHandler<String>() {

                Arp arp = new Arp();

                public void nextPacket(PcapPacket packet, String user) {

                    if (packet.hasHeader(arp)) {
                        if (arp.operationEnum() == Arp.OpCode.REPLY) {
                            System.out.println("Reply came " + Utils.ipByteToString(arp.spa()) + " " + Utils.macByteToString(arp.sha()));
                            System.out.println("Reply came to " + Utils.ipByteToString(arp.tpa()) + " " + Utils.macByteToString(arp.tha()));
                            if (Arrays.equals(ipOut, arp.tpa()) && Arrays.equals(macOut, arp.tha())) {
                                ipToMacReceived.put(Utils.ipByteToString(arp.spa()), Utils.macByteToString(arp.sha()));
                            }
                        }
                    }
                }
            };
            pcap.loop(Pcap.LOOP_INFINITE, handler, "Loop end!");

            pcap.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
