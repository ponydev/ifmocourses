package secondLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class D2 {
	private static String in = "bluered.in";
	private static String out = "bluered.out";
	private int n;
	private int m;
	private int v;
	private int w;
	private int color;
	private boolean[] used;
	private ArrayList<Integer> binar;
	private ArrayList<Integer>[] outList;
	private ArrayList<edge> edgeList;
	private fraction[] game1;
	private boolean[] notnull1;
	private fraction[] game2;
	private boolean[] notnull2;
	private fraction[] res;

	class fraction {
		int en;
		int den;

		fraction(int en, int den) {
			this.en = en;
			this.den = den;
		}

		public int getEn() {
			return en;
		}

		public int getDen() {
			return den;
		}

		public void setEn(int a) {
			this.en = a;
		}

		public void setDen(int a) {
			this.den = a;
		}

		public fraction sum(fraction b) {
			return new fraction(this.getEn() * b.getDen() + this.getDen()
					* b.getEn(), b.getDen() * this.getDen());
		}

		public fraction sum(int b) {
			return new fraction(this.getEn() + this.getDen() * b, this.getDen());
		}

		@Override
		public String toString() {
			return this.getEn() + " " + this.getDen();
		}

		public fraction max(fraction a) {
			if ((this.sum(a.negate())).getEn() < 0) {
				return a;
			}
			return this;
		}

		public fraction min(fraction a) {
			if ((this.sum(a.negate())).getEn() > 0) {
				return a;
			}
			return this;
		}

		public fraction negate() {
			return new fraction(-this.getEn(), this.getDen());
		}

		public int compare() {
			if (this.getEn() > 0)
				return 1;
			if (this.getEn() < 0)
				return -1;
			return 0;
		}

		public int compare(fraction a) {
			if (this.sum(a.negate()).getEn() > 0)
				return 1;
			if (this.sum(a.negate()).getEn() < 0)
				return -1;
			return 0;
		}

	}

	class edge {
		private int v;
		private int w;
		private int color;
		private int qualEnum;
		private int qualDenum;

		public edge(int v, int w, int color) {
			this.v = v;
			this.w = w;
			this.color = color;
		}

		public edge(edge another) {
			this.v = another.v;
			this.w = another.w;
			this.color = another.color;
		}

		public int getV() {
			return v;
		}

		public int getW() {
			return w;
		}

		public int getColor() {
			return color;
		}

		public int getQualEnum() {
			return qualEnum;
		}

		public int getQualDenum() {
			return qualDenum;
		}

		@Override
		public String toString() {
			if (color == 0)
				return v + "-->" + w + "; Blue";
			return v + "-->" + w + " ;Red";
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null)
				return false;
			if (obj.getClass() != getClass())
				return false;

			edge an = (edge) obj;
			if (((an.getV() == this.getV()) && (an.getW() == this.getW()) && (an
					.getColor() == this.getColor()))
					|| ((an.getV() == this.getW())
							&& (an.getW() == this.getV()) && (an.getColor() == this
							.getColor())))
				return true;
			return false;

		}

		@Override
		public int hashCode() {
			int hash = 1;
			hash = hash * 31 + Integer.valueOf(this.getV()).hashCode();
			hash = hash * 31 + Integer.valueOf(this.getW()).hashCode()
					+ Integer.valueOf(this.getColor()).hashCode();
			;
			return hash;
		}

	}

	public static void main(String[] args) {
		try {
			new D2().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	@SuppressWarnings("unchecked")
	private void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));
		n = sc.nextInt();
		m = sc.nextInt();
		outList = new ArrayList[n];
		edgeList = new ArrayList<edge>();
		binar = new ArrayList<Integer>();
		res = new fraction[(1 << (m))];
		used = new boolean[(1 << (m))];
		oldmasks = new int[(1 << (m))];
		game1 = new fraction[(1 << (m))];
		game2 = new fraction[(1 << (m))];
		oldmasksUsed = new boolean[(1 << (m))];
		Arrays.fill(game1, new fraction(0, 1));
		Arrays.fill(game2, new fraction(0, 1));
		notnull1 = new boolean[(1 << (m))];
		notnull2 = new boolean[(1 << (m))];
		Arrays.fill(oldmasks, 0);
		Arrays.fill(used, false);
		Arrays.fill(oldmasksUsed, false);
		Arrays.fill(notnull1, false);
		Arrays.fill(notnull2, false);

		for (int i = 0; i < n; i++) {
			outList[i] = new ArrayList<>();
		}
		int bb = 1;
		for (int i = 0; i < m; i++) {
			v = sc.nextInt();
			w = sc.nextInt();
			color = sc.nextInt();
			binar.add(bb);
			bb *= 2;
			outList[v - 1].add(i);
			edgeList.add(new edge(v - 1, w - 1, color));
			if (v == w)
				continue;
			outList[w - 1].add(i);
		}
		sc.close();

		for (int k = 0; k < m; k++) {
			edge j = edgeList.get(k);
			if (j.getV() == 0 || j.getW() == 0) {
				int mask = binar.get(k);
				res[mask] = (j.getColor() == 1) ? new fraction(-1, 1)
						: new fraction(1, 1);

				used[mask] = true;
			}
		}
		used[0] = true;
		res[0] = new fraction(0, 1);
		// System.out.println("outList = " + Arrays.toString(outList));
		// System.out.println("edgeList = " + edgeList);
		// System.out.println("binar = " + binar);
		// System.out.println("res = " + Arrays.toString(res));
		// System.out.println("used = " + Arrays.toString(used));
		solveRB((1 << m) - 1);
		// System.out.println(Arrays.toString(res));
		System.out.println("ANS = " + res[(1 << (m)) - 1]);
		outStr.flush();
		outStr.close();
		// System.out.println(Arrays.toString(outList));
	}

	public void solveRB(int mask) {
		if (used[mask])
			return;
		edge j;
		for (int i = 0; i < m; i++) {
			j = edgeList.get(i);
			// System.out.println(Arrays.toString(game1) + "  ------  " +
			// Arrays.toString(game2));
			if ((mask & binar.get(i)) == 0)
				continue;

			int tmp_mask = (mask ^ binar.get(i));
			tmp_mask = dfs(tmp_mask);
			//return;
			if (!used[tmp_mask])
				solveRB(tmp_mask);
			if (j.getColor() == 0) {
				// System.out.println("game1 = " +game1[mask]);
				if (!notnull1[mask]) {
					notnull1[mask] = true;
					game1[mask] = res[tmp_mask];
					// System.out.println("game1 = " +game1[mask]);
				} else {
					if (game1[mask].compare(res[tmp_mask]) < 0)
						game1[mask] = res[tmp_mask];
//					System.out.println("game1 = " + game1[mask]);
					// System.out.println("res = " +res[tmp_mask]);
					// System.out.println("game12 = " +game1[mask]);
				}
			} else {
				// System.out.println("game2 = " +game2[mask]);
				if (!notnull2[mask]) {
					notnull2[mask] = true;
					game2[mask] = res[tmp_mask];
				} else {
					if (game2[mask].compare(res[tmp_mask]) > 0)
						game2[mask] = res[tmp_mask];
					// System.out.println("res = " +res[tmp_mask]);
					// System.out.println("game21 = " +game2[mask]);
				}
			}
			// System.out.println(Arrays.toString(game1) + "  ------  " +
			// Arrays.toString(game2));
			// System.out.println(game1[mask]);
		}
		// System.out.println(game1[mask] + "   !!!   " + game2[mask]);

		res[mask] = findValues(mask);
		// System.out.println(mask);
		// System.out.println(Arrays.toString(used));
		used[mask] = true;
	}

	int[] oldmasks;
	boolean[] oldmasksUsed;

	private int dfs(int mask) {
		if (oldmasksUsed[mask])
			return oldmasks[mask];
		int tmp = mask;
		boolean[] used = new boolean[n];
		bugaga = tmp;
		small_dfs(0, used);
		oldmasks[mask] = mask ^ bugaga;
		oldmasksUsed[mask] = true;
		return oldmasks[mask];
	}
	Integer bugaga;
	private void small_dfs(int i, boolean[] used) {
//		System.out.println("----------");
//		System.out.println(i+" "+bugaga+" "+Arrays.toString(used));
		if (used[i])
			return;
		used[i] = true;
		for (int x = 0; x < outList[i].size(); x++) {
//			System.out.println(bugaga);
			int j = outList[i].get(x);
//			System.out.println(j);
			if ((bugaga & binar.get(j)) == 0)
				continue;
//			System.out.println(j);

			if (edgeList.get(j).getV() == i) {
				bugaga = bugaga ^ binar.get(j);
//				 System.out.println("MAAAAAASK1 = "+bugaga);
				small_dfs(edgeList.get(j).getW(),
						used);
			} else if (edgeList.get(j).getW() == i) {
				// System.out.println("MAAAAAASK2 = "+mask +
				// "   "+binar.get(j));
				bugaga = bugaga ^ binar.get(j);
				small_dfs(edgeList.get(j).getV(),
						 used);
			}
		}
		//return mask;

	}

	public fraction findValues(int mask) {

		if (notnull1[mask] && notnull2[mask]) {
			fraction a = game1[mask];
			fraction b = game2[mask];
			if (a.compare() < 0 && b.compare() > 0)
				return new fraction(0, 1);
			if (a.compare(b) < 0 && a.compare() >= 0) {
				fraction q = new fraction((a.getEn() - a.getEn() % a.getDen())
						/ a.getDen(), 1);
				fraction v = new fraction(1, 1);
				while (q.compare(a) <= 0 || q.compare(b) >= 0) {
					if (q.compare(a) <= 0) {
						q = q.sum(v);
						v.setDen(v.getDen() * 2);
					} else if (q.compare(b) >= 0) {
						v.setEn(-v.getEn());
						q = q.sum(v);
						v.setEn(-v.getEn());
						v.setDen(v.getDen() * 2);
					}
				}
				return q;
			} else if (a.compare(b) < 0 && b.compare() <= 0) {
				fraction q = new fraction((b.getEn() - b.getEn() % b.getDen())
						/ b.getDen(), 1);
				fraction v = new fraction(-1, 1);

				while (q.compare(a) <= 0 || q.compare(b) >= 0) {
					if (q.compare(a) <= 0) {
						v.setEn(-v.getEn());
						q = q.sum(v);
						v.setEn(-v.getEn());
						v.setDen(v.getDen() * 2);
					} else if (q.compare(b) >= 0) {
						q = q.sum(v);
						v.setDen(v.getDen() * 2);
					}
				}
				return q;
			}
		}
		if (notnull1[mask] && !notnull2[mask]) {
			return new fraction((game1[mask].getEn() - game1[mask].getEn()
					% game1[mask].getDen())
					/ game1[mask].getDen() + 1, 1);
		}

		if (!notnull1[mask] && notnull2[mask]) {
			return new fraction((game2[mask].getEn() - game2[mask].getEn()
					% game2[mask].getDen())
					/ game2[mask].getDen() - 1, 1);
		}

		return new fraction(0, 1);

	}

	public int GCD(int a, int b) {
		return b == 0 ? a : GCD(b, a % b);
	}

	public fraction simplify(fraction a) {
		if (a.getEn() == 0)
			return new fraction(0, 1);
		int g = GCD(a.getEn(), a.getDen());
		if (g < 0)
			return new fraction(a.getEn() / (-g), a.getDen() / (-g));
		return new fraction(a.getEn() / g, a.getDen() / g);
	}

}