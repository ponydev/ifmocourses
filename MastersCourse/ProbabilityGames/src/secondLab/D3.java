package secondLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class D3 {
	private static String in = "bluered.in";
	private static String out = "bluered.out";
	private int n;
	private int m;
	private int v;
	private int w;
	private int color;
	private boolean[] used;
	int[] oldmasks;
	boolean[] oldmasksUsed;
	private ArrayList<Integer> prebuiltBinaries;
	private ArrayList<Integer>[] outList;
	private ArrayList<edge> edgeList;
	private fraction[] game1;
	private boolean[] game1nulls;
	private fraction[] game2;
	private boolean[] game2nulls;
	private fraction[] res;
	private int combNumb = 0;

	class fraction {
		int en;
		int den;

		fraction(int en, int den) {
			this.en = en;
			this.den = den;
		}

		public int getEn() {
			return en;
		}

		public int getDen() {
			return den;
		}

		public void setEn(int a) {
			this.en = a;
		}

		public void setDen(int a) {
			this.den = a;
		}

		public fraction sum(fraction b) {
			return new fraction(this.getEn() * b.getDen() + this.getDen()
					* b.getEn(), b.getDen() * this.getDen());
		}

		public fraction sum(int b) {
			return new fraction(this.getEn() + this.getDen() * b, this.getDen());
		}

		@Override
		public String toString() {
			return this.getEn() + " " + this.getDen();
		}

		public fraction max(fraction a) {
			if ((this.sum(a.negate())).getEn() < 0) {
				return a;
			}
			return this;
		}

		public fraction min(fraction a) {
			if ((this.sum(a.negate())).getEn() > 0) {
				return a;
			}
			return this;
		}

		public fraction negate() {
			return new fraction(-this.getEn(), this.getDen());
		}

		public int compare() {
			if (this.getEn() > 0)
				return 1;
			if (this.getEn() < 0)
				return -1;
			return 0;
		}

		public int compare(fraction a) {
			if (this.sum(a.negate()).getEn() > 0)
				return 1;
			if (this.sum(a.negate()).getEn() < 0)
				return -1;
			return 0;
		}

	}

	class edge {
		private int v;
		private int w;
		private int color;
		private int qualEnum;
		private int qualDenum;

		public edge(int v, int w, int color) {
			this.v = v;
			this.w = w;
			this.color = color;
		}

		public edge(edge another) {
			this.v = another.v;
			this.w = another.w;
			this.color = another.color;
		}

		public int getV() {
			return v;
		}

		public int getW() {
			return w;
		}

		public int getColor() {
			return color;
		}

		public int getQualEnum() {
			return qualEnum;
		}

		public int getQualDenum() {
			return qualDenum;
		}

		@Override
		public String toString() {
			if (color == 0)
				return v + "-->" + w + "; Blue";
			return v + "-->" + w + " ;Red";
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null)
				return false;
			if (obj.getClass() != getClass())
				return false;

			edge an = (edge) obj;
			if (((an.getV() == this.getV()) && (an.getW() == this.getW()) && (an
					.getColor() == this.getColor()))
					|| ((an.getV() == this.getW())
							&& (an.getW() == this.getV()) && (an.getColor() == this
							.getColor())))
				return true;
			return false;

		}

		@Override
		public int hashCode() {
			int hash = 1;
			hash = hash * 31 + Integer.valueOf(this.getV()).hashCode();
			hash = hash * 31 + Integer.valueOf(this.getW()).hashCode()
					+ Integer.valueOf(this.getColor()).hashCode();
			;
			return hash;
		}

	}

	public static void main(String[] args) {
		try {
			new D3().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	@SuppressWarnings("unchecked")
	private void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));
		n = sc.nextInt();
		m = sc.nextInt();
		outList = new ArrayList[n];
		edgeList = new ArrayList<edge>();
		prebuiltBinaries = new ArrayList<Integer>();
		combNumb = (1 << (m));
		res = new fraction[combNumb];
		used = new boolean[combNumb];
		oldmasks = new int[combNumb];
		game1 = new fraction[combNumb];
		game2 = new fraction[combNumb];
		oldmasksUsed = new boolean[combNumb];
		game1nulls = new boolean[combNumb];
		game2nulls = new boolean[combNumb];
		Arrays.fill(game1nulls, true);
		Arrays.fill(game2nulls, true);

		for (int i = 0; i < n; i++) {
			outList[i] = new ArrayList<>();
		}
		int bin = 1;
		for (int i = 0; i < m; i++) {
			v = sc.nextInt();
			w = sc.nextInt();
			color = sc.nextInt();
			prebuiltBinaries.add(bin);
			bin = bin * 2;
			outList[v - 1].add(i);
			edgeList.add(new edge(v - 1, w - 1, color));
			if (v == w)
				continue;
			outList[w - 1].add(i);
		}
		sc.close();

		for (int k = 0; k < m; k++) {
			edge j = edgeList.get(k);
			if (j.getV() == 0 || j.getW() == 0) {
				int mask = prebuiltBinaries.get(k);
				if (j.getColor() == 1)
					res[mask] = new fraction(-1, 1);
				else
					res[mask] = new fraction(1, 1);
				used[mask] = true;
			}
		}
		used[0] = true;
		res[0] = new fraction(0, 1);
		// System.out.println("outList = " + Arrays.toString(outList));
		// System.out.println("edgeList = " + edgeList);
		// System.out.println("binar = " + binar);
		// System.out.println("res = " + Arrays.toString(res));
		// System.out.println("used = " + Arrays.toString(used));
		solveRB(combNumb - 1);
		outStr.println(simplify(res[combNumb - 1]));
		outStr.flush();
		outStr.close();
	}

	public void solveRB(int mask) {
		if (used[mask])
			return;
		edge j;
		for (int i = 0; i < m; i++) {
			j = edgeList.get(i);
			if ((mask & prebuiltBinaries.get(i)) == 0)
				continue;

			int tmp_mask = (mask ^ prebuiltBinaries.get(i));
			tmp_mask = fixEdges(tmp_mask);
			if (!used[tmp_mask])
				solveRB(tmp_mask);
			if (j.getColor() == 0) {
				if (game1nulls[mask]) {
					game1nulls[mask] = false;
					game1[mask] = res[tmp_mask];
				} else {
					if (game1[mask].compare(res[tmp_mask]) < 0)
						game1[mask] = res[tmp_mask];
				}
			} else {
				if (game2nulls[mask]) {
					game2nulls[mask] = false;
					game2[mask] = res[tmp_mask];
				} else {
					if (game2[mask].compare(res[tmp_mask]) > 0)
						game2[mask] = res[tmp_mask];
				}
			}
		}

		res[mask] = findValues(mask);
		used[mask] = true;
	}


	private int fixEdges(int mask) {
		if (oldmasksUsed[mask])
			return oldmasks[mask];
		int tmp = mask;
		boolean[] used = new boolean[n];
		bugaga = tmp;
		dfs(0, used);
		oldmasks[mask] = mask ^ bugaga;
		oldmasksUsed[mask] = true;
		return oldmasks[mask];
	}

	Integer bugaga; //TODO bad hack need to fix

	private void dfs(int i, boolean[] used) {
		if (used[i])
			return;
		used[i] = true;
		for (int x = 0; x < outList[i].size(); x++) {
			int j = outList[i].get(x);
			if ((bugaga & prebuiltBinaries.get(j)) == 0)
				continue;

			if (edgeList.get(j).getV() == i) {
				bugaga = bugaga ^ prebuiltBinaries.get(j);
				dfs(edgeList.get(j).getW(), used);
			} else if (edgeList.get(j).getW() == i) {
				bugaga = bugaga ^ prebuiltBinaries.get(j);
				dfs(edgeList.get(j).getV(), used);
			}
		}

	}

	public fraction findValues(int mask) {

		if (!game1nulls[mask] && !game2nulls[mask]) {
			fraction a = game1[mask];
			fraction b = game2[mask];
			if (a.compare() < 0 && b.compare() > 0)
				return new fraction(0, 1);
			if (a.compare(b) < 0 && a.compare() >= 0) {
				fraction q = new fraction((a.getEn() - a.getEn() % a.getDen())
						/ a.getDen(), 1);
				fraction v = new fraction(1, 1);
				while (q.compare(a) <= 0 || q.compare(b) >= 0) {
					if (q.compare(a) <= 0) {
						q = q.sum(v);
						v.setDen(v.getDen() * 2);
					} else if (q.compare(b) >= 0) {
						v.setEn(-v.getEn());
						q = q.sum(v);
						v.setEn(-v.getEn());
						v.setDen(v.getDen() * 2);
					}
				}
				return q;
			} else if (a.compare(b) < 0 && b.compare() <= 0) {
				fraction q = new fraction((b.getEn() - b.getEn() % b.getDen())
						/ b.getDen(), 1);
				fraction v = new fraction(-1, 1);

				while (q.compare(a) <= 0 || q.compare(b) >= 0) {
					if (q.compare(a) <= 0) {
						v.setEn(-v.getEn());
						q = q.sum(v);
						v.setEn(-v.getEn());
						v.setDen(v.getDen() * 2);
					} else if (q.compare(b) >= 0) {
						q = q.sum(v);
						v.setDen(v.getDen() * 2);
					}
				}
				return q;
			}
		}
		if (!game1nulls[mask] && game2nulls[mask]) {
			return new fraction((game1[mask].getEn() - game1[mask].getEn()
					% game1[mask].getDen())
					/ game1[mask].getDen() + 1, 1);
		}

		if (game1nulls[mask] && !game2nulls[mask]) {
			return new fraction((game2[mask].getEn() - game2[mask].getEn()
					% game2[mask].getDen())
					/ game2[mask].getDen() - 1, 1);
		}

		return new fraction(0, 1);

	}

	public int GCD(int a, int b) {
		return b == 0 ? a : GCD(b, a % b);
	}

	public fraction simplify(fraction a) {
		int a1,b1;
		a1 = a.getEn();
		b1 = a.getDen();
		while((a1 % 2 == 0) && (b1 % 2 == 0)) 
		{
			a1 /= 2; 
			b1 /= 2;
		}
		
		return new fraction(a1,b1);
//		if (a.getEn() == 0)
//			return new fraction(0, 1);
//		int g = GCD(a.getEn(), a.getDen());
//		if (g < 0)
//			return new fraction(a.getEn() / (-g), a.getDen() / (-g));
//		return new fraction(a.getEn() / g, a.getDen() / g);
	}

}