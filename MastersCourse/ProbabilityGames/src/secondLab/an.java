package secondLab;

import java.io.*;
import java.util.*;

import secondLab.A.Game;

public class an {

	private static Scanner in;
	private static PrintWriter out;
	public Map<String, Game> sum;

	public static void main(String[] args) throws FileNotFoundException {
		an g = new an();
		in = new Scanner(new FileReader("short.in"));
		out = new PrintWriter("short.out");

		g.solve();

		in.close();
		out.close();
	}

	public class Game {
		public ArrayList<Game> l;
		public ArrayList<Game> r;

		public boolean isRun;
		public boolean isReduced;
		public boolean lt;
		public boolean gt;

		public Game() {
			l = new ArrayList<Game>();
			r = new ArrayList<Game>();
			isRun = false;
			isReduced = false;
			gt = false;
			lt = false;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			for (int i = 0; i < l.size(); ++i) {
				sb.append(l.get(i).toString());
				if (i != l.size() - 1)
					sb.append(",");
			}
			sb.append("|");
			for (int i = 0; i < r.size(); ++i) {
				sb.append(r.get(i).toString());
				if (i != r.size() - 1)
					sb.append(",");
			}
			sb.append("}");

			return sb.toString();
		}

	}

	public class Pair<A, B> {
		public A first;
		public B second;

		public Pair(A first, B second) {
			super();
			this.first = first;
			this.second = second;
		}
	}

	public Game inverseGame(Game g) {
		Game gn = new Game();

		if (!g.l.isEmpty()) {
			for (Game i : g.l)
				gn.r.add(inverseGame(i));
		}

		if (!g.r.isEmpty()) {
			for (Game i : g.r)
				gn.l.add(inverseGame(i));
		}

		return gn;
	}

	public Game deduct(Game g1, Game g2) {
		Game sub = new Game();

		for (int i = 0; i < g1.l.size(); i++) {
			sub.l.add(deduct(g1.l.get(i), g2));
		}

		for (int i = 0; i < g2.r.size(); i++) {
			sub.l.add(deduct(g1, g2.r.get(i)));
		}

		for (int i = 0; i < g1.r.size(); i++) {
			sub.r.add(deduct(g1.r.get(i), g2));
		}

		for (int i = 0; i < g2.l.size(); i++) {
			sub.r.add(deduct(g1, g2.l.get(i)));
		}
		return sub;
	}

	public Game sumGames(Game g1, Game g2) {
		String str = buildString(g1).concat(buildString(g2));

		if (sum.containsKey(str)) {
			return sum.get(str);
		}

		Game t = new Game();
		for (Game i : g1.l) {
			t.l.add(sumGames(i, g2));
		}
		for (Game i : g2.l) {
			t.l.add(sumGames(g1, i));
		}

		for (Game i : g1.r) {
			t.r.add(sumGames(i, g2));
		}
		for (Game i : g2.r) {
			t.r.add(sumGames(g1, i));
		}

		sum.put(str, t);
		return t;
	}

	public void solve() {
		sum = new HashMap<String, Game>();
		pos = 0;
		Game st = parseGame(in.nextLine());
		Game ans = reduceGame(st);
		out.println(buildString(ans));

	}

	public int pos;

	public Game parseGame(String str) {
		Game g = new Game();

		if (str.charAt(pos) == '{') {
			++pos;
			Game t = parseGame(str);
			if (t != null) {
				g.l.add(t);
			}
		} else {
			return null;
		}

		while (str.charAt(pos) == ',') {
			++pos;
			Game t = parseGame(str);
			if (t != null) {
				g.l.add(t);
			}
		}

		++pos;
		Game ng = parseGame(str);
		if (ng != null) {
			g.r.add(ng);
		}

		while (str.charAt(pos) == ',') {
			++pos;
			Game t = parseGame(str);
			if (t != null) {
				g.r.add(t);
			}
		}

		pos++;
		return g;
	}

	public String buildString(Game g) {
		StringBuilder sb = new StringBuilder();

		sb.append("{");

		for (int i = 0; i < g.l.size(); i++) {
			sb.append(buildString(g.l.get(i)));
			if (i < g.l.size() - 1)
				sb.append(",");
		}

		sb.append("|");

		for (int i = 0; i < g.r.size(); i++) {
			sb.append(buildString(g.r.get(i)));
			if (i < g.r.size() - 1)
				sb.append(",");
		}

		sb.append("}");

		return sb.toString();
	}

	public void runGame(Game g) {
		boolean gtl = false;
		boolean ltr = false;

		if (g.isRun)
			return;

		g.isRun = true;

		if (g.l.isEmpty()) {
			g.lt = true;
		} else {
			g.lt = false;
		}

		if (g.r.isEmpty()) {
			g.gt = true;
		} else {
			g.gt = false;
		}

		for (Game i : g.l) {
			runGame(i);
			if (i.gt)
				gtl = true;
		}

		for (Game i : g.r) {
			runGame(i);
			if (i.lt)
				ltr = true;
		}

		if (ltr && gtl) {
			g.lt = false;
			g.gt = false;
		} else if (!ltr && !gtl) {
			g.lt = true;
			g.gt = true;
		} else if (!ltr && gtl) {
			g.lt = false;
			g.gt = true;
		} else if (ltr && !gtl) {
			g.lt = true;
			g.gt = false;
		}
	}

	public Pair<Boolean, Boolean> compareGames(Game g1, Game g2) {
		Game t = deduct(g1, g2);//sumGames(g1, inverseGame(g2));
		runGame(t);
		return new Pair<Boolean, Boolean>(t.gt, t.lt);
	}

	private Game reduceGame(Game g) {

		if (g == null || g.isReduced)
			return g;

		boolean f = true;
		while (f) {
			f = false;
			for (int i = 0; i < g.l.size(); i++) {
				for (int j = 0; j < g.l.size(); j++) {
					if (i != j) {
						reduceGame(g.l.get(i));
						reduceGame(g.l.get(j));
						Pair<Boolean, Boolean> comp = compareGames(g.l.get(i),
								g.l.get(j));
						if (comp.first) {
							f = true;

							g.l.remove(j);
							j--;

							if (j < i - 1) {
								i--;
							}
						}
					}
				}
			}

			for (int i = 0; i < g.l.size(); i++) {
				boolean flag = false;
				Game t = new Game();
				for (Game j : g.l.get(i).r) {
					Pair<Boolean, Boolean> comp = compareGames(j, g);
					if (comp.first) {
						flag = true;
						t = j;
					}
				}

				if (flag) {
					f = true;

					g.l.remove(i);
					for (Game j : t.l) {
						g.l.add(j);
					}
					i--;

					break;
				}
			}

			for (int i = 0; i < g.r.size(); i++) {
				for (int j = 0; j < g.r.size(); j++) {
					if (i != j) {
						reduceGame(g.r.get(i));
						reduceGame(g.r.get(j));
						Pair<Boolean, Boolean> comp = compareGames(g.r.get(i),
								g.r.get(j));
						if (comp.second) {
							f = true;

							g.r.remove(j);
							j--;
							if (j < i - 1) {
								i--;
							}

						}
					}
				}
			}
			for (int i = 0; i < g.r.size(); i++) {
				boolean flag = false;
				Game t = new Game();
				for (Game j : g.r.get(i).l) {
					Pair<Boolean, Boolean> comp = compareGames(g, j);
					if (comp.second) {
						flag = true;
						t = j;
					}
				}

				if (flag) {
					f = true;

					g.r.remove(i);
					for (Game j : t.r) {
						g.r.add(j);
					}
					i--;

					break;
				}
			}
		}
		System.out.println(g);
		g.isReduced = true;
		return g;
	}

}
