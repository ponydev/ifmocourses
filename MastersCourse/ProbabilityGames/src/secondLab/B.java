package secondLab;

import java.io.*;
import java.util.*;

public class B {
	private static String in = "chocolate.in";
	private static String out = "chocolate.out";
	private int[][] a = new int[1000][1000];

	public static void main(String[] args) throws FileNotFoundException {
		try {
			new B().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}

	}

	public void bakePossibleCutcakes() {
		for (int i = 1; i <= 300; i = i * 2) {
			int c = 0;
			for (int j = i; j <= 300; j = j + i) {
				for (int x = 0; x < i; x++) {
					for (int y = 0; y < i; y++) {
						a[i + x][j + y] = c;
						a[j + x][i + y] = -c;
					}
				}
				c++;
			}
		}
	}

	public void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		int n = sc.nextInt();
		int ans = 0;

		bakePossibleCutcakes();

		for (int i = 0; i < n; i++) {
			ans += a[sc.nextInt()][sc.nextInt()];
		}

		if (ans > 0)
			outStr.println("Vova");
		else
			outStr.println("Gena");

		sc.close();
		outStr.flush();
		outStr.close();
	}

}