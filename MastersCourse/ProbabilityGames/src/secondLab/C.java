package secondLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class C {
	private static String in = "hackentree.in";
	private static String out = "hackentree.out";
	private int n;
	private int v;
	private int w;
	private int color;
	private ArrayList<edge>[] outList;
	private boolean[] achieved;
	fraction val = new fraction(BigInteger.ZERO, BigInteger.ONE);

	class fraction {
		BigInteger en;
		BigInteger den;

		fraction(BigInteger en, BigInteger den) {
			this.en = en;
			this.den = den;
		}

		public BigInteger getEn() {
			return en;
		}

		public BigInteger getDen() {
			return den;
		}

		public fraction sum(fraction b) {
			return new fraction((this.getEn().multiply(b.getDen())).add(this
					.getDen().multiply(b.getEn())), this.getDen().multiply(
					b.getDen()));
		}

		public fraction sum(BigInteger b) {
			return new fraction(this.getEn().add((this.getDen().multiply(b))),
					this.getDen());
		}

		public fraction sum(int b) {
			return new fraction(this.getEn().add(
					(this.getDen().multiply(BigInteger.valueOf(b)))),
					this.getDen());
		}

		@Override
		public String toString() {
			return this.getEn() + " " + this.getDen();
		}

	}

	class edge {
		private int v;
		private int w;
		private int color;

		public edge(int v, int w, int color) {
			this.v = v;
			this.w = w;
			this.color = color;
		}

		public int getV() {
			return v;
		}

		public int getW() {
			return w;
		}

		public int getColor() {
			return color;
		}

		@Override
		public String toString() {
			if (color == 0)
				return v + "-->" + w + "; Blue";
			return v + "-->" + w + " ;Red";
		}

	}

	public static void main(String[] args) {
		try {
			new C().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	@SuppressWarnings("unchecked")
	private void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));
		n = sc.nextInt();
		outList = new ArrayList[n];
		achieved = new boolean[n];
		achieved[0] = true;
		for (int i = 0; i < n; i++) {
			outList[i] = new ArrayList<>();
		}
		for (int i = 0; i < n - 1; i++) {
			v = sc.nextInt();
			w = sc.nextInt();
			color = sc.nextInt();
			outList[v - 1].add(new edge(v - 1, w - 1, color));
			outList[w - 1].add(new edge(w - 1, v - 1, color));
		}
		fraction val = new fraction(BigInteger.ZERO, BigInteger.ONE);
		for (edge j : outList[0]) {
			val = val.sum(findValue(j.getW(), j.getColor()));
		}
		outStr.println(simplify(val));
		sc.close();
		outStr.flush();
		outStr.close();

	}

	public fraction findValue(int i, int color) {
		if (achieved[i] == false) {
			achieved[i] = true;
			fraction val = new fraction(BigInteger.ZERO, BigInteger.ONE);
			if (!outList[i].isEmpty()) {
				for (edge j : outList[i]) {
					val = val.sum(findValue(j.getW(), j.getColor()));
				}
			} else {
				if (color == 0)
					return new fraction(BigInteger.ONE, BigInteger.ONE);
				else
					return new fraction(BigInteger.ONE.negate(), BigInteger.ONE);
			}
			return findX(val, color, 1);
		}
		return new fraction(BigInteger.ZERO, BigInteger.ONE);
	}

	public fraction findX(fraction x, int color, int num) {
		if (color == 0) {
			while (x.sum(num).getEn().compareTo(x.sum(num).getDen()) < 0) {
				num++;
			}
			return simplify(new fraction(x.sum(num).getEn(),
					(x.sum(num).getDen()).multiply((BigInteger.ONE
							.add(BigInteger.ONE)).pow(num - 1))));
		} else {
			while (((x.sum(-num).getEn()).compareTo(x.sum(-num).getDen()
					.negate()) > 0)) {
				num++;
			}
			return simplify(new fraction(x.sum(-num).getEn(),
					x.sum(-num)
							.getDen()
							.multiply(
									(BigInteger.ONE.add(BigInteger.ONE))
											.pow(num - 1))));
		}
	}

	public long GCD(long a, long b) {
		if (b == 0)
			return a;
		return GCD(b, a % b);
	}

	public fraction simplify(fraction a) {
		if (a.getEn().compareTo(BigInteger.ZERO) == 0)
			return new fraction(BigInteger.ZERO, BigInteger.ONE);
		BigInteger g = a.getEn().gcd(a.getDen());
		if (g.compareTo(BigInteger.ZERO) < 0)
			return new fraction(a.getEn().divide(g.negate()), a.getDen()
					.divide(g.negate()));
		return new fraction(a.getEn().divide(g), a.getDen().divide(g));
	}
}
