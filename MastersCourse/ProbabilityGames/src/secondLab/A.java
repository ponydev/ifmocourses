package secondLab;

import java.io.*;
import java.util.*;

import secondLab.an.Game;

public class A {
	private static String in = "short.in";
	private static String out = "short.out";
	private int[][] a = new int[1000][1000];

	public static void main(String[] args) throws FileNotFoundException {
		try {
			new A().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}

	}

	public class Game {
		ArrayList<Game> l;
		ArrayList<Game> r;
		boolean isNull;
		boolean solved;
		boolean calc;
		boolean gr1;
		boolean gr2;

		public Game() {
			l = new ArrayList<>();
			r = new ArrayList<>();
			isNull = false;
			solved = false;
			calc = false;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();

			sb.append("{");

			for (int i = 0; i < this.l.size(); i++) {
				sb.append(this.l.get(i).toString());
				if (i < this.l.size() - 1)
					sb.append(",");
			}

			sb.append("|");

			for (int i = 0; i < this.r.size(); i++) {
				sb.append(this.r.get(i).toString());
				if (i < this.r.size() - 1)
					sb.append(",");
			}

			sb.append("}");

			return sb.toString();
		}

		Game deduct(Game a) {
			Game sub = new Game();

			for (int i = 0; i < l.size(); i++) {
				sub.l.add(l.get(i).deduct(a));
			}

			for (int i = 0; i < a.r.size(); i++) {
				sub.l.add(this.deduct(a.r.get(i)));
			}

			for (int i = 0; i < r.size(); i++) {
				sub.r.add(r.get(i).deduct(a));
			}

			for (int i = 0; i < a.l.size(); i++) {
				sub.r.add(this.deduct(a.l.get(i)));
			}
			return sub;
		}

		Game add(Game a) {
			Game addit = new Game();

			for (int i = 0; i < l.size(); i++) {
				addit.l.add(l.get(i).add(a));
			}

			for (int i = 0; i < a.r.size(); i++) {
				addit.l.add(a.r.get(i).add(this));
			}

			for (int i = 0; i < r.size(); i++) {
				addit.r.add(r.get(i).add(a));
			}

			for (int i = 0; i < a.l.size(); i++) {
				addit.r.add(a.l.get(i).add(this));
			}
			return addit;
		}

	}
	
	Game parse(String a, int pos) {
		Game g = new Game();
		if (a.charAt(pos) == '{') {
			Game b = parse(a, ++pos);
			if (!b.isNull)
				g.l.add(b);
		} else {
			g.isNull = true;
			return g;
		}

		while (a.charAt(pos) == ',') {
			Game b = parse(a, ++pos);
			if (!b.isNull)
				g.l.add(b);
		}

		if (a.charAt(pos) == '|') {
			Game b = parse(a, ++pos);
			if (!b.isNull)
				g.r.add(b);
		}

		while (a.charAt(pos) == ',') {
			Game b = parse(a, ++pos);
			if (!b.isNull)
				g.r.add(b);
		}

		// if (a.charAt(pos) == '}') {
		pos++;
		return g;
		// }
	}


	public void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		String s = sc.next();

	//	Game g = parse(a, 0);
		//solve(g);
		//System.out.println(g);
		sc.close();
		outStr.flush();
		outStr.close();
	}

}