package secondLab;
 
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
 
public class D {
    private static String in = "bluered.in";
    private static String out = "bluered.out";
    private int n;
    private int m;
    private int v;
    private int w;
    private int color;
    private ArrayList<edge>[] outList;
    private ArrayList<edge> edgeList;
    private boolean[] checked;
    private long mask = 0;
    private HashMap<Long, fraction> ha;
 
    class fraction {
        int en;
        int den;
 
        fraction(int en, int den) {
            this.en = en;
            this.den = den;
        }
 
        public int getEn() {
            return en;
        }
 
        public int getDen() {
            return den;
        }
 
        public void setEn(int a) {
            this.en = a;
        }
 
        public void setDen(int a) {
            this.den = a;
        }
 
        public fraction sum(fraction b) {
            return new fraction(this.getEn() * b.getDen() + this.getDen()
                    * b.getEn(), b.getDen() * this.getDen());
        }
 
        public fraction sum(int b) {
            return new fraction(this.getEn() + this.getDen() * b, this.getDen());
        }
 
        @Override
        public String toString() {
            return this.getEn() + " " + this.getDen();
        }
 
        public fraction max(fraction a) {
            if ((this.sum(a.negate())).getEn() < 0) {
                return a;
            }
            return this;
        }
 
        public fraction min(fraction a) {
            if ((this.sum(a.negate())).getEn() > 0) {
                return a;
            }
            return this;
        }
 
        public fraction negate() {
            return new fraction(-this.getEn(), this.getDen());
        }
 
        public int compare() {
            if (this.getEn() > 0)
                return 1;
            if (this.getEn() < 0)
                return -1;
            return 0;
        }
 
        public int compare(fraction a) {
            if (this.sum(a.negate()).getEn() > 0)
                return 1;
            if (this.sum(a.negate()).getEn() < 0)
                return -1;
            return 0;
        }
 
    }
 
    class edge {
        private int v;
        private int w;
        private int color;
        private int qualEnum;
        private int qualDenum;
 
        public edge(int v, int w, int color) {
            this.v = v;
            this.w = w;
            this.color = color;
        }
 
        public edge(edge another) {
            this.v = another.v;
            this.w = another.w;
            this.color = another.color;
        }
 
        public int getV() {
            return v;
        }
 
        public int getW() {
            return w;
        }
 
        public int getColor() {
            return color;
        }
 
        public int getQualEnum() {
            return qualEnum;
        }
 
        public int getQualDenum() {
            return qualDenum;
        }
 
        @Override
        public String toString() {
            if (color == 0)
                return v + "-->" + w + "; Blue";
            return v + "-->" + w + " ;Red";
        }
 
        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            if (obj.getClass() != getClass())
                return false;
 
            edge an = (edge) obj;
            if (((an.getV() == this.getV()) && (an.getW() == this.getW()) && (an
                    .getColor() == this.getColor()))
                    || ((an.getV() == this.getW())
                            && (an.getW() == this.getV()) && (an.getColor() == this
                            .getColor())))
                return true;
            return false;
 
        }
 
        @Override
        public int hashCode() {
            int hash = 1;
            hash = hash * 31 + Integer.valueOf(this.getV()).hashCode();
            hash = hash * 31 + Integer.valueOf(this.getW()).hashCode()
                    + Integer.valueOf(this.getColor()).hashCode();
            ;
            return hash;
        }
 
    }
 
    public static void main(String[] args) {
        try {
            new D().solve();
        } catch (IOException e) {
            System.err.println("Smthing bad with IO");
        }
    }
 
    @SuppressWarnings("unchecked")
    private void solve() throws IOException {
        Scanner sc = new Scanner(new FileReader(in));
        PrintWriter outStr = new PrintWriter(new FileWriter(out));
        n = sc.nextInt();
        m = sc.nextInt();
        outList = new ArrayList[n];
        edgeList = new ArrayList<edge>();
        ha = new HashMap<Long, fraction>();
        checked = new boolean[m];
        for (int i = 0; i < n; i++) {
            outList[i] = new ArrayList<>();
        }
 
        for (int i = 0; i < m; i++) {
            v = sc.nextInt();
            w = sc.nextInt();
            color = sc.nextInt();
            outList[v - 1].add(new edge(v - 1, w - 1, color));
            outList[w - 1].add(new edge(w - 1, v - 1, color));
            edgeList.add(new edge(v - 1, w - 1, color));
        }
        // outStr.println(Arrays.toString(outList));
        // outStr.println(edgeList);
        outStr.println(solveRB(outList, edgeList));
        sc.close();
        outStr.flush();
        outStr.close();
        // System.out.println(Arrays.toString(outList));
    }
 
    public fraction solveRB(ArrayList<edge>[] outList, ArrayList<edge> edgeList) {
        fraction blue = null;
        fraction red = null;
        mask = 0;
        boolean zzz = false;
        if (edgeList.size() == 1) {
            if (edgeList.get(0).getColor() == 0) {
                return new fraction(1, 1);
 
            } else {
                return new fraction(-1, 1);
            }
        } else {
            if (edgeList.size() == 0) {
                return new fraction(0, 1);
            } else {
                for (edge j : edgeList) {
                    ArrayList<edge> edgeList1 = new ArrayList<edge>(edgeList);
                    ArrayList<edge>[] outList1 = new ArrayList[n];
                    for (int i = 0; i < n; i++) {
                        outList1[i] = new ArrayList<edge>(outList[i]);
                    }
                    edgeList1.remove(j);
                    outList1[j.getV()].remove(j);
                    outList1[j.getW()].remove(j);
 
                    fixEdges(j, outList1, edgeList1);
 
                    if (ha.containsKey(mask)) {
                        return ha.get(mask);
                    }else {
                        zzz = true;
                    }
                    // System.out.println("________________________");
                    // System.out.println("edge - " + j);
                    // System.out.println(Arrays.toString(outList1));
                    // System.out.println(edgeList1);
                    // System.out.println("________________________");
                    if (j.getColor() == 0) {
                        if (blue == null) {
                            blue = solveRB(outList1, edgeList1);
                        } else {
                            blue = blue.max(solveRB(outList1, edgeList1));
                        }
                    } else {
                        if (red == null) {
                            red = solveRB(outList1, edgeList1);
                        } else {
                            red = red.min(solveRB(outList1, edgeList1));
                        }
                    }
                }
            }
        }
        fraction temp = simplify(findValues(blue, red));
        if (zzz) {
            ha.put(mask, temp);
        }
        // System.out.println("ZNACH!:" + temp);
        return temp;
    }
 
    private void fixEdges(edge j, ArrayList<edge>[] outList,
            ArrayList<edge> edgeList) {
 
        checked = new boolean[outList.length];
        dfs(outList, edgeList, 0);
        for (int i = 0; i < checked.length; i++) {
            if (!checked[i]) {
                edgeList.removeAll(outList[i]);
                outList[i] = new ArrayList<edge>();
            }
            if (checked[i]) {
                mask = (long) (mask + Math.pow(10, i));
            }
 
        }
    }
 
    private void dfs(ArrayList<edge>[] outList, ArrayList<edge> edgeList, int i) {
        if (checked[i] == false) {
            checked[i] = true;
            if (!outList[i].isEmpty()) {
                for (edge j : outList[i]) {
                    dfs(outList, edgeList, j.getW());
                }
            }
        }
    }
 
    public fraction findValues(fraction a, fraction b) {
        // System.out.println("BUGAGAG");
        // System.out.println(a);
        // System.out.println(b);
        // System.out.println("BUGAGAG");
        if (a != null && b != null) {
            if (a.compare() < 0 && b.compare() > 0)
                return new fraction(0, 1);
            if (a.compare(b) < 0 && a.compare() >= 0) {
                fraction q = new fraction((a.getEn() - a.getEn() % a.getDen())
                        / a.getDen(), 1);
                fraction v = new fraction(1, 1);
                while (q.compare(a) <= 0 || q.compare(b) >= 0) {
                    if (q.compare(a) <= 0) {
                        q = q.sum(v);
                        v.setDen(v.getDen() * 2);
                    } else if (q.compare(b) >= 0) {
                        v.setEn(-v.getEn());
                        q = q.sum(v);
                        v.setEn(-v.getEn());
                        v.setDen(v.getDen() * 2);
                    }
                }
                return q;
            } else if (a.compare(b) < 0 && b.compare() <= 0) {
                fraction q = new fraction((b.getEn() - b.getEn() % b.getDen())
                        / b.getDen(), 1);
                fraction v = new fraction(-1, 1);
 
                while (q.compare(a) <= 0 || q.compare(b) >= 0) {
                    if (q.compare(a) <= 0) {
                        v.setEn(-v.getEn());
                        q = q.sum(v);
                        v.setEn(-v.getEn());
                        v.setDen(v.getDen() * 2);
                    } else if (q.compare(b) >= 0) {
                        q = q.sum(v);
                        v.setDen(v.getDen() * 2);
                    }
                }
                return q;
            }
        }
        if (a != null && b == null) {
            return new fraction((a.getEn() - a.getEn() % a.getDen())
                    / a.getDen() + 1, 1);
        }
 
        if (a == null && b != null) {
            return new fraction((b.getEn() - b.getEn() % b.getDen())
                    / b.getDen() - 1, 1);
        }
 
        return new fraction(0, 1);
 
    }
 
    public int GCD(int a, int b) {
        return b == 0 ? a : GCD(b, a % b);
    }
 
    public fraction simplify(fraction a) {
        if (a.getEn() == 0)
            return new fraction(0, 1);
        int g = GCD(a.getEn(), a.getDen());
        if (g < 0)
            return new fraction(a.getEn() / (-g), a.getDen() / (-g));
        return new fraction(a.getEn() / g, a.getDen() / g);
    }
 
}