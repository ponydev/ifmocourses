package secondLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class E {
	private static String in = "hotchpotch.in";
	private static String out = "hotchpotch.out";
	private int n;
	private int m;
	private int v;
	private int w;
	private int color;
	private boolean[] used;
	int[] oldmasks;
	boolean[] oldmasksUsed;
	private ArrayList<Integer> prebuiltBinaries;
	private ArrayList<Integer>[] outList;
	private ArrayList<edge> edgeList;
	private boolean[] game1;
	private boolean[] game1nulls;
	private boolean[] game2;
	private boolean[] game2nulls;
	private int combNumb = 0;

	class fraction {
		int en;
		int den;

		fraction(int en, int den) {
			this.en = en;
			this.den = den;
		}

		public int getEn() {
			return en;
		}

		public int getDen() {
			return den;
		}

		public void setEn(int a) {
			this.en = a;
		}

		public void setDen(int a) {
			this.den = a;
		}

		public fraction sum(fraction b) {
			return new fraction(this.getEn() * b.getDen() + this.getDen()
					* b.getEn(), b.getDen() * this.getDen());
		}

		public fraction sum(int b) {
			return new fraction(this.getEn() + this.getDen() * b, this.getDen());
		}

		@Override
		public String toString() {
			return this.getEn() + " " + this.getDen();
		}

		public fraction max(fraction a) {
			if ((this.sum(a.negate())).getEn() < 0) {
				return a;
			}
			return this;
		}

		public fraction min(fraction a) {
			if ((this.sum(a.negate())).getEn() > 0) {
				return a;
			}
			return this;
		}

		public fraction negate() {
			return new fraction(-this.getEn(), this.getDen());
		}

		public int compare() {
			if (this.getEn() > 0)
				return 1;
			if (this.getEn() < 0)
				return -1;
			return 0;
		}

		public int compare(fraction a) {
			if (this.sum(a.negate()).getEn() > 0)
				return 1;
			if (this.sum(a.negate()).getEn() < 0)
				return -1;
			return 0;
		}

	}

	class edge {
		private int v;
		private int w;
		private int color;
		private int qualEnum;
		private int qualDenum;

		public edge(int v, int w, int color) {
			this.v = v;
			this.w = w;
			this.color = color;
		}

		public edge(edge another) {
			this.v = another.v;
			this.w = another.w;
			this.color = another.color;
		}

		public int getV() {
			return v;
		}

		public int getW() {
			return w;
		}

		public int getColor() {
			return color;
		}

		public int getQualEnum() {
			return qualEnum;
		}

		public int getQualDenum() {
			return qualDenum;
		}

		@Override
		public String toString() {
			if (color == 0)
				return v + "-->" + w + "; Blue";
			return v + "-->" + w + " ;Red";
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null)
				return false;
			if (obj.getClass() != getClass())
				return false;

			edge an = (edge) obj;
			if (((an.getV() == this.getV()) && (an.getW() == this.getW()) && (an
					.getColor() == this.getColor()))
					|| ((an.getV() == this.getW())
							&& (an.getW() == this.getV()) && (an.getColor() == this
							.getColor())))
				return true;
			return false;

		}

		@Override
		public int hashCode() {
			int hash = 1;
			hash = hash * 31 + Integer.valueOf(this.getV()).hashCode();
			hash = hash * 31 + Integer.valueOf(this.getW()).hashCode()
					+ Integer.valueOf(this.getColor()).hashCode();
			;
			return hash;
		}

	}

	public static void main(String[] args) {
		try {
			new E().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	@SuppressWarnings("unchecked")
	private void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));
		n = sc.nextInt();
		m = sc.nextInt();
		outList = new ArrayList[n];
		edgeList = new ArrayList<edge>();
		prebuiltBinaries = new ArrayList<Integer>();
		combNumb = (1 << (m));
		used = new boolean[combNumb];
		oldmasks = new int[combNumb];
		game1 = new boolean[combNumb];
		game2 = new boolean[combNumb];
		oldmasksUsed = new boolean[combNumb];
		game1nulls = new boolean[combNumb];
		game2nulls = new boolean[combNumb];
		Arrays.fill(game1nulls, true);
		Arrays.fill(game2nulls, true);
		Arrays.fill(game1, true);
		Arrays.fill(game2, true);

		for (int i = 0; i < n; i++) {
			outList[i] = new ArrayList<>();
		}
		int bin = 1;
		for (int i = 0; i < m; i++) {
			v = sc.nextInt();
			w = sc.nextInt();
			color = sc.nextInt();
			prebuiltBinaries.add(bin);
			bin = bin * 2;
			outList[v - 1].add(i);
			edgeList.add(new edge(v - 1, w - 1, color));
			if (v == w)
				continue;
			outList[w - 1].add(i);
		}
		sc.close();

		for (int k = 0; k < m; k++) {
			edge j = edgeList.get(k);
			if (j.getV() == 0 || j.getW() == 0) {
				int mask = prebuiltBinaries.get(k);
				if (j.getColor() == 1) {
					game1[mask] = false;
					game2[mask] = true;
				} else {
					if (j.getColor() == 0) {
						game1[mask] = true;
						game2[mask] = false;
					} else {
						game1[mask] = false;
						game2[mask] = false;
					}
				}
				used[mask] = true;
			}
		}
		used[0] = true;
		game1[0] = true;
		game2[0] = true;
		solveRB(combNumb - 1);
		if (game1[combNumb - 1] && !game2[combNumb - 1])
			outStr.println("Left Left");
		if (!game1[combNumb - 1] && game2[combNumb - 1])
			outStr.println("Right Right");
		if (game1[combNumb - 1] && game2[combNumb - 1])
			outStr.println("Right Left");
		if (!game1[combNumb - 1] && !game2[combNumb - 1])
			outStr.println("Left Right");
		outStr.flush();
		outStr.close();
	}

	public void solveRB(int mask) {
		if (used[mask])
			return;
		edge j;
		for (int i = 0; i < m; i++) {
			j = edgeList.get(i);
			if ((mask & prebuiltBinaries.get(i)) == 0)
				continue;

			int tmp_mask = (mask ^ prebuiltBinaries.get(i));
			tmp_mask = fixEdges(tmp_mask);
			if (!used[tmp_mask])
				solveRB(tmp_mask);
			if (j.getColor() == 0) {
				if (game1[tmp_mask])
					game2[mask] = false;
			} else if (j.getColor() == 1) {
				if (game2[tmp_mask])
					game1[mask] = false;
			} else {
				if (game2[tmp_mask])
					game1[mask] = false;

				if (game1[tmp_mask])
					game2[mask] = false;
			}
		}

		used[mask] = true;
	}

	private int fixEdges(int mask) {
		if (oldmasksUsed[mask])
			return oldmasks[mask];
		int tmp = mask;
		boolean[] used = new boolean[n];
		bugaga = tmp;
		dfs(0, used);
		oldmasks[mask] = mask ^ bugaga;
		oldmasksUsed[mask] = true;
		return oldmasks[mask];
	}

	Integer bugaga; // TODO bad hack need to fix

	private void dfs(int i, boolean[] used) {
		if (used[i])
			return;
		used[i] = true;
		for (int x = 0; x < outList[i].size(); x++) {
			int j = outList[i].get(x);
			if ((bugaga & prebuiltBinaries.get(j)) == 0)
				continue;

			if (edgeList.get(j).getV() == i) {
				bugaga = bugaga ^ prebuiltBinaries.get(j);
				dfs(edgeList.get(j).getW(), used);
			} else if (edgeList.get(j).getW() == i) {
				bugaga = bugaga ^ prebuiltBinaries.get(j);
				dfs(edgeList.get(j).getV(), used);
			}
		}

	}

}