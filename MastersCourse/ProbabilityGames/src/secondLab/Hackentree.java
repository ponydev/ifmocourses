package secondLab;
import java.io.*;
import java.util.*;
import java.math.BigInteger;

public class Hackentree {
 
    private static Scanner in;
    private static PrintWriter out;
	public ArrayList<Edge>[] gr;
	private boolean[] used;
     
    public static void main(String[] args) throws FileNotFoundException {
    	Hackentree g = new Hackentree();
        in = new Scanner(new FileReader("hackentree.in"));
        out = new PrintWriter("hackentree.out");
         
        g.solve();
         
        in.close();
        out.close();
    }
     
    public class Edge {
		public int u;
		public int v;
		public int color;
		
		@Override
		public String toString() {
			return this.u + "-->" + this.v + "Color:" + color;
		}
	}
    
    public class Fraction {
		public BigInteger numerator;
		public BigInteger denominator;

		public Fraction plus(Fraction t) {
			Fraction temp = new Fraction();
			temp.numerator = (numerator.multiply(t.denominator)).add(denominator.multiply(t.numerator));
			temp.denominator = denominator.multiply(t.denominator);
			return temp;
		}
		
		public Fraction plus(int t) {
			return plus(BigInteger.valueOf(t));
		}

		public Fraction plus(BigInteger t) {
			Fraction temp = new Fraction();
			temp.numerator = numerator.add(denominator.multiply(t));
			temp.denominator = denominator;
			return temp;
		}
		@Override
		public String toString() {
			return this.numerator + "/" + this.denominator;
		}
		
	}
    
    
    public void solve() {
		int n = in.nextInt();
		gr = new ArrayList[n];
		used = new boolean[n];
		
		for (int i = 0; i < n; i++) {
			gr[i] = new ArrayList();
		}
		for (int i = 0; i < n - 1; i++) {
			int u = in.nextInt() - 1;
			int v = in.nextInt() - 1;
			int color = in.nextInt();
			Edge t = new Edge();
			t.u = u;
			t.v = v;
			t.color = color;
			System.out.println(t);
			System.out.println(Arrays.toString(gr));
			gr[u].add(t);
			//t = new Edge();
			t.u = v;
			t.v = u;
			System.out.println(t);
			System.out.println(Arrays.toString(gr));
			gr[v].add(t);
		}
		System.out.println(Arrays.toString(gr));
		used[0] = true;
		Fraction ans = new Fraction();
		ans.numerator = BigInteger.ZERO;
		ans.denominator = BigInteger.ONE;
		for (Edge i : gr[0]) {
			ans = ans.plus(getAns(i.v, i.color));
		}
		
		ans = reduce(ans);
		
		System.out.println(ans.numerator + " " + ans.denominator);
    }
    
    public Fraction reduce(Fraction a) {
    	System.out.println(a);
    	if (a.numerator.equals(BigInteger.ZERO)){
			Fraction t = new Fraction();
			t.numerator = BigInteger.ZERO;
			t.denominator = BigInteger.ONE;
			return t;
		}

		BigInteger gcd = a.numerator.gcd(a.denominator);
		
		if (gcd.compareTo(BigInteger.ZERO) < 0){
			gcd = gcd.negate();
		}
		
		Fraction t = new Fraction();
		t.numerator = a.numerator.divide(gcd);
		t.denominator = a.denominator.divide(gcd);
		return t;
	}
    
    public Fraction getAns(int v, int color) {
    	Fraction t = new Fraction();
		t.numerator = BigInteger.ZERO;
		t.denominator = BigInteger.ONE;
		System.out.println(v);
		System.out.println(Arrays.toString(used));
		if(used[v])
			return t;
		
		used[v] = true;			
		if(gr[v].isEmpty()) {
			if (color == 0) {
				System.out.println("cc");
				t.numerator = BigInteger.ONE;
				return t;
			}
			else {
				t.numerator = BigInteger.ONE.negate();
				return t;
			}
		}
		
		for (Edge i : gr[v]) {
			t = t.plus(getAns(i.v, i.color));
		}

		int c = 1;
    	
    	if (color == 0) {
			while (t.plus(c).numerator.compareTo(t.plus(c).denominator) < 0) {
				c++;
			}
		} 
		else {
			c = -c;
			while (((t.plus(c).numerator).compareTo(t.plus(c).denominator.negate()) > 0)) {
				c--;
			}
		}
    	Fraction k = new Fraction();
		k.numerator = t.plus(c).numerator;
		k.denominator = (t.plus(c).denominator).multiply((BigInteger.valueOf(2)).pow(Math.abs(c) - 1));
		return reduce(k);
	}
    
}

