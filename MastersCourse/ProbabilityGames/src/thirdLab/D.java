package thirdLab;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.Scanner;

public class D
{
    private static String  in  = "completely.in";
    private static String  out = "completely.out";
    private int            n   = 0;
    private BigDecimal[][] Game;

    public static void main (String[] args) throws FileNotFoundException
    {
        try
        {
            new D().solve();
        }
        catch (IOException e)
        {
            System.err.println("Smthing bad with IO");
        }

    }

    public static void swap (BigDecimal bd1, BigDecimal bd2)
    {
        BigDecimal tmp = bd1;
        bd1 = bd2;
        bd2 = tmp;
        tmp = null;
    }

    static void gaussInverse (BigDecimal[][] m)
    {
        for (int k = 0; k < m.length; k++)
        {
            if (m[k][k].compareTo(BigDecimal.ZERO) == 0)
            {
                int i = k + 1;
                while ((i < m.length))
                {
                    if (!((m[i][k].compareTo(BigDecimal.ZERO)) == 0))
                    {
                        for (int j = k; j < m.length * 2; j++)
                        {
                            swap(m[k][j], m[i][j]);
                        }
                        break;
                    }
                    i++;
                }
            }

            for (int i = k + 1; i < m.length * 2; i++)
            {
                m[k][i] = m[k][i].divide(m[k][k], 16, BigDecimal.ROUND_HALF_UP);
            }
            m[k][k] = BigDecimal.ONE;

            for (int i = k + 1; i < m.length; i++)
            {
                for (int j = k + 1; j < m.length * 2; j++)
                {
                    m[i][j] = m[i][j].subtract(m[k][j].multiply(m[i][k],
                            new MathContext(16)));
                }
                m[i][k] = BigDecimal.ZERO;
            }
        }

        for (int i = m.length - 2; i >= 0; i--)
        {
            int j = i + 1;
            while ((j < m.length))
            {
                if (!(m[i][j].compareTo(BigDecimal.ZERO) == 0))
                {
                    for (int t = m.length; t < 2 * m.length; t++)
                    {
                        m[i][t] = m[i][t].subtract(m[i][j].multiply(m[j][t],
                                new MathContext(16)));
                    }
                    m[i][j] = BigDecimal.ZERO;
                }
                j++;
            }

        }
    }

    public void solve () throws IOException
    {

        Scanner sc = new Scanner(new FileReader(in));
        PrintWriter outStr = new PrintWriter(new FileWriter(out));
        n = sc.nextInt();

        Game = new BigDecimal[n][2 * n];

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Game[i][j] = new BigDecimal(sc.nextInt() + 1001);
                if (i == j)
                    Game[i][j + n] = BigDecimal.ONE;
                else
                    Game[i][j + n] = BigDecimal.ZERO;
            }
        }
        System.out.println(Arrays.deepToString(Game));
        gaussInverse(Game);
        System.out.println(Arrays.deepToString(Game));

        BigDecimal sumAll = BigDecimal.ZERO;
        BigDecimal[] columns = new BigDecimal[n];
        BigDecimal[] lines = new BigDecimal[n];
        for (int i = 0; i < n; i++)
            columns[i] = BigDecimal.ZERO;

        for (int i = 0; i < n; i++)
        {
            BigDecimal tmp = BigDecimal.ZERO;
            for (int j = n; j < 2 * n; j++)
            {
                sumAll = sumAll.add(Game[i][j]);
                tmp = tmp.add(Game[i][j]);
                columns[j - n] = columns[j - n].add(Game[i][j]);
            }
            lines[i] = tmp.add(BigDecimal.ZERO);
        }

        for (int i = 0; i < n; i++)
        {
            outStr.print(columns[i]
                    .divide(sumAll, 10, BigDecimal.ROUND_HALF_UP).doubleValue()
                    + " ");
        }
        outStr.println();
        for (int i = 0; i < n; i++)
        {
            outStr.print(lines[i].divide(sumAll, 10, BigDecimal.ROUND_HALF_UP)
                    .doubleValue() + " ");
        }
        outStr.println();

        outStr.close();
        sc.close();
    }

}