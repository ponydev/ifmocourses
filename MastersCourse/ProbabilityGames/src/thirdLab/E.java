package thirdLab;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class E
{

    private static String in   = "nash.in";
    private static String out  = "nash.out";
    private int[][]       Game = new int[100][100];
    private int           m    = 0;
    private int           n    = 0;

    public static void main (String[] args) throws FileNotFoundException
    {
        try
        {
            new E().solve();
        }
        catch (IOException e)
        {
            System.err.println("Smthing bad with IO");
        }

    }

    public void solve () throws IOException
    {

        Scanner sc = new Scanner(new FileReader(in));
        PrintWriter outStr = new PrintWriter(new FileWriter(out));

        m = sc.nextInt();
        n = sc.nextInt();
        @SuppressWarnings("unchecked")
        ArrayList<Integer>[] maxLines = new ArrayList[m];
        @SuppressWarnings("unchecked")
        ArrayList<Integer>[] maxColumns = new ArrayList[n];
        ArrayList<Integer> nash = new ArrayList<>();
        int localMaxLineValue = Integer.MIN_VALUE;
        int localMaxColumnValue = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++)
        {
            maxColumns[i] = new ArrayList<>();
        }

        for (int i = 0; i < m; i++)
        {
            maxLines[i] = new ArrayList<>();
        }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Game[i][j] = sc.nextInt();
            }
        }

        for (int j = 0; j < n; j++)
        {
            localMaxColumnValue = Integer.MIN_VALUE;
            for (int i = 0; i < m; i++)
            {
                int temp = Game[i][j];
                if (temp == localMaxColumnValue)
                {
                    maxColumns[j].add(i + 1);
                }
                if (temp > localMaxColumnValue)
                {
                    localMaxColumnValue = temp;
                    maxColumns[j].clear();
                    maxColumns[j].add(i + 1);
                }

            }

        }

        for (int i = 0; i < m; i++)
        {
            localMaxLineValue = Integer.MIN_VALUE;
            for (int j = 0; j < n; j++)
            {
                int temp = sc.nextInt();
                if (temp == localMaxLineValue)
                {
                    maxLines[i].add(j + 1);
                }
                if (temp > localMaxLineValue)
                {
                    localMaxLineValue = temp;
                    maxLines[i].clear();
                    maxLines[i].add(j + 1);
                }

            }

        }

        for (int i = 0; i < n; i++)
        {
            for (int j : maxColumns[i])
            {
                if (maxLines[j - 1].contains(i + 1))
                {
                    nash.add(i + 1);
                    nash.add(j);
                }
            }
        }
        if (nash.size() != 0)
        {
            outStr.println(nash.size() / 2);
            for (int i = 0; i < nash.size(); i = i + 2)
            {
                outStr.print(nash.get(i + 1) + " " + nash.get(i));
                outStr.println();
            }
        }
        else
        {
            outStr.println(0);
        }

        sc.close();
        outStr.flush();
        outStr.close();
    }
}
