package thirdLab;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class G
{

    private static String in    = "pareto.in";
    private static String out   = "pareto.out";
    private int[][]       Game1 = new int[100][100];
    private int[][]       Game2 = new int[100][100];
    private int           m     = 0;
    private int           n     = 0;

    public static void main (String[] args) throws FileNotFoundException
    {
        try
        {
            new G().solve();
        }
        catch (IOException e)
        {
            System.err.println("Smthing bad with IO");
        }

    }

    public boolean checkElem (int elem1, int elem2)
    {
        for (int k = 0; k < m; k++)
        {
            for (int l = 0; l < n; l++)
            {

                if (((elem1) <= (Game1[k][l]) && (elem2) < (Game2[k][l]))
                        || ((elem1) < (Game1[k][l]) && (elem2) <= (Game2[k][l])))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void solve () throws IOException
    {

        Scanner sc = new Scanner(new FileReader(in));
        PrintWriter outStr = new PrintWriter(new FileWriter(out));

        m = sc.nextInt();
        n = sc.nextInt();
        ArrayList<Integer> pareto = new ArrayList<>();
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Game1[i][j] = sc.nextInt();
            }
        }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Game2[i][j] = sc.nextInt();
            }
        }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (checkElem(Game1[i][j], Game2[i][j]))
                {
                    pareto.add(i + 1);
                    pareto.add(j + 1);
                }
            }
        }

//        System.out.println(pareto);
        if (pareto.size() != 0)
        {
            outStr.println(pareto.size() / 2);
            for (int i = 0; i < pareto.size(); i = i + 2)
            {
                outStr.print(pareto.get(i) + " " + pareto.get(i + 1));
                outStr.println();
            }
        }
        else
        {
            outStr.println(0);
        }

        sc.close();
        outStr.flush();
        outStr.close();
    }
}
