package thirdLab;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class A
{

    private static String in   = "pure.in";
    private static String out  = "pure.out";
    private int[][]       Game = new int[100][100];
    private int           m    = 0;
    private int           n    = 0;

    public static void main (String[] args) throws FileNotFoundException
    {
        try
        {
            new A().solve();
        }
        catch (IOException e)
        {
            System.err.println("Smthing bad with IO");
        }

    }

    public void solve () throws IOException
    {

        Scanner sc = new Scanner(new FileReader(in));
        PrintWriter outStr = new PrintWriter(new FileWriter(out));

        m = sc.nextInt();
        n = sc.nextInt();
        ArrayList<Integer> maxLines = new ArrayList<>();
        ArrayList<Integer> minColumns = new ArrayList<>();
        int maxLine = Integer.MIN_VALUE;
        int minColumn = Integer.MAX_VALUE;
        int localMinLineValue = Integer.MAX_VALUE;
        int localMaxColumnValue = Integer.MIN_VALUE;

        for (int i = 0; i < m; i++)
        {
            localMinLineValue = Integer.MAX_VALUE;
            for (int j = 0; j < n; j++)
            {
                int temp = sc.nextInt();
                Game[i][j] = temp;
                if (temp < localMinLineValue)
                {
                    localMinLineValue = temp;
                }
            }
            if (localMinLineValue == maxLine)
            {
                maxLines.add(i + 1);
            }
            if (localMinLineValue > maxLine)
            {
                maxLine = localMinLineValue;
                maxLines.clear();
                maxLines.add(i + 1);
            }

        }

        for (int j = 0; j < n; j++)
        {
            localMaxColumnValue = Integer.MIN_VALUE;
            for (int i = 0; i < m; i++)
            {
                int temp = Game[i][j];
                if (temp > localMaxColumnValue)
                {
                    localMaxColumnValue = temp;
                }
            }
            if (localMaxColumnValue == minColumn)
            {
                minColumns.add(j + 1);
            }
            if (localMaxColumnValue < minColumn)
            {
                minColumn = localMaxColumnValue;
                minColumns.clear();
                minColumns.add(j + 1);
            }
        }
        if (minColumn != maxLine)
        {
            outStr.println("0 0");
        }
        else
        {
            outStr.println(maxLines.size() + " " + minColumns.size());
            int i;
            for (i = 0; i < maxLines.size()-1; i++)
            {
                outStr.print(maxLines.get(i) + " ");
            }
            outStr.print(maxLines.get(i));
            outStr.println();
            for (i = 0; i < minColumns.size()-1; i++)
            {
                outStr.print(minColumns.get(i) + " ");
            }
            outStr.print(minColumns.get(i));
        }

        sc.close();
        outStr.flush();
        outStr.close();
    }
}
