package thirdLab;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class C
{

    private static String in   = "nx2.in";
    private static String out  = "nx2.out";
    private int[][]       Game = new int[2][100];
    private int           m    = 0;
    private int           n    = 0;

    public static void main (String[] args) throws FileNotFoundException
    {
        try
        {
            new C().solve();
        }
        catch (IOException e)
        {
            System.err.println("Smthing bad with IO");
        }

    }

    class function
    {
        private int k;
        private int b;

        public function(int k, int b) {
            this.b = b;
            this.k = k;
        }

        public function sum (function a)
        {
            return new function(this.getK() + a.getK(), this.getB() + a.getB());
        }

        public function multiply (int a)
        {
            return new function(this.getK() * a, this.getB() * a);
        }

        public int getK ()
        {
            return k;
        }

        public int getB ()
        {
            return b;
        }

        @Override
        public String toString ()
        {
            return this.getK() + "*x +" + this.getB();
        }

    }

    public function[] mult (function[] vector, int[][] matrix)
    {
        function[] temp = new function[n];
        for (int i = 0; i < n; i++)
        {
            temp[i] = (vector[0].multiply(matrix[0][i]).sum(vector[1]
                    .multiply(matrix[1][i])));
        }

        return temp;
    }

    // public function[] mult (function[][] matrix, int[] vector)
    // {
    // return vector;
    // }

    public void solve () throws IOException
    {

        Scanner sc = new Scanner(new FileReader(in));
        PrintWriter outStr = new PrintWriter(new FileWriter(out));

        n = sc.nextInt();

        function[] s = new function[n];

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Game[i][j] = sc.nextInt();
            }
        }
        sc.close();
        function[] temp = { new function(1, 0), new function(-1, 1) };
        s = mult(temp, Game);
        System.out.println(Arrays.toString(s));

        outStr.flush();
        outStr.close();
    }
}
