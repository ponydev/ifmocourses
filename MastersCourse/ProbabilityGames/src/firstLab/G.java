package firstLab;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class G {

	private ArrayList<Integer>[] gr;
	private static String in = "greenhackenbush.in";
	private static String out = "greenhackenbush.out";

	public static void main(String[] args) throws FileNotFoundException {
		try {
			new G().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	@SuppressWarnings("unchecked")
	public void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		int n = sc.nextInt();
		int m = sc.nextInt();

		gr = new ArrayList[n];
		for (int i = 0; i < n; i++) {
			gr[i] = new ArrayList<Integer>();
		}

		for (int i = 0; i < m; ++i) {
			int x = sc.nextInt();
			int y = sc.nextInt();
			gr[x - 1].add(y - 1);
			if (x != y) {
				gr[y - 1].add(x - 1);
			}
		}

		int[] used = new int[n];
		Set<Integer> join = new HashSet<Integer>();

		int ans = dfs(0, -1, used, join);

		if (ans == 0) {
			outStr.println("Second");
		} else {
			outStr.println("First");

		}
		sc.close();
		outStr.flush();
		outStr.close();
	}

	int dfs(int v, int par, int[] used, Set<Integer> join) {
		used[v] = 1;

		int ans = 0;
		boolean edge = false;

		for (int u : gr[v]) {
			if (u == par && !edge) {
				edge = true;
				continue;
			}

			if (used[u] == 0) {
				Set<Integer> join_new = new HashSet<Integer>();
				int ans_new = dfs(u, v, used, join_new);
				if (join_new.isEmpty()) {
					ans ^= (1 + ans_new);
				} else {
					join.addAll(join_new);
					ans ^= ans_new;
					ans ^= 1;
				}
			} else if (used[u] == 1) {
				join.add(u);
				ans ^= 1;
			}
		}

		used[v] = 2;

		join.remove(v);

		return ans;
	}

}
