package firstLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class F {

	private ArrayList<ArrayList<Integer>> gr;
	private int[] grundy;
	private int[] to;
	private int[] s;
	private static String in = "snakesarrows.in";
	private static String out = "snakesarrows.out";

	public static void main(String[] args) {
		try {
			PrintWriter outStr = new PrintWriter(new FileWriter(out));
			new F().solve(outStr);
			outStr.flush();
			outStr.close();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	public int getGrundy(int i) {
		if (grundy[i] != -1) {
			return grundy[i];
		}
		if (gr.get(i).size() == 0) {
			return 0;
		}
		int[] a = new int[gr.get(i).size()];
		int k = 0;
		for (int j = 0; j < gr.get(i).size(); j++) {
			a[k] = grundy[gr.get(i).get(j)];
			k++;
		}
		boolean[] z = new boolean[a.length + 1];
		for (int j : a) {
			if (j > -1 && j < z.length) {
				z[j] = true;
			}
		}
		for (int j = 0; j < z.length; j++) {
			if (!z[j]) {
				return j;
			}
		}
		return -1;
	}

	public void solve(PrintWriter outStr) throws IOException {
		Scanner sc = new Scanner(new FileReader(in));

		int n = sc.nextInt();
		int m = sc.nextInt();
		int k = sc.nextInt();
		int q = sc.nextInt();

		gr = new ArrayList<>();
		grundy = new int[n];
		to = new int[n];
		s = new int[k];
		Arrays.fill(grundy, -1);
		Arrays.fill(to, -1);

		for (int i = 0; i < n; ++i) {
			gr.add(new ArrayList<Integer>());
		}

		for (int i = 0; i < k; i++) {
			s[i] = sc.nextInt() - 1;
		}

		for (int i = 0; i < m; i++) {
			int x = sc.nextInt() - 1;
			int y = sc.nextInt() - 1;
			to[x] = y;
		}

		sc.close();

		for (int i = 0; i < n - 1; i++) {
			for (int j = 1; j <= q; j++) {
				int t = Math.min(i + j, n - 1);
				if (to[t] == -1) {
					gr.get(i).add(t);
				} else {
					gr.get(i).add(to[t]);
				}
			}
		}

		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 0; i < n; i++) {
				if (grundy[i] == -1) {
					int mex = getGrundy(i);
					boolean f = true;
					for (int v : gr.get(i)) {
						boolean flag = false;
						for (int j : gr.get(v)) {
							if (grundy[j] == mex) {
								flag = true;
							}
						}
						if (grundy[v] == -1 && !flag) {
							f = false;
							break;
						}
					}
					if (f) {
						if (grundy[i] != mex) {
							changed = true;
							grundy[i] = mex;
							break;
						}
					}
				}
			}

		}

		int sum = 0;
		int ch = 0;
		int pos = 0;
		for (int i = 0; i < s.length; i++) {
			if (grundy[s[i]] >= 0) {
				sum ^= grundy[s[i]];
			} else {
				if (ch == 0) {
					pos = i;
				}
				ch++;
			}
		}

		if (ch > 1) {
			outStr.print("Draw");
		} else if (ch == 1) {
			for (int v : gr.get(s[pos])) {
				if (grundy[v] == sum) {
					outStr.println("Arkadii");
					int i = 1;
					for (; i <= q; ++i) {
						if (v - s[pos] > q) {
							if (to[s[pos] + i] == v) {
								break;
							}
						} else {
							if (s[pos] + i == v) {
								break;
							}
						}
					}
					outStr.println((pos + 1) + " " + i);
					return;
				}
			}
			outStr.print("Draw");
		} else if (ch == 0) {
			if (sum == 0) {
				outStr.print("Boris");
			} else {
				for (int i = 0; i < s.length; ++i) {
					int ans = sum ^ grundy[s[i]];
					for (int v : gr.get(s[i])) {
						if (grundy[v] == ans) {
							outStr.println("Arkadii");
							int j = 1;
							for (; j <= q; ++j) {
								if (v - s[i] > q) {
									if (to[s[i] + j] == v) {
										break;
									}
								} else {
									if (s[i] + j == v) {
										break;
									}
								}
							}
							outStr.println((i + 1) + " " + j);
							return;
						}
					}
				}
			}
		}

	}

}