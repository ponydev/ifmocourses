package firstLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class C {

	private int n;
	private int m;
	private ArrayList<ArrayList<Integer>> outEdges = new ArrayList<ArrayList<Integer>>();
	private int[] ans;
	private static String in = "grundy.in";
	private static String out = "grundy.out";

	public static void main(String[] args) {
		try {
			new C().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	private void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		n = sc.nextInt();
		m = sc.nextInt();

		ans = new int[n];
		Arrays.fill(ans, -1);
		for (int i = 0; i < n; i++) {
			outEdges.add(new ArrayList<Integer>());
		}

		for (int i = 0; i < m; i++) {
			outEdges.get(sc.nextInt() - 1).add(sc.nextInt() - 1);
		}
		sc.close();

		for (int i = 0; i < ans.length; i++) {
			outStr.println(getans(i));
		}
		outStr.flush();
		outStr.close();

	}

	private int getans(int x) {
		if (ans[x] == -1) {
			if (outEdges.get(x).size() == 0) {
				ans[x] = 0;
			} else {
				boolean[] freeInt = new boolean[outEdges.get(x).size() + 1];
				for (int y : outEdges.get(x)) {
					int temp = getans(y);
					if (temp < freeInt.length) {
						freeInt[temp] = true;
					}
				}
				int i = 0;
				while (freeInt[i]) {
					if (i >= freeInt.length) {
						break;
					}
					i++;
				}
				ans[x] = i;
			}
		}
		return ans[x];
	}

}