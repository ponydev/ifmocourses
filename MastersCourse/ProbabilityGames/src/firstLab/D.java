package firstLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class D {
	private int a, b, c;
	private static String in = "karlsson.in";
	private static String out = "karlsson.out";

	public static void main(String[] args) {
		try {
			new D().solve();

		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}

	}

	public void printAnswer(int[] mex, PrintWriter outStr) {
		if ((mex[a] ^ mex[b] ^ mex[c]) == 0) {
			outStr.println("NO");
		} else {
			outStr.println("YES");

			for (int i = 1; i <= a / 2; i++) {
				if ((mex[a - i] ^ mex[b] ^ mex[c]) == 0) {
					outStr.println((a - i) + " " + b + " " + c);
					return;
				}
			}

			for (int i = 1; i <= b / 2; i++) {
				if ((mex[a] ^ mex[b - i] ^ mex[c]) == 0) {
					outStr.println(a + " " + (b - i) + " " + c);
					return;
				}
			}

			for (int i = 1; i <= c / 2; i++) {
				if ((mex[a] ^ mex[b] ^ mex[c - i]) == 0) {
					outStr.println(a + " " + b + " " + (c - i));
					return;
				}
			}

		}
	}

	public void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();

		sc.close();
		int max = Math.max(a, Math.max(b, c));

		int[] mex = new int[max + 1];

		for (int i = 2; i <= max; i++) {
			boolean usedInt[] = new boolean[max + 1];

			for (int j = 1; j <= i / 2; j++) {
				usedInt[mex[i - j]] = true;
			}

			while (usedInt[mex[i]])
				mex[i]++;

		}

		printAnswer(mex, outStr);

		outStr.flush();
		outStr.close();

	}

}