package firstLab;

import java.io.*;
import java.util.*;

public class E {

	private ArrayList<ArrayList<Integer>> gr;
	private int[] grundy;
	private static String in = "smith.in";
	private static String out = "smith.out";

	public static void main(String[] args) {
		try {
			new E().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	public int getGrundy(int i) {
		if (grundy[i] != -1) {
			return grundy[i];
		}
		if (gr.get(i).size() == 0) {
			return 0;
		}
		int[] a = new int[gr.get(i).size()];
		for (int j = 0; j < a.length; j++) {
			a[j] = grundy[gr.get(i).get(j)];
		}
		boolean[] z = new boolean[a.length + 1];
		for (int j : a) {
			if (j > -1 && j < z.length) {
				z[j] = true;
			}
		}
		for (int j = 0; j < z.length; j++) {
			if (!z[j]) {
				return j;
			}
		}
		return -1;
	}

	public void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		int n = sc.nextInt();
		int m = sc.nextInt();

		gr = new ArrayList<>();
		grundy = new int[n];
		Arrays.fill(grundy, -1);

		for (int i = 0; i < n; ++i) {
			gr.add(new ArrayList<Integer>());
		}

		for (int i = 0; i < m; i++) {
			int x = sc.nextInt() - 1;
			int y = sc.nextInt() - 1;
			gr.get(x).add(y);
		}

		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 0; i < n; i++) {
				if (grundy[i] == -1) {
					int mex = getGrundy(i);
					boolean f = true;
					for (int v : gr.get(i)) {
						boolean flag = false;
						for (int j : gr.get(v)) {
							if (grundy[j] == mex) {
								flag = true;
							}
						}
						if (grundy[v] == -1 && !flag) {
							f = false;
							break;
						}
					}
					if (f) {
						if (grundy[i] != mex) {
							changed = true;
							grundy[i] = mex;
							break;
						}
					}
				}
			}

		}

		for (int i = 0; i < n; i++) {
			outStr.print(grundy[i] + " ");
			if (grundy[i] == -1) {
				int[] a = new int[gr.get(i).size()];
				ArrayList<Integer> l = new ArrayList<>();
				for (int j = 0; j < a.length; j++) {
					a[j] = grundy[gr.get(i).get(j)];
				}
				Arrays.sort(a);
				int begin = 0;
				while (begin != a.length) {
					if (a[begin] != -1) {
						break;
					}
					begin++;
				}
				for (int j = begin; j < a.length; j++) {
					if (l.isEmpty() || a[j] != a[j - 1]) {
						l.add(a[j]);
					}
				}
				outStr.print(l.size() + " ");
				for (int j : l) {
					outStr.print(j + " ");
				}
			}
			outStr.println();
		}
		sc.close();
		outStr.close();
	}

}