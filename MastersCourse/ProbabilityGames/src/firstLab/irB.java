
package firstLab;
 
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
 
/**
 * @author Irene Petrova
 */
public class irB {
    public static int[][][][] graph;
    public static int[][] map;
    public static boolean[][][][] visited;
    public static int[][][][] win;
    public static int n;
 
	private static int zz1 = 0;
	private static int zz2 = 0;
    
    private static void dfs(int xw, int yw, int xb, int yb) {
        visited[xw][yw][xb][yb] = true;
        int minx = Math.max(0, xb - 1);
        int maxy = Math.min(yb + 1, n - 1);
        int miny = Math.max(0,yb - 1);
        int maxx = Math.min(n - 1, xb + 1);
        for (int xc = minx; xc <= maxx; ++xc) {
            for (int yc = miny; yc <= maxy; ++yc) {
                if (win[xw][yw][xb][yb] == -1) {
                    if (!visited[xc][yc][xw][yw] && !(xc == xb && yc == yb)) {
                    	zz1++;
                        win[xc][yc][xw][yw] = 1;
                        visited[xc][yc][xw][yw] = true;
                        dfs(xc,yc,xw,yw);
                    }
                } else if (!visited[xc][yc][xw][yw] && !(xc == xb && yc == yb)) {
                    if (--graph[xc][yc][xw][yw] == 0) {
                    	zz2++;
                        win[xc][yc][xw][yw] = -1;
                        visited[xc][yc][xw][yw] = true;
                        dfs(xc,yc,xw,yw);
                    }
                }
 
            }
        }
    }
 
    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("gameofthrones.in")));
            StringTokenizer st = new StringTokenizer(br.readLine());
            n = Integer.valueOf(st.nextToken());
            int m = Integer.valueOf(st.nextToken());
            st = new StringTokenizer(br.readLine());
            int rw = Integer.valueOf(st.nextToken());
            int cw = Integer.valueOf(st.nextToken());
            int rb = Integer.valueOf(st.nextToken());
            int cb = Integer.valueOf(st.nextToken());
            --rw;
            --cw;
            --rb;
            --cb;
            graph = new int[n][n][n][n];
            visited = new boolean[n][n][n][n];
            win = new int[n][n][n][n];
            map = new int[n][n];
            for (int i = 1; i < n - 1; ++i) {
                map[i][0] = 5;
                map[0][i] = 5;
                map[i][n - 1] = 5;
                map[n - 1][i] = 5;
            }
 
            map[0][0] = 3;
            map[n - 1][0] = 3;
            map[0][n - 1] = 3;
            map[n - 1][n - 1] = 3;
            for (int i = 1; i < n - 1; ++i) {
                for (int j = 1; j < n - 1; ++j) {
                    map[i][j] = 8;
                }
            }
            for (int i = 0; i < m; ++i) {
                st = new StringTokenizer(br.readLine());
                int v1 = Integer.valueOf(st.nextToken());
                int v2 = Integer.valueOf(st.nextToken());
                v1--;
                v2--;
                map[v1][v2] = 0;
                int minx = Math.max(0, v1 - 1);
                int maxy = Math.min(v2 + 1, n - 1);
                int miny = Math.max(0,v2 - 1);
                int maxx = Math.min(n - 1, v1 + 1);
                for (int xc = minx; xc <= maxx; ++xc) {
                    for (int yc = miny; yc <= maxy; ++yc) {
                        if (map[xc][yc] != 0) {
                            --map[xc][yc];
                        }
                    }
                }
            }
            List<List<Integer>> vDfs = new ArrayList<>();
            for (int xw = 0; xw < n; ++xw) {
                for (int yw = 0; yw < n; ++yw) {
                    for (int xb = 0; xb < n; ++xb) {
                        for (int yb = 0; yb < n; ++yb) {
                            if ((xb == xw && yw == yb && map[xw][yw] != 0) || map[xw][yw] == 0) {
                                win[xw][yw][xb][yb] = -1;
                                if (map[xw][yw] != 0) {
                                    List<Integer> temp = new ArrayList<Integer>();
                                    temp.add(xw);
                                    temp.add(yw);
                                    temp.add(xb);
                                    temp.add(yb);
                                    vDfs.add(temp);
                                }
                                visited[xw][yw][xb][yb] = true;
 
                                //dfs(xw,yw,xb,yb);
                            } else if (map[xw][yw] != 0) {
                                graph[xw][yw][xb][yb] = map[xw][yw];
                                win[xw][yw][xb][yb] = 0;
                            }
                        }
                    }
                }
            }
            for (List<Integer> l: vDfs) {
                dfs(l.get(0), l.get(1), l.get(2), l.get(3));
            }
            PrintWriter pw = new PrintWriter(new File("gameofthrones.out"));
            if (win[rw][cw][rb][cb] == -1) {
                pw.println("Black");
            } else if (win[rw][cw][rb][cb] == 0) {
                pw.println("Draw");
            } else {
                pw.println("White");
            }
            System.out.println(zz1 + "/" + zz2);
            pw.flush();
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}