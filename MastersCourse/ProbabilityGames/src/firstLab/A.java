package firstLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class A {

	private int n;
	private int m;
	private ArrayList<ArrayList<Integer>> inEdges = new ArrayList<ArrayList<Integer>>();
	private static String in = "graphgame.in";
	private static String out = "graphgame.out";

	public static void main(String[] args) {
		try {
			new A().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}
	}

	private void solve() throws IOException {
		Queue<Integer> q = new LinkedList<Integer>();
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		n = sc.nextInt();
		m = sc.nextInt();

		int[] z = new int[n];
		int[] ans = new int[n];
		for (int i = 0; i < n; i++) {
			inEdges.add(new ArrayList<Integer>());
		}

		for (int i = 0; i < m; i++) {
			int x = sc.nextInt();
			int y = sc.nextInt();
			inEdges.get(y - 1).add(x - 1);
			z[x - 1]++;
		}
		sc.close();
		// testPrint();

		for (int i = 0; i < n; i++) {
			if (z[i] == 0) {
				q.add(i);
				ans[i] = -1;
			}
		}
		while (!q.isEmpty()) {
			Integer u = q.poll();
			if (ans[u] == 1) {
				for (int v : inEdges.get(u)) {
					z[v]--;
					if (ans[v] == 0 && z[v] == 0) {
						ans[v] = -1;
						q.add(v);
					}
				}
			}
			if (ans[u] == -1) {
				for (int v : inEdges.get(u)) {
					if (ans[v] == 0) {
						ans[v] = 1;
						q.add(v);
					}
				}
			}
		}

		for (int i = 0; i < ans.length; i++) {
			switch (ans[i]) {
			case -1:
				outStr.println("Loss");
				break;
			case 1:
				outStr.println("Win");
				break;
			default:
				outStr.println("Draw");
			}
		}
		outStr.flush();
		outStr.close();
	}

//	private void testPrint() {
//		Iterator<ArrayList<Integer>> it = inEdges.iterator();
//		while (it.hasNext()) {
//			System.out.println(Arrays.toString(it.next().toArray()));
//		}
//	}

}
