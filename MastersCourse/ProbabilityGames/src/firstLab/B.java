package firstLab;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class B {
	private int[][][][] gr;
	private boolean[][][][] used;
	private int[][][][] win;
	private int[][] field;
	private static String in = "gameofthrones.in";
	private static String out = "gameofthrones.out";
	ArrayList<ArrayList<Integer>> l = new ArrayList<>();

	private int zz1 = 0;
	private int zz2 = 0;

	public static void main(String[] args) throws FileNotFoundException {
		try {
			new B().solve();
		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}

	}

	public void solve() throws IOException {
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		int n = sc.nextInt();
		int m = sc.nextInt();
		int rw = sc.nextInt() - 1;
		int cw = sc.nextInt() - 1;
		int rb = sc.nextInt() - 1;
		int cb = sc.nextInt() - 1;

		gr = new int[n][n][n][n];
		used = new boolean[n][n][n][n];
		win = new int[n][n][n][n];
		field = new int[n][n];

		field[0][0] = 3;
		field[n - 1][0] = 3;
		field[0][n - 1] = 3;
		field[n - 1][n - 1] = 3;
		for (int i = 1; i < n - 1; i++) {
			field[i][0] = 5;
			field[0][i] = 5;
			field[i][n - 1] = 5;
			field[n - 1][i] = 5;
		}
		for (int i = 1; i < n - 1; i++) {
			for (int j = 1; j < n - 1; j++) {
				field[i][j] = 8;
			}
		}
		for (int i = 0; i < m; ++i) {
			int u = sc.nextInt() - 1;
			int v = sc.nextInt() - 1;
			field[u][v] = 0;
			for (int j = Math.max(0, u - 1); j <= Math.min(n - 1, u + 1); j++) {
				for (int k = Math.max(0, v - 1); k <= Math.min(v + 1, n - 1); k++) {
					if (field[j][k] > 0) {
						field[j][k]--;
					}
				}
			}
		}

		ArrayList<ArrayList<Integer>> l = new ArrayList<>();
		for (int xw = 0; xw < n; xw++) {
			for (int yw = 0; yw < n; yw++) {
				for (int xb = 0; xb < n; xb++) {
					for (int yb = 0; yb < n; yb++) {
						if ((xb == xw && yw == yb && field[xw][yw] != 0)
								|| field[xw][yw] == 0) {
							win[xw][yw][xb][yb] = -1;
							if (field[xw][yw] != 0) {
								l.add(new ArrayList<Integer>());
								l.get(l.size() - 1).add(xw);
								l.get(l.size() - 1).add(yw);
								l.get(l.size() - 1).add(xb);
								l.get(l.size() - 1).add(yb);
							}
							used[xw][yw][xb][yb] = true;

						} else if (field[xw][yw] != 0) {
							gr[xw][yw][xb][yb] = field[xw][yw];
							win[xw][yw][xb][yb] = 0;
						}
					}
				}
			}
		}
		for (int i = 0; i < l.size(); i++) {
			dfs(l.get(i).get(0), l.get(i).get(1), l.get(i).get(2), l.get(i)
					.get(3), n);
		}

		if (win[rw][cw][rb][cb] == -1) {
			outStr.println("Black");
		} else if (win[rw][cw][rb][cb] == 0) {
			outStr.println("Draw");
		} else {
			outStr.println("White");
		}
		// System.out.println(zz1 + "/" + zz2);
		sc.close();
		outStr.close();
	}

	private void dfs(int xw, int yw, int xb, int yb, int n) {
		used[xw][yw][xb][yb] = true;
		for (int i = Math.max(0, xb - 1); i <= Math.min(n - 1, xb + 1); i++) {
			for (int j = Math.max(0, yb - 1); j <= Math.min(yb + 1, n - 1); j++) {
				if (win[xw][yw][xb][yb] == -1 && !used[i][j][xw][yw]
						&& !(i == xb && j == yb)) {
					// zz1++;
					win[i][j][xw][yw] = 1;
					used[i][j][xw][yw] = true;
					dfs(i, j, xw, yw, n);
				} else if (!used[i][j][xw][yw] && !(i == xb && j == yb)) {
					gr[i][j][xw][yw]--;
					if (gr[i][j][xw][yw] == 0) {
						// zz2++;
						// gr[i][j][xw][yw]--;
						win[i][j][xw][yw] = -1;
						used[i][j][xw][yw] = true;
						dfs(i, j, xw, yw, n);
					}
				}

			}
		}
	}
}
