package firstLab;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class H {
	private int n;
	private static String in = "sqrtnim.in";
	private static String out = "sqrtnim.out";

	public static void main(String[] args) {
		try {
			new H().solve();

		} catch (IOException e) {
			System.err.println("Smthing bad with IO");
		}

	}

	public void solve() throws IOException {
		long ans = 0;
		long numb;
		Scanner sc = new Scanner(new FileReader(in));
		PrintWriter outStr = new PrintWriter(new FileWriter(out));

		n = sc.nextInt();

		for (int i = 0; i < n; i++) {
			numb = sc.nextLong();

			long sqrt = (long) Math.floor(Math.sqrt(numb));
			while (sqrt * sqrt != numb) {
				if (sqrt * sqrt > numb) {
					sqrt--;
				} else {
					numb -= sqrt + 1;
				}
			}
			ans = ans ^ sqrt;
		}

		if (ans != 0) {
			outStr.println("First");
		} else {
			outStr.println("Second");
		}

		sc.close();
		outStr.flush();
		outStr.close();
	}

}