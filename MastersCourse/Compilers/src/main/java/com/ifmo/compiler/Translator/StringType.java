package com.ifmo.compiler.Translator;

import org.objectweb.asm.Type;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 17.06.14
 * To change this template use File | Settings | File Templates.
 */

public class StringType implements DataType {
    private final int size;

    public StringType(int size) {
        this.size = size;
    }


    @Override
    public Type getType() {
        return Type.getType(String.class);
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isPrimitive() {
        return false;
    }

    @Override
    public boolean isInteger() {
        return false;
    }

    @Override
    public boolean isBoolean() {
        return false;
    }

    @Override
    public boolean isFunction() {
        return false;
    }


    @Override
    public String toString() {
        return "STRING";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return ((StringType) o).getSize() == size;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + 113 * (result - 13);
        return result;
    }
}

