package com.ifmo.compiler.Scope;

import com.ifmo.compiler.PythonParser;
import com.ifmo.compiler.Translator.DataType;
import org.objectweb.asm.Label;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 09.06.14
 * To change this template use File | Settings | File Templates.
 */


public class TranslateScope implements Scope {
    private String className;
    private String methodName;
    private DataType methodType;
    private byte[] byteCode;
    private Map<String, DataType> global = new HashMap<>();
    private Map<String, Integer> functions = new HashMap<>();
    private Map<String, Integer> functionsGenerated = new HashMap<>();
    private Map<String, PythonParser.SuiteContext> functionsSuite = new HashMap<>();
    private Map<String, String[]> functionsArgumentsNames = new HashMap<>();
    private Map<String, LocalVariableDescriptor> local = new HashMap<>();
    private Stack<LoopDescriptor> loop = new Stack<>();

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public byte[] getByteCode() {
        return byteCode;
    }

    public void setByteCode(byte[] byteCode) {
        this.byteCode = byteCode;
    }

    public void addGlobalVariable(String name, DataType type) {
        global.put(name, type);
    }

    public Map<String, DataType> getGlobalVariables() {
        return Collections.unmodifiableMap(global);
    }

    public boolean isGlobalVariable(String name) {
        return global.containsKey(name);
    }

    public DataType getGlobalVariableType(String name) {
        return global.get(name);
    }

    public void refreshLocalVariables() {
        local.clear();
    }

    public boolean isLocalVariable(String name) {
        return local.containsKey(name);
    }

    public int getLocalVariableIndex(String name) {
        return local.get(name).getIndex();
    }

    public DataType getLocalVariableType(String name) {
        return local.get(name).getType();
    }

    public int addLocalVariable(String name, DataType type) {
        local.put(name, new LocalVariableDescriptor(local.size(), type));
        return local.size() - 1;
    }

    public int changeLocalVariableType(String name, DataType type) {
        LocalVariableDescriptor temp = local.get(name);
        temp.changeTypeTo(type);
        local.put(name, temp);
        return temp.getIndex();
    }


    public boolean isFunctionDeclared(String name) {
        return functions.containsKey(name);
    }

    public void addFunctionSuite(String name, Integer numberOfArguments, PythonParser.SuiteContext suite) {
        functionsSuite.put(name + numberOfArguments, suite);
    }

    public PythonParser.SuiteContext getFunctionSuite(String name, Integer numberOfArguments) {
        return functionsSuite.get(name + numberOfArguments);
    }

    public void addFunctionArgumentsNames(String name, Integer numberOfArguments, String[] argumentsNames) {
        functionsArgumentsNames.put(name + numberOfArguments, argumentsNames);
    }

    public String[] getFunctionArgumentsNames(String name, Integer numberOfArguments) {
        return functionsArgumentsNames.get(name + numberOfArguments);
    }


    public int functionArgumentCount(String name) {
        return functions.get(name);
    }

    public void addFunctionGenerated(String name, Integer argumentCount) {
        functionsGenerated.put(name, argumentCount);
    }

    public boolean isFunctionGenerated(String name, Integer argumentCount) {
        if (!functionsGenerated.containsKey(name)) return false;
        return (functionsGenerated.get(name)).equals(argumentCount);
    }

//    public DataType getFunctionReturnType(String name, Integer argumentCount) {
//        return functions.get(new FunctionSignature(name, argumentCount));
//    }

    public void declareFunction(String name, Integer argumentCount) {
        functions.put(name, argumentCount);
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    public DataType getMethodType() {
        return methodType;
    }

    public void setMethodType(DataType methodType) {
        this.methodType = methodType;
    }

    public boolean inLoop() {
        return !loop.isEmpty();
    }

    public Label getContinueLabel() {
        return loop.peek().getContinueLabel();
    }

    public Label getBreakLabel() {
        return loop.peek().getBreakLabel();
    }

    public void enterLoop(Label continueLabel, Label breakLabel) {
        loop.push(new LoopDescriptor(continueLabel, breakLabel));
    }

    public void exitLoop() {
        loop.pop();
    }
}