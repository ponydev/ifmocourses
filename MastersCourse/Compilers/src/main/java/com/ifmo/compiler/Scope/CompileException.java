package com.ifmo.compiler.Scope;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 09.06.14
 * To change this template use File | Settings | File Templates.
 */
public class CompileException extends IllegalStateException {
    public CompileException(String s) {
        super(s);
    }

    public CompileException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public CompileException(Throwable throwable) {
        super(throwable);
    }
}