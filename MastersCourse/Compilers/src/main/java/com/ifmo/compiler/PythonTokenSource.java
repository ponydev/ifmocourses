package com.ifmo.compiler;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PythonTokenSource implements TokenSource {
    private static final Logger logger = LoggerFactory.getLogger(PythonTokenSource.class);
    public static final int MAX_INDENTS = 100;
    public static final int FIRST_CHAR_POSITION = 0;
    List tokens = new ArrayList();
    int[] indentStack = new int[MAX_INDENTS];
    CommonTokenStream stream;
    int sp = -1; // grow upwards
    int lastTokenAddedIndex = -1;
    boolean debugMode = false;

    public PythonTokenSource(PythonLexer lexer) {
    }

    public PythonTokenSource(CommonTokenStream stream, boolean debugMode) {
        this.stream = stream;
        // "state" of indent level is FIRST_CHAR_POSITION
        push(FIRST_CHAR_POSITION);
    }


    @Override
    public Token nextToken() {
        // if something in queue, just remove and return it
//        Token t = null;

        if (tokens.size() > 0) {
            Token t = (Token) tokens.get(0);
            tokens.remove(0);

            return t;

        }

        insertImaginaryIndentDedentTokens();

        return nextToken();
    }

    @Override
    public int getLine() {
        return 0;
    }

    @Override
    public int getCharPositionInLine() {
        return 0;
    }

    @Override
    public CharStream getInputStream() {
        return null;
    }

    @Override
    public String getSourceName() {
        return null;
    }

    @Override
    public void setTokenFactory(@NotNull TokenFactory<?> tokenFactory) {

    }

    @Override
    public TokenFactory<?> getTokenFactory() {
        return null;
    }

    protected void insertImaginaryIndentDedentTokens() {
        Token t = stream.LT(1);
        if (t.getType() != PythonLexer.EOF)
            stream.consume();
        if (t.getType() != PythonLexer.NEWLINE) {
//            System.out.println("NEWLINE processing:" + t.getText() + t.getType());
            List hiddenTokens = null;
            try {
                hiddenTokens = stream.getTokens(lastTokenAddedIndex + 1, t.getTokenIndex() - 1);
                if (hiddenTokens != null) tokens.addAll(hiddenTokens);
            } catch (IndexOutOfBoundsException e) {

            }
            lastTokenAddedIndex = t.getTokenIndex();
            tokens.add(t);
            return;
        }

        // we know it's a newline
        Set<Integer> hidden = new HashSet<>();
        hidden.add(PythonLexer.LEADING_WS);
        hidden.add(PythonLexer.NEWLINE);
        hidden.add(PythonLexer.WS);
        // save NEWLINE in the queue
        if (debugMode)
            System.out.println("found newline: " + t + " stack is " + stackString());
        List hiddenTokens = stream.getTokens(lastTokenAddedIndex + 1,
                t.getTokenIndex() - 1,
                hidden);
        if (hiddenTokens != null) {
            tokens.addAll(hiddenTokens);
        }
        lastTokenAddedIndex = t.getTokenIndex();
        tokens.add(t);

        // grab first token of next line
        t = stream.LT(1);
        if (t.getType() != PythonLexer.EOF)
            stream.consume();

        hiddenTokens = stream.getTokens(lastTokenAddedIndex + 1, t.getTokenIndex() - 1, hidden);
        if (hiddenTokens != null) {
            tokens.addAll(hiddenTokens);
        }
        lastTokenAddedIndex = t.getTokenIndex();

        // compute cpos as the char pos of next non-WS token in line
        int cpos = t.getCharPositionInLine(); // column dictates indent/dedent
        if (t.getType() == Token.EOF) {
            cpos = -1; // pretend EOF always happens at left edge
        } else if (t.getType() == PythonLexer.LEADING_WS) {
            cpos = t.getText().length();
        }
        if (debugMode)
            System.out.println("next token is: " + t);

        // compare to last indent level
        int lastIndent = peek();
        if (debugMode)
            System.out.println("cpos, lastIndent = " + cpos + ", " + lastIndent);
        if (cpos > lastIndent) { // they indented; track and gen INDENT
            push(cpos);
            if (debugMode)
                System.out.println("push(" + cpos + "): " + stackString());
            CommonToken indent = new CommonToken(PythonParser.INDENT, "");
            indent.setCharPositionInLine(t.getCharPositionInLine());
            indent.setLine(t.getLine());
            tokens.add(indent);
        } else if (cpos < lastIndent) { // they dedented
            // how far back did we dedent?
            int prevIndex = findPreviousIndent(cpos);
            if (debugMode)
                System.out.println("dedented; prevIndex of cpos=" + cpos + " is " + prevIndex);
            // generate DEDENTs for each indent level we backed up over
            for (int d = sp - 1; d >= prevIndex; d--) {
                CommonToken dedent = new CommonToken(PythonParser.DEDENT, "");
                dedent.setCharPositionInLine(t.getCharPositionInLine());
                dedent.setLine(t.getLine());
                tokens.add(dedent);
            }
            sp = prevIndex; // pop those off indent level
        }
        if (t.getType() != PythonLexer.LEADING_WS) { // discard WS
            tokens.add(t);
        }
    }
    //  T O K E N  S T A C K  M E T H O D S

    protected void push(int i) {
        if (sp >= MAX_INDENTS) {
            throw new IllegalStateException("stack overflow");
        }
        sp++;
        indentStack[sp] = i;
    }

    protected int pop() {
        if (sp < 0) {
            throw new IllegalStateException("stack underflow");
        }
        int top = indentStack[sp];
        sp--;
        return top;
    }

    protected int peek() {
        return indentStack[sp];
    }

    /**
     * Return the index on stack of previous indent level == i else -1
     */
    protected int findPreviousIndent(int i) {
        for (int j = sp - 1; j >= 0; j--) {
            if (indentStack[j] == i) {
                return j;
            }
        }
        return FIRST_CHAR_POSITION;
    }

    public String stackString() {
        StringBuffer buf = new StringBuffer();
        for (int j = sp; j >= 0; j--) {
            buf.append(" ");
            buf.append(indentStack[j]);
        }
        return buf.toString();
    }


}