package com.ifmo.compiler.Translator;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 09.06.14
 * To change this template use File | Settings | File Templates.
 */

import org.objectweb.asm.Type;


public enum PrimitiveType implements DataType {
    INTEGER(Type.INT_TYPE), BOOLEAN(Type.BOOLEAN_TYPE);//, CHAR(Type.CHAR_TYPE);

    private final Type type;

    private PrimitiveType(Type type) {
        this.type = type;
    }

    @Override
    public boolean isInteger() {
        return this.equals(INTEGER);
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public boolean isBoolean() {
        return this.equals(BOOLEAN);
    }

    @Override
    public boolean isPrimitive() {
        return true;
    }

    @Override
    public boolean isFunction() {
        return false;
    }

    @Override
    public Type getType() {
        return type;
    }
}