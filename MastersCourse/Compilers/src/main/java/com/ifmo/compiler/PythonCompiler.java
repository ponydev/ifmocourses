package com.ifmo.compiler;

import com.ifmo.compiler.Scope.Scope;
import com.ifmo.compiler.Translator.TranslatorVisitor;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class PythonCompiler {
    private static final CompilerErrorListener ERROR_LISTENER = new CompilerErrorListener();

    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("Expected filename to compile.");
            System.exit(1);
        }

        String compileName = args[0];
        try {
//            System.out.println(compileName);
            java.net.URL url = PythonCompiler.class.getResource(compileName);
//            InputStream fis = new FileInputStream(compileName);
            java.io.FileInputStream fis = new java.io.FileInputStream(url.getFile());
            ANTLRInputStream input = new ANTLRInputStream(fis);

            PythonLexer lexer = new PythonLexer(input);
            lexer.addErrorListener(ERROR_LISTENER);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            tokens.fill();
            //System.out.println(tokens.getText());
            PythonTokenSource indentedSource = new PythonTokenSource(tokens, true);
            tokens = new CommonTokenStream(indentedSource);
            tokens.fill();
            // System.out.println(tokens.getTokens());
            PythonParser parser = new PythonParser(tokens);
            parser.addErrorListener(ERROR_LISTENER);
            PythonParser.File_inputContext ctx = parser.file_input();
//            PythonParser.Small_stmtContext ctx = parser.small_stmt();
            System.out.println(ctx.toStringTree(parser));
            if (ERROR_LISTENER.isErrorOccurred()) {
                System.err.println("Syntax error occurred!");
                System.exit(1);
            }


//            System.out.println(ctx.toStringTree(parser));
            TranslatorVisitor visitor = new TranslatorVisitor();
            Scope scope = visitor.visitFile_input(ctx, compileName);
//
            try (FileOutputStream fos = new FileOutputStream(scope.getClassName() + ".class")) {
                fos.write(scope.getByteCode());
            } catch (IOException e) {
                System.err.println("Unexpected io exception: " + e.getMessage());
                e.printStackTrace();
                System.exit(1);
            }
            System.out.println("Successfully compiled " + scope.getClassName() + ".class");
        } catch (FileNotFoundException e) {
            System.err.println("Can't find file: " + compileName);
            System.exit(1);

        } catch (IOException e) {
            System.err.println("Unexpected io exception!");
            e.printStackTrace();
            System.exit(1);
        }
    }
}