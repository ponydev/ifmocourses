package com.ifmo.compiler.Scope;

import org.objectweb.asm.Label;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 09.06.14
 * To change this template use File | Settings | File Templates.
 */
class LoopDescriptor {
    private final Label continueLabel;
    private final Label breakLabel;

    public LoopDescriptor(Label continueLabel, Label breakLabel) {
        this.continueLabel = continueLabel;
        this.breakLabel = breakLabel;
    }

    public Label getBreakLabel() {
        return breakLabel;
    }

    public Label getContinueLabel() {
        return continueLabel;
    }
}

