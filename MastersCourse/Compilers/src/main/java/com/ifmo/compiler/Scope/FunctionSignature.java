package com.ifmo.compiler.Scope;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 09.06.14
 * To change this template use File | Settings | File Templates.
 */


class FunctionSignature {
    private final String name;
    //    private final DataType[] argumentType;
    private final Integer argumentCount;

    public FunctionSignature(String name, Integer argumentCount) {
        this.name = name;
        this.argumentCount = argumentCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FunctionSignature that = (FunctionSignature) o;
        return name.equals(that.name) && argumentCount.equals(this.argumentCount);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + argumentCount.hashCode();
        return result;
    }
}