package com.ifmo.compiler.Translator;

import org.objectweb.asm.Type;


/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 09.06.14
 * To change this template use File | Settings | File Templates.
 */

public interface DataType {
    Type getType();

    boolean isPrimitive();

    boolean isInteger();

    boolean isBoolean();

    boolean isFunction();

    int getSize();
}