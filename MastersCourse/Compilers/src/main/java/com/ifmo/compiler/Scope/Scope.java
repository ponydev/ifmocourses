package com.ifmo.compiler.Scope;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 09.06.14
 * To change this template use File | Settings | File Templates.
 */

public interface Scope {
    String getClassName();

    byte[] getByteCode();
}
