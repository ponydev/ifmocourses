package com.ifmo.compiler.Scope;

import com.ifmo.compiler.Translator.DataType;

/**
 * Created with IntelliJ IDEA.
 * User: killerforfun
 * Date: 09.06.14
 * To change this template use File | Settings | File Templates.
 */
public class LocalVariableDescriptor {
    private final int index;
    private DataType type;

    public LocalVariableDescriptor(int index, DataType type) {
        this.index = index;
        this.type = type;
    }

    public int getIndex() {
        return index;
    }

    public void changeTypeTo(DataType s) {
        this.type = s;
    }

    public DataType getType() {
        return type;
    }
}