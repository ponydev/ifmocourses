grammar Python;


@lexer::members {
    int spaces = 0;
}

tokens {
    INDENT,
    DEDENT
}


single_input
    : NEWLINE
    | simple_stmt
    | compound_stmt NEWLINE;


file_input
    :   (NEWLINE | stmt)*
	;


funcdef
    :   'def' NAME parameters COLON suite
	{System.out.println("found method def "+$NAME.text);}
	;

parameters
    :   LPAREN (varargslist)? RPAREN
	;

varargslist
    :   defparameter (COMMA defparameter)*
    ;

defparameter
    :   NAME (ASSIGN test)?
    ;


stmt: simple_stmt
	| compound_stmt
	;

simple_stmt
    :   small_stmt (SEMI small_stmt)* (SEMI)? NEWLINE
    |   small_stmt (SEMI small_stmt)* (SEMI)? EOF  //mbe bad
	;

small_stmt: expr_stmt
	| print_stmt
	| global_stmt
    | flow_stmt;

expr_stmt
    :	power (ASSIGN test)?
	;


print_stmt:
        'print' test
      /*  (   testlist
        |   RIGHTSHIFT testlist
        )?  */
	;


flow_stmt: break_stmt
	| continue_stmt
	| return_stmt
	;

break_stmt: 'break'
	;

continue_stmt: 'continue'
	;

return_stmt: 'return' (test)?
	;

global_stmt: 'global' NAME (COMMA NAME)*
	;

compound_stmt: if_stmt
	| while_stmt
//	| for_stmt         implement for string
	| funcdef
	;

if_stmt: 'if' test COLON suite ('elif' test COLON suite)* ('else' COLON suite)?
	;

while_stmt: 'while' test COLON suite ('else' COLON suite)?
	;

for_stmt: 'for' exprlist 'in' test COLON suite ('else' COLON suite)?
	;


suite: simple_stmt
	| NEWLINE INDENT (stmt)+ DEDENT
	;


test: and_test ('or' and_test)*

	;

and_test
	: not_test ('and' not_test)*
	;

not_test
	: 'not' not_test
	| comparison
	;

comparison: expr (comp_op expr)*
	;

comp_op: LESS
	|GREATER
	|EQUAL
	|GREATEREQUAL
	|LESSEQUAL
	|ALT_NOTEQUAL
	|NOTEQUAL
    ;

expr: xor_expr (VBAR xor_expr)*
	;

xor_expr: and_expr (CIRCUMFLEX and_expr)*
	;

and_expr: shift_expr (AMPER shift_expr)*
	;

shift_expr: arith_expr ((LEFTSHIFT|RIGHTSHIFT) arith_expr)*
	;

arith_expr: term ((PLUS|MINUS) term)*
	;

term: factor ((STAR | SLASH | PERCENT) factor)*
	;

factor
	: (PLUS|MINUS) factor
	| power
	;

power
	: atom (trailer)?
	;

atom: LPAREN (test)? RPAREN
	| BOOL
	| NAME
	| NAME LBRACK INT RBRACK
	| NAME LBRACK NAME RBRACK
	| INT
	| (STRING)+ LBRACK INT RBRACK
    | (STRING)+ LBRACK NAME RBRACK
	| (STRING)+
	;

trailer: LPAREN (arglist)? RPAREN
	;


exprlist
    :   expr (COMMA expr)* (COMMA)?
	;


arglist
    :   argument (COMMA argument)*
    ;

argument : NAME ASSIGN test
         | test
         ;


LPAREN	: '(';

RPAREN	: ')';

LBRACK	: '[' ;

RBRACK	: ']'  ;

COLON 	: ':' ;

COMMA	: ',' ;

SEMI	: ';' ;

PLUS	: '+' ;

MINUS	: '-' ;

STAR	: '*' ;

SLASH	: '/' ;

VBAR	: '|' ;

AMPER	: '&' ;

LESS	: '<' ;

GREATER	: '>' ;

ASSIGN	: '=' ;

PERCENT	: '%' ;

LCURLY	: '{'  ;

RCURLY	: '}'  ;

CIRCUMFLEX	: '^' ;

TILDE	: '~' ;

EQUAL	: '==' ;

NOTEQUAL	: '!=' ;

ALT_NOTEQUAL: '<>' ;

LESSEQUAL	: '<=' ;

LEFTSHIFT	: '<<' ;

GREATEREQUAL	: '>=' ;

RIGHTSHIFT	: '>>' ;

DOT : '.' ;


INT : ( '0' .. '9' )+ ;

BOOL : 'True' | 'False';



NAME:	( 'a' .. 'z' | 'A' .. 'Z' | '_')
        ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
    ;


STRING
    :   ('r'|'u'|'ur')?
        (   '\'\'\'' ()*? '\'\'\''
        |   '"""' ()*? '"""'
        |   '"' (~('\\'|'\n'|'"'))* '"'
        |   '\'' (~('\\'|'\n'|'\''))* '\''
        )
	;


NEWLINE
    :   (('\r')? '\n' )+
    //{if ( getCharPositionInLine()==0 ) setChannel(HIDDEN); }
    ;

WS	:	{getCharPositionInLine()>0}? (' '|'\t')+ {setChannel(HIDDEN); }
    ;


LEADING_WS
     :
    {spaces = 0;}
    {getCharPositionInLine()==0}?
    (  ( 	' '  { spaces++; }
        	|	'\t' { spaces += 8; spaces -= (spaces % 8); }
       		)+
        	{
            // make a string of n spaces where n is column number - 1
            char[] indentation = new char[spaces];
            for (int i=0; i<spaces; i++) {
                indentation[i] = ' ';
            }
            String s = new String(indentation);
            emit(new CommonToken(LEADING_WS,new String(indentation)));

        	}
        	 //kill trailing newline if present and then ignore
        	//( ('\r')? '\n' {if (state.token!=null) state.token.setChannel(HIDDEN); else setChannel(HIDDEN);})*
            //{token.setChannel(99); }
        )
    ;

