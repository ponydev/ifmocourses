// Generated from C:/Programs/CompilerCourse/src/main/antlr4/com/ifmo/compiler\Python.g4 by ANTLR 4.x
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PythonParser}.
 */
public interface PythonListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PythonParser#arglist}.
	 * @param ctx the parse tree
	 */
	void enterArglist(@NotNull PythonParser.ArglistContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#arglist}.
	 * @param ctx the parse tree
	 */
	void exitArglist(@NotNull PythonParser.ArglistContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(@NotNull PythonParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(@NotNull PythonParser.AtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#trailer}.
	 * @param ctx the parse tree
	 */
	void enterTrailer(@NotNull PythonParser.TrailerContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#trailer}.
	 * @param ctx the parse tree
	 */
	void exitTrailer(@NotNull PythonParser.TrailerContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#and_expr}.
	 * @param ctx the parse tree
	 */
	void enterAnd_expr(@NotNull PythonParser.And_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#and_expr}.
	 * @param ctx the parse tree
	 */
	void exitAnd_expr(@NotNull PythonParser.And_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#and_test}.
	 * @param ctx the parse tree
	 */
	void enterAnd_test(@NotNull PythonParser.And_testContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#and_test}.
	 * @param ctx the parse tree
	 */
	void exitAnd_test(@NotNull PythonParser.And_testContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#global_stmt}.
	 * @param ctx the parse tree
	 */
	void enterGlobal_stmt(@NotNull PythonParser.Global_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#global_stmt}.
	 * @param ctx the parse tree
	 */
	void exitGlobal_stmt(@NotNull PythonParser.Global_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#test}.
	 * @param ctx the parse tree
	 */
	void enterTest(@NotNull PythonParser.TestContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#test}.
	 * @param ctx the parse tree
	 */
	void exitTest(@NotNull PythonParser.TestContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(@NotNull PythonParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(@NotNull PythonParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#varargslist}.
	 * @param ctx the parse tree
	 */
	void enterVarargslist(@NotNull PythonParser.VarargslistContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#varargslist}.
	 * @param ctx the parse tree
	 */
	void exitVarargslist(@NotNull PythonParser.VarargslistContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#file_input}.
	 * @param ctx the parse tree
	 */
	void enterFile_input(@NotNull PythonParser.File_inputContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#file_input}.
	 * @param ctx the parse tree
	 */
	void exitFile_input(@NotNull PythonParser.File_inputContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#defparameter}.
	 * @param ctx the parse tree
	 */
	void enterDefparameter(@NotNull PythonParser.DefparameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#defparameter}.
	 * @param ctx the parse tree
	 */
	void exitDefparameter(@NotNull PythonParser.DefparameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#xor_expr}.
	 * @param ctx the parse tree
	 */
	void enterXor_expr(@NotNull PythonParser.Xor_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#xor_expr}.
	 * @param ctx the parse tree
	 */
	void exitXor_expr(@NotNull PythonParser.Xor_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#power}.
	 * @param ctx the parse tree
	 */
	void enterPower(@NotNull PythonParser.PowerContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#power}.
	 * @param ctx the parse tree
	 */
	void exitPower(@NotNull PythonParser.PowerContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#shift_expr}.
	 * @param ctx the parse tree
	 */
	void enterShift_expr(@NotNull PythonParser.Shift_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#shift_expr}.
	 * @param ctx the parse tree
	 */
	void exitShift_expr(@NotNull PythonParser.Shift_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(@NotNull PythonParser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(@NotNull PythonParser.ParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#exprlist}.
	 * @param ctx the parse tree
	 */
	void enterExprlist(@NotNull PythonParser.ExprlistContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#exprlist}.
	 * @param ctx the parse tree
	 */
	void exitExprlist(@NotNull PythonParser.ExprlistContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#small_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSmall_stmt(@NotNull PythonParser.Small_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#small_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSmall_stmt(@NotNull PythonParser.Small_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#simple_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSimple_stmt(@NotNull PythonParser.Simple_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#simple_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSimple_stmt(@NotNull PythonParser.Simple_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#comp_op}.
	 * @param ctx the parse tree
	 */
	void enterComp_op(@NotNull PythonParser.Comp_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#comp_op}.
	 * @param ctx the parse tree
	 */
	void exitComp_op(@NotNull PythonParser.Comp_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(@NotNull PythonParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(@NotNull PythonParser.ArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#funcdef}.
	 * @param ctx the parse tree
	 */
	void enterFuncdef(@NotNull PythonParser.FuncdefContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#funcdef}.
	 * @param ctx the parse tree
	 */
	void exitFuncdef(@NotNull PythonParser.FuncdefContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFor_stmt(@NotNull PythonParser.For_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFor_stmt(@NotNull PythonParser.For_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#arith_expr}.
	 * @param ctx the parse tree
	 */
	void enterArith_expr(@NotNull PythonParser.Arith_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#arith_expr}.
	 * @param ctx the parse tree
	 */
	void exitArith_expr(@NotNull PythonParser.Arith_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(@NotNull PythonParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(@NotNull PythonParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#single_input}.
	 * @param ctx the parse tree
	 */
	void enterSingle_input(@NotNull PythonParser.Single_inputContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#single_input}.
	 * @param ctx the parse tree
	 */
	void exitSingle_input(@NotNull PythonParser.Single_inputContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#compound_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCompound_stmt(@NotNull PythonParser.Compound_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#compound_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCompound_stmt(@NotNull PythonParser.Compound_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#expr_stmt}.
	 * @param ctx the parse tree
	 */
	void enterExpr_stmt(@NotNull PythonParser.Expr_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#expr_stmt}.
	 * @param ctx the parse tree
	 */
	void exitExpr_stmt(@NotNull PythonParser.Expr_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#not_test}.
	 * @param ctx the parse tree
	 */
	void enterNot_test(@NotNull PythonParser.Not_testContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#not_test}.
	 * @param ctx the parse tree
	 */
	void exitNot_test(@NotNull PythonParser.Not_testContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactor(@NotNull PythonParser.FactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactor(@NotNull PythonParser.FactorContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#break_stmt}.
	 * @param ctx the parse tree
	 */
	void enterBreak_stmt(@NotNull PythonParser.Break_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#break_stmt}.
	 * @param ctx the parse tree
	 */
	void exitBreak_stmt(@NotNull PythonParser.Break_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#while_stmt}.
	 * @param ctx the parse tree
	 */
	void enterWhile_stmt(@NotNull PythonParser.While_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#while_stmt}.
	 * @param ctx the parse tree
	 */
	void exitWhile_stmt(@NotNull PythonParser.While_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#comparison}.
	 * @param ctx the parse tree
	 */
	void enterComparison(@NotNull PythonParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#comparison}.
	 * @param ctx the parse tree
	 */
	void exitComparison(@NotNull PythonParser.ComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(@NotNull PythonParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(@NotNull PythonParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#flow_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFlow_stmt(@NotNull PythonParser.Flow_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#flow_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFlow_stmt(@NotNull PythonParser.Flow_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#print_stmt}.
	 * @param ctx the parse tree
	 */
	void enterPrint_stmt(@NotNull PythonParser.Print_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#print_stmt}.
	 * @param ctx the parse tree
	 */
	void exitPrint_stmt(@NotNull PythonParser.Print_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#return_stmt}.
	 * @param ctx the parse tree
	 */
	void enterReturn_stmt(@NotNull PythonParser.Return_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#return_stmt}.
	 * @param ctx the parse tree
	 */
	void exitReturn_stmt(@NotNull PythonParser.Return_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIf_stmt(@NotNull PythonParser.If_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIf_stmt(@NotNull PythonParser.If_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#suite}.
	 * @param ctx the parse tree
	 */
	void enterSuite(@NotNull PythonParser.SuiteContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#suite}.
	 * @param ctx the parse tree
	 */
	void exitSuite(@NotNull PythonParser.SuiteContext ctx);
	/**
	 * Enter a parse tree produced by {@link PythonParser#continue_stmt}.
	 * @param ctx the parse tree
	 */
	void enterContinue_stmt(@NotNull PythonParser.Continue_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link PythonParser#continue_stmt}.
	 * @param ctx the parse tree
	 */
	void exitContinue_stmt(@NotNull PythonParser.Continue_stmtContext ctx);
}