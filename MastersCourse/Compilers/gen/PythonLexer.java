// Generated from C:/Programs/CompilerCourse/src/main/antlr4/com/ifmo/compiler\Python.g4 by ANTLR 4.x
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PythonLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__14=1, T__13=2, T__12=3, T__11=4, T__10=5, T__9=6, T__8=7, T__7=8, T__6=9, 
		T__5=10, T__4=11, T__3=12, T__2=13, T__1=14, T__0=15, LPAREN=16, RPAREN=17, 
		LBRACK=18, RBRACK=19, COLON=20, COMMA=21, SEMI=22, PLUS=23, MINUS=24, 
		STAR=25, SLASH=26, VBAR=27, AMPER=28, LESS=29, GREATER=30, ASSIGN=31, 
		PERCENT=32, LCURLY=33, RCURLY=34, CIRCUMFLEX=35, TILDE=36, EQUAL=37, NOTEQUAL=38, 
		ALT_NOTEQUAL=39, LESSEQUAL=40, LEFTSHIFT=41, GREATEREQUAL=42, RIGHTSHIFT=43, 
		DOT=44, INT=45, BOOL=46, NAME=47, STRING=48, NEWLINE=49, WS=50, LEADING_WS=51;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'", "'\\u0015'", "'\\u0016'", "'\\u0017'", "'\\u0018'", 
		"'\\u0019'", "'\\u001A'", "'\\u001B'", "'\\u001C'", "'\\u001D'", "'\\u001E'", 
		"'\\u001F'", "' '", "'!'", "'\"'", "'#'", "'$'", "'%'", "'&'", "'''", 
		"'('", "')'", "'*'", "'+'", "','", "'-'", "'.'", "'/'", "'0'", "'1'", 
		"'2'", "'3'"
	};
	public static final String[] ruleNames = {
		"T__14", "T__13", "T__12", "T__11", "T__10", "T__9", "T__8", "T__7", "T__6", 
		"T__5", "T__4", "T__3", "T__2", "T__1", "T__0", "LPAREN", "RPAREN", "LBRACK", 
		"RBRACK", "COLON", "COMMA", "SEMI", "PLUS", "MINUS", "STAR", "SLASH", 
		"VBAR", "AMPER", "LESS", "GREATER", "ASSIGN", "PERCENT", "LCURLY", "RCURLY", 
		"CIRCUMFLEX", "TILDE", "EQUAL", "NOTEQUAL", "ALT_NOTEQUAL", "LESSEQUAL", 
		"LEFTSHIFT", "GREATEREQUAL", "RIGHTSHIFT", "DOT", "INT", "BOOL", "NAME", 
		"STRING", "NEWLINE", "WS", "LEADING_WS"
	};


	    int spaces = 0;


	public PythonLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Python.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 49: WS_action((RuleContext)_localctx, actionIndex); break;
		case 50: LEADING_WS_action((RuleContext)_localctx, actionIndex); break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0: setChannel(HIDDEN);  break;
		}
	}
	private void LEADING_WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1: spaces = 0; break;
		case 2:  spaces++;  break;
		case 3:  spaces += 8; spaces -= (spaces % 8);  break;
		case 4: 
		            // make a string of n spaces where n is column number - 1
		            char[] indentation = new char[spaces];
		            for (int i=0; i<spaces; i++) {
		                indentation[i] = ' ';
		            }
		            String s = new String(indentation);
		            emit(new CommonToken(LEADING_WS,new String(indentation)));

		        	 break;
		}
	}
	@Override
	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 49: return WS_sempred((RuleContext)_localctx, predIndex);
		case 50: return LEADING_WS_sempred((RuleContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean WS_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return getCharPositionInLine()>0;
		}
		return true;
	}
	private boolean LEADING_WS_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1: return getCharPositionInLine()==0;
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\65\u015a\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64"+
		"\t\64\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3"+
		"\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3"+
		"\16\3\16\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\22\3"+
		"\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3"+
		"\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 "+
		"\3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3&\3\'\3\'\3\'\3(\3(\3(\3)\3)\3"+
		")\3*\3*\3*\3+\3+\3+\3,\3,\3,\3-\3-\3.\6.\u00f8\n.\r.\16.\u00f9\3/\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\5/\u0105\n/\3\60\3\60\7\60\u0109\n\60\f\60\16\60\u010c"+
		"\13\60\3\61\3\61\3\61\5\61\u0111\n\61\3\61\3\61\3\61\3\61\3\61\7\61\u0118"+
		"\n\61\f\61\16\61\u011b\13\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\7"+
		"\61\u0125\n\61\f\61\16\61\u0128\13\61\3\61\3\61\3\61\3\61\3\61\7\61\u012f"+
		"\n\61\f\61\16\61\u0132\13\61\3\61\3\61\3\61\7\61\u0137\n\61\f\61\16\61"+
		"\u013a\13\61\3\61\5\61\u013d\n\61\3\62\5\62\u0140\n\62\3\62\6\62\u0143"+
		"\n\62\r\62\16\62\u0144\3\63\3\63\6\63\u0149\n\63\r\63\16\63\u014a\3\63"+
		"\3\63\3\64\3\64\3\64\3\64\3\64\3\64\6\64\u0155\n\64\r\64\16\64\u0156\3"+
		"\64\3\64\4\u0119\u0126\2\65\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25"+
		"\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32"+
		"\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a"+
		"\62c\63e\64g\65\3\2\b\5\2C\\aac|\6\2\62;C\\aac|\4\2ttww\5\2\f\f$$^^\5"+
		"\2\f\f))^^\4\2\13\13\"\"\u016a\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2"+
		"\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2"+
		"+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2"+
		"\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2"+
		"C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3"+
		"\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2"+
		"\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\3"+
		"i\3\2\2\2\5l\3\2\2\2\7r\3\2\2\2\tu\3\2\2\2\13y\3\2\2\2\r\177\3\2\2\2\17"+
		"\u0083\3\2\2\2\21\u0086\3\2\2\2\23\u008b\3\2\2\2\25\u0092\3\2\2\2\27\u0099"+
		"\3\2\2\2\31\u00a2\3\2\2\2\33\u00a6\3\2\2\2\35\u00ac\3\2\2\2\37\u00b0\3"+
		"\2\2\2!\u00b5\3\2\2\2#\u00b7\3\2\2\2%\u00b9\3\2\2\2\'\u00bb\3\2\2\2)\u00bd"+
		"\3\2\2\2+\u00bf\3\2\2\2-\u00c1\3\2\2\2/\u00c3\3\2\2\2\61\u00c5\3\2\2\2"+
		"\63\u00c7\3\2\2\2\65\u00c9\3\2\2\2\67\u00cb\3\2\2\29\u00cd\3\2\2\2;\u00cf"+
		"\3\2\2\2=\u00d1\3\2\2\2?\u00d3\3\2\2\2A\u00d5\3\2\2\2C\u00d7\3\2\2\2E"+
		"\u00d9\3\2\2\2G\u00db\3\2\2\2I\u00dd\3\2\2\2K\u00df\3\2\2\2M\u00e2\3\2"+
		"\2\2O\u00e5\3\2\2\2Q\u00e8\3\2\2\2S\u00eb\3\2\2\2U\u00ee\3\2\2\2W\u00f1"+
		"\3\2\2\2Y\u00f4\3\2\2\2[\u00f7\3\2\2\2]\u0104\3\2\2\2_\u0106\3\2\2\2a"+
		"\u0110\3\2\2\2c\u0142\3\2\2\2e\u0146\3\2\2\2g\u014e\3\2\2\2ij\7k\2\2j"+
		"k\7p\2\2k\4\3\2\2\2lm\7y\2\2mn\7j\2\2no\7k\2\2op\7n\2\2pq\7g\2\2q\6\3"+
		"\2\2\2rs\7q\2\2st\7t\2\2t\b\3\2\2\2uv\7h\2\2vw\7q\2\2wx\7t\2\2x\n\3\2"+
		"\2\2yz\7r\2\2z{\7t\2\2{|\7k\2\2|}\7p\2\2}~\7v\2\2~\f\3\2\2\2\177\u0080"+
		"\7p\2\2\u0080\u0081\7q\2\2\u0081\u0082\7v\2\2\u0082\16\3\2\2\2\u0083\u0084"+
		"\7k\2\2\u0084\u0085\7h\2\2\u0085\20\3\2\2\2\u0086\u0087\7g\2\2\u0087\u0088"+
		"\7n\2\2\u0088\u0089\7k\2\2\u0089\u008a\7h\2\2\u008a\22\3\2\2\2\u008b\u008c"+
		"\7i\2\2\u008c\u008d\7n\2\2\u008d\u008e\7q\2\2\u008e\u008f\7d\2\2\u008f"+
		"\u0090\7c\2\2\u0090\u0091\7n\2\2\u0091\24\3\2\2\2\u0092\u0093\7t\2\2\u0093"+
		"\u0094\7g\2\2\u0094\u0095\7v\2\2\u0095\u0096\7w\2\2\u0096\u0097\7t\2\2"+
		"\u0097\u0098\7p\2\2\u0098\26\3\2\2\2\u0099\u009a\7e\2\2\u009a\u009b\7"+
		"q\2\2\u009b\u009c\7p\2\2\u009c\u009d\7v\2\2\u009d\u009e\7k\2\2\u009e\u009f"+
		"\7p\2\2\u009f\u00a0\7w\2\2\u00a0\u00a1\7g\2\2\u00a1\30\3\2\2\2\u00a2\u00a3"+
		"\7f\2\2\u00a3\u00a4\7g\2\2\u00a4\u00a5\7h\2\2\u00a5\32\3\2\2\2\u00a6\u00a7"+
		"\7d\2\2\u00a7\u00a8\7t\2\2\u00a8\u00a9\7g\2\2\u00a9\u00aa\7c\2\2\u00aa"+
		"\u00ab\7m\2\2\u00ab\34\3\2\2\2\u00ac\u00ad\7c\2\2\u00ad\u00ae\7p\2\2\u00ae"+
		"\u00af\7f\2\2\u00af\36\3\2\2\2\u00b0\u00b1\7g\2\2\u00b1\u00b2\7n\2\2\u00b2"+
		"\u00b3\7u\2\2\u00b3\u00b4\7g\2\2\u00b4 \3\2\2\2\u00b5\u00b6\7*\2\2\u00b6"+
		"\"\3\2\2\2\u00b7\u00b8\7+\2\2\u00b8$\3\2\2\2\u00b9\u00ba\7]\2\2\u00ba"+
		"&\3\2\2\2\u00bb\u00bc\7_\2\2\u00bc(\3\2\2\2\u00bd\u00be\7<\2\2\u00be*"+
		"\3\2\2\2\u00bf\u00c0\7.\2\2\u00c0,\3\2\2\2\u00c1\u00c2\7=\2\2\u00c2.\3"+
		"\2\2\2\u00c3\u00c4\7-\2\2\u00c4\60\3\2\2\2\u00c5\u00c6\7/\2\2\u00c6\62"+
		"\3\2\2\2\u00c7\u00c8\7,\2\2\u00c8\64\3\2\2\2\u00c9\u00ca\7\61\2\2\u00ca"+
		"\66\3\2\2\2\u00cb\u00cc\7~\2\2\u00cc8\3\2\2\2\u00cd\u00ce\7(\2\2\u00ce"+
		":\3\2\2\2\u00cf\u00d0\7>\2\2\u00d0<\3\2\2\2\u00d1\u00d2\7@\2\2\u00d2>"+
		"\3\2\2\2\u00d3\u00d4\7?\2\2\u00d4@\3\2\2\2\u00d5\u00d6\7\'\2\2\u00d6B"+
		"\3\2\2\2\u00d7\u00d8\7}\2\2\u00d8D\3\2\2\2\u00d9\u00da\7\177\2\2\u00da"+
		"F\3\2\2\2\u00db\u00dc\7`\2\2\u00dcH\3\2\2\2\u00dd\u00de\7\u0080\2\2\u00de"+
		"J\3\2\2\2\u00df\u00e0\7?\2\2\u00e0\u00e1\7?\2\2\u00e1L\3\2\2\2\u00e2\u00e3"+
		"\7#\2\2\u00e3\u00e4\7?\2\2\u00e4N\3\2\2\2\u00e5\u00e6\7>\2\2\u00e6\u00e7"+
		"\7@\2\2\u00e7P\3\2\2\2\u00e8\u00e9\7>\2\2\u00e9\u00ea\7?\2\2\u00eaR\3"+
		"\2\2\2\u00eb\u00ec\7>\2\2\u00ec\u00ed\7>\2\2\u00edT\3\2\2\2\u00ee\u00ef"+
		"\7@\2\2\u00ef\u00f0\7?\2\2\u00f0V\3\2\2\2\u00f1\u00f2\7@\2\2\u00f2\u00f3"+
		"\7@\2\2\u00f3X\3\2\2\2\u00f4\u00f5\7\60\2\2\u00f5Z\3\2\2\2\u00f6\u00f8"+
		"\4\62;\2\u00f7\u00f6\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00f7\3\2\2\2\u00f9"+
		"\u00fa\3\2\2\2\u00fa\\\3\2\2\2\u00fb\u00fc\7V\2\2\u00fc\u00fd\7t\2\2\u00fd"+
		"\u00fe\7w\2\2\u00fe\u0105\7g\2\2\u00ff\u0100\7H\2\2\u0100\u0101\7c\2\2"+
		"\u0101\u0102\7n\2\2\u0102\u0103\7u\2\2\u0103\u0105\7g\2\2\u0104\u00fb"+
		"\3\2\2\2\u0104\u00ff\3\2\2\2\u0105^\3\2\2\2\u0106\u010a\t\2\2\2\u0107"+
		"\u0109\t\3\2\2\u0108\u0107\3\2\2\2\u0109\u010c\3\2\2\2\u010a\u0108\3\2"+
		"\2\2\u010a\u010b\3\2\2\2\u010b`\3\2\2\2\u010c\u010a\3\2\2\2\u010d\u0111"+
		"\t\4\2\2\u010e\u010f\7w\2\2\u010f\u0111\7t\2\2\u0110\u010d\3\2\2\2\u0110"+
		"\u010e\3\2\2\2\u0110\u0111\3\2\2\2\u0111\u013c\3\2\2\2\u0112\u0113\7)"+
		"\2\2\u0113\u0114\7)\2\2\u0114\u0115\7)\2\2\u0115\u0119\3\2\2\2\u0116\u0118"+
		"\3\2\2\2\u0117\u0116\3\2\2\2\u0118\u011b\3\2\2\2\u0119\u011a\3\2\2\2\u0119"+
		"\u0117\3\2\2\2\u011a\u011c\3\2\2\2\u011b\u0119\3\2\2\2\u011c\u011d\7)"+
		"\2\2\u011d\u011e\7)\2\2\u011e\u013d\7)\2\2\u011f\u0120\7$\2\2\u0120\u0121"+
		"\7$\2\2\u0121\u0122\7$\2\2\u0122\u0126\3\2\2\2\u0123\u0125\3\2\2\2\u0124"+
		"\u0123\3\2\2\2\u0125\u0128\3\2\2\2\u0126\u0127\3\2\2\2\u0126\u0124\3\2"+
		"\2\2\u0127\u0129\3\2\2\2\u0128\u0126\3\2\2\2\u0129\u012a\7$\2\2\u012a"+
		"\u012b\7$\2\2\u012b\u013d\7$\2\2\u012c\u0130\7$\2\2\u012d\u012f\n\5\2"+
		"\2\u012e\u012d\3\2\2\2\u012f\u0132\3\2\2\2\u0130\u012e\3\2\2\2\u0130\u0131"+
		"\3\2\2\2\u0131\u0133\3\2\2\2\u0132\u0130\3\2\2\2\u0133\u013d\7$\2\2\u0134"+
		"\u0138\7)\2\2\u0135\u0137\n\6\2\2\u0136\u0135\3\2\2\2\u0137\u013a\3\2"+
		"\2\2\u0138\u0136\3\2\2\2\u0138\u0139\3\2\2\2\u0139\u013b\3\2\2\2\u013a"+
		"\u0138\3\2\2\2\u013b\u013d\7)\2\2\u013c\u0112\3\2\2\2\u013c\u011f\3\2"+
		"\2\2\u013c\u012c\3\2\2\2\u013c\u0134\3\2\2\2\u013db\3\2\2\2\u013e\u0140"+
		"\7\17\2\2\u013f\u013e\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u0141\3\2\2\2"+
		"\u0141\u0143\7\f\2\2\u0142\u013f\3\2\2\2\u0143\u0144\3\2\2\2\u0144\u0142"+
		"\3\2\2\2\u0144\u0145\3\2\2\2\u0145d\3\2\2\2\u0146\u0148\6\63\2\2\u0147"+
		"\u0149\t\7\2\2\u0148\u0147\3\2\2\2\u0149\u014a\3\2\2\2\u014a\u0148\3\2"+
		"\2\2\u014a\u014b\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u014d\b\63\2\2\u014d"+
		"f\3\2\2\2\u014e\u014f\b\64\3\2\u014f\u0154\6\64\3\2\u0150\u0151\7\"\2"+
		"\2\u0151\u0155\b\64\4\2\u0152\u0153\7\13\2\2\u0153\u0155\b\64\5\2\u0154"+
		"\u0150\3\2\2\2\u0154\u0152\3\2\2\2\u0155\u0156\3\2\2\2\u0156\u0154\3\2"+
		"\2\2\u0156\u0157\3\2\2\2\u0157\u0158\3\2\2\2\u0158\u0159\b\64\6\2\u0159"+
		"h\3\2\2\2\21\2\u00f9\u0104\u010a\u0110\u0119\u0126\u0130\u0138\u013c\u013f"+
		"\u0144\u014a\u0154\u0156\7\3\63\2\3\64\3\3\64\4\3\64\5\3\64\6";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}